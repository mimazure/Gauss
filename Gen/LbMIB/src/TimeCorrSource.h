/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef TIMECORRSOURCE_H
#define TIMECORRSOURCE_H 1

// Include files
// from Gaudi
#include "GaudiAlg/GaudiHistoTool.h"
#include "GaudiKernel/RndmGenerators.h"
#include "GaudiKernel/SystemOfUnits.h"
#include "Kernel/IParticlePropertySvc.h"
#include "AIDA/IHistogram1D.h"
#include "AIDA/IHistogram2D.h"

#include "IMIBSource.h"            // Interface

// ROOT
#include "TFile.h"
#include "TTree.h"
#include "TDirectory.h"
#include "RooStringVar.h"

/** @class TimeCorrSource TimeCorrSource.h
 *
 *  Tool to read file containing particles reaching LHCb cavern
 *  due to various background sources.
 *  The source can be re-weighted according to the options selected
 *  and produces particles from it.
 *
 *  @author Magnus Lieng
 *  @date   2009-03-31
 */
class TimeCorrSource : public extends<GaudiHistoTool,IMIBSource> {

public:

  /// Standard constructor
  using extends::extends;

  StatusCode initialize() override;    ///< Tool initialization
  StatusCode finalize  () override;    ///< Tool finalization

  StatusCode generateEvent( LHCb::GenHeader* theHeader,
                            LHCb::GenCollisions* theCollisions,
                            LHCb::HepMCEvents* theEvents,
                            int& numPart) override;

protected:

  /// Make particle
  StatusCode generateParticle( HepMC::GenEvent* evt, int i, int& numPart );

  /// Make Envelopes
  StatusCode createEnvelopes();

  /// Book Histograms
  StatusCode bookHistos();

  /// Get Momentum
  HepMC::FourVector getMomentum(double ekin, int pid, double dx, double dy);

  /// Get Vertex
  HepMC::FourVector getVertex(double ekin, int pid, double x, double y,
                              double dx, double dy, double dt);

private:

  /// The particle source file
  Gaudi::Property<std::string> m_pSourceFile{this,"ParticleSourceFile","","Particle input file"};

  /// Scaling factors
  //double m_overrideSource{0.}; //AD - unused 
  Gaudi::Property<double> m_scalingFactor{this,"ScalingFactor",1.0,"Scaling factor"};
  Gaudi::Property<double> m_protInFile{this,"ProtonsInFile",-999999.,""};
  Gaudi::Property<double> m_bunchSize{this,"BunchSize",1.15e11,"Bunch Size"};
  //double m_luminosity{0.};//AD - unused
  Gaudi::Property<double> m_beamEnergy{this,"BeamEnergy",-999999.,"BeamEnergy"};

  /// Histogram generation
  Gaudi::Property<bool> m_genHist{this,"GenerationHist",false,"Histogram generation"};

  /// Mode modifiers
  Gaudi::Property<int> m_pPerEvt{this,"ForceLossPerEvt",-1," -1 : Use weight to find number of losses in event. "
  ">=0 : Force the generation of this many losses in event."};
  Gaudi::Property<int> m_fileOffset{this,"ReadFileFromOffset",-1,"-1 : Choose random losses using weight based envelope method. "
  ">=0 : Pick losses sequentually from file starting at this entry number."};

  /// Timing
  Gaudi::Property<double> m_timeOffset{this,"TimeOffset",0.0*Gaudi::Units::ns,""};

  /// Random number generators
  Rndm::Numbers m_flatGenerator;
  Rndm::Numbers m_poissonGenerator;

  /// Envelope method variables
  Gaudi::Property<unsigned int> m_envelopeSize{this,"EnvelopeSize",10,"Envelope Size used for random loss selection."};
  std::vector<double> m_envelopeHolders;
  int m_counter{0};
  double m_sumOfWeights{0};

  /// ROOT file and trees
  TFile* m_rootFile{nullptr};
  TTree* m_evtTree{nullptr};
  TTree* m_partTree{nullptr};

  /// Structure for full envelope version
  struct ParticleData {
    Int_t pid;
    Double_t ekin;
    Double_t x;
    Double_t y;
    Double_t dx;
    Double_t dy;
    Double_t dt;
    Double_t weight;
    Double_t sumOfWeights;
  };
  double m_zGen{0.};                     ///< Z particle origin generated
  Gaudi::Property<double> m_zOrigin{this,"ZParticleOrigin",-999999.,"Z particle origin in file"};///< Z particle origin in file
  Gaudi::Property<double> m_zGenOff{this,"ZParticleOffset", 0.0*Gaudi::Units::m,"Z particle offset from origin during generation"};///< Z particle offset from origin during generation
  Gaudi::Property<int> m_dz{this,"ZDirection",-999999,"Z particle direction"};///< Z particle direction

  /// Defaults
  int m_dzDef{-999999};
  double m_zOriginDef{-999999.};
  double m_protInFileDef{-999999.};
  double m_beamEnergyDef{-999999.};

  /// Histograms
  AIDA::IHistogram2D* m_xyDistGen{nullptr};
  AIDA::IHistogram2D* m_pxVSrGen{nullptr};
  AIDA::IHistogram2D* m_pyVSrGen{nullptr};
  AIDA::IHistogram2D* m_pzVSrGen{nullptr};
  AIDA::IHistogram1D* m_absPGen{nullptr};
  AIDA::IHistogram1D* m_thetaGen{nullptr};
  AIDA::IHistogram1D* m_timeGen{nullptr};


  LHCb::IParticlePropertySvc* m_ppSvc{nullptr};   ///< Pointer to Particle Property Service

protected:

  /// Get Random Interaction
  StatusCode getRandInt( int &firstPart, int &nParts );

  /// Get Sequencial Interaction
  StatusCode getInt( int &firstPart, int &nParts );

  /// Get Sequencial Particle
  StatusCode getPart(ParticleData* target, int i, int &nPart);

};

#endif  // TIMECORRSOURCE_TIMECORRSOURCEALG_H

