/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef GENCUTS_DAUGHTERSINLHCBANDCUTSFORDS_H
#define GENCUTS_DAUGHTERSINLHCBANDCUTSFORD_H 1

// Include files
#include "DaughtersInLHCb.h"
#include "GaudiKernel/SystemOfUnits.h"

/** @class DaughtersInLHCbAndCutsForD DaughtersInLHCbAndCutsForD.h
 *
 *  Tool to keep events with daughters from signal particles
 *  in LHCb and with p and pt cuts on D daughters.
 *  Concrete implementation of IGenCutTool.
 *
 *  @author Benedetto G. Siddi
 *  @date   2020-03-27
 */

class DaughtersInLHCbAndCutsForD : public extends<DaughtersInLHCb, IGenCutTool> {
 public:
  /// Standard constructor
  using extends::extends;
  
  /** Accept events with daughters in LHCb and p/pt cuts on Dstar daughters
   *  (defined by min and max angles, different values for charged and neutrals)
   *  Implements IGenCutTool::applyCut.
   */
  virtual bool applyCut( ParticleVector & theParticleVector ,
                         const HepMC::GenEvent * theEvent ,
                         const LHCb::GenCollision * theCollision ) const override;

 private:
  /** Study a particle a returns true when all stable daughters
   *  are in LHCb AndWithMinP
   */
  bool passCuts( const HepMC::GenParticle * theSignal ) const ;

  // cut value of D(s) pt
  Gaudi::Property<double> m_dptCut{this,"DPtCuts",1700 * Gaudi::Units::MeV,"cut value of D(s) pt"} ;
  
  // cut on D(s) ctau
  Gaudi::Property<double> m_dctauCut{this,"DCTauCut",-1.,"cut on D(s) ctau"} ;
  
  // cut value on daughters min pt
  Gaudi::Property<double> m_daughtersptminCut{this,"DaughtersPtMinCut",700 * Gaudi::Units::MeV,"cut value on daughters min pt"} ;

  // cut value on daughters max pt
  Gaudi::Property<double> m_daughtersptmaxCut{this,"DaughtersPtMaxCut",1050 * Gaudi::Units::MeV,"cut value on daughters max pt"} ;

  // cut value on daughters min p
  Gaudi::Property<double> m_daughterspminCut{this,"DaughtersPMinCut",4500 * Gaudi::Units::MeV,"cut value on daughters min p"} ;
  
  Gaudi::Property<int> m_signalID{this,"SignalID",431,"SignalID"};

  Gaudi::Property<std::vector<int> > m_childs{this,"Childrens",{321, 211},"Childrens"};
};
#endif // GENCUTS_DAUGHTERSINLHCDANDCUTSFORD_H
