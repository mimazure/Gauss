/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// $Id: BcChargedNumInLHCb.h,v 1.0 2017-04-11 22:24:49 $
#ifndef GENERATORS_BCCHARGEDNUMINLHCB_H
#define GENERATORS_BCCHARGEDNUMINLHCB_H 1

// Include files
// from Gaudi
#include "GaudiAlg/GaudiTool.h"
#include "GaudiKernel/Transform4DTypes.h"

#include "MCInterfaces/IGenCutTool.h"
#include "GaudiKernel/SystemOfUnits.h"

// Forward declaration
class IDecayTool ;

/** @class BcChargedNumInLHCb BcChargedNumInLHCb.h
 *
 *  Tool to select events with enough charged stable particles
 *  which are in LHCb acceptance and from B_c.
 *  Concrete implementation of IGenCutTool.
 *
 *  @author Wenqian Huang
 *  @date   2017-05-04
 */

class BcChargedNumInLHCb : public extends<GaudiTool, IGenCutTool> {
 public:
  /// Standard constructor
  using extends::extends;

  StatusCode initialize( ) override;   ///< Initialize method

  /** Accept events with daughters in LHCb acceptance (defined by min and
   *  max angles, different values for charged and neutrals)
   *  Implements IGenCutTool::applyCut.
   */
  bool applyCut( ParticleVector & theParticleVector ,
                 const HepMC::GenEvent * theEvent ,
                 const LHCb::GenCollision * theCollision ) const override;

  StatusCode finalize( ) override;   ///< Finalize method

protected:



private:
  /// Decay tool
  IDecayTool*  m_decayTool{nullptr};

  /** Study a particle a returns true when all stable daughters
   *  are in LHCb acceptance
   */
  bool passCuts( const HepMC::GenParticle * theSignal ) const ;

  /** Minimum value of angle around z-axis for charged daughters
   * (set by options)
   */
  Gaudi::Property<double> m_chargedThetaMin {this, "ChargedThetaMin",10 * Gaudi::Units::mrad,"Minimum value of angle around z-axis for charged daughters"};

  /** Maximum value of angle around z-axis for charged daughters
   * (set by options)
   */
  Gaudi::Property<double> m_chargedThetaMax{this,"ChargedThetaMax",400 * Gaudi::Units::mrad,"Maximum value of angle around z-axis for charged daughters"} ;

  /** Minimum number of electron in LHCb charged acceptance
   * (set by options)
   */
  Gaudi::Property<int> m_enuminlhcb{this,"ENumInLHCb",0,"Minimum number of electron in LHCb charged acceptance"} ;

  /** Minimum number of muon in LHCb charged acceptance
   * (set by options)
   */
  Gaudi::Property<int> m_munuminlhcb{this,"MuNumInLHCb",0,"Minimum number of muon in LHCb charged acceptance"} ;

  /** Minimum number of kaon in LHCb charged acceptance
   * (set by options)
   */
  Gaudi::Property<int> m_knuminlhcb{this,"KNumInLHCb",0,"Minimum number of kaon in LHCb charged acceptance"} ;

  /** Minimum number of pion in LHCb charged acceptance
   * (set by options)
   */
  Gaudi::Property<int> m_pinuminlhcb{this,"PiNumInLHCb",0,"Minimum number of pion in LHCb charged acceptance"} ;

  /** Minimum number of proton in LHCb charged acceptance
   * (set by options)
   */
  Gaudi::Property<int> m_pnuminlhcb{this,"PNumInLHCb",0,"Minimum number of proton in LHCb charged acceptance"} ;

  /** Minimum number of proton in LHCb charged acceptance
    * (set by options)
    */
  Gaudi::Property<int> m_hadnuminlhcb{this,"HadNumInLHCb",0,"Minimum number of proton in LHCb charged acceptance"} ;

  Gaudi::Property<double> m_mupt{this,"MuPt",0. * Gaudi::Units::MeV,"Mu Pt"} ;

  Gaudi::Property<double> m_hadpt{this,"HadPt",0. * Gaudi::Units::MeV,"Hadron Pt"} ;

  /// Name of the decay tool to use
  Gaudi::Property<std::string> m_decayToolName{this,"DecayTool","EvtGenDecay","Name of the decay tool to use"} ;


  Gaudi::Property<int>  m_sigBcPID{this,"BcPdgId",541,"PDG Id of the B_c"} ;  ///< PDG Id of the B_c

};
#endif // GENERATORS_BCCHARGEDNUMINLHCB_H
