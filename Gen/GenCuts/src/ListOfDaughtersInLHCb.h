/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// $Id: ListOfDaughtersInLHCb.h,v 1.4 2008-05-29 14:22:00 gcorti Exp $
#ifndef GENCUTS_LISTOFDAUGHTERSINLHCB_H
#define GENCUTS_LISTOFDAUGHTERSINLHCB_H 1

// Include files
// from Gaudi
#include "GaudiAlg/GaudiTool.h"
#include "GaudiKernel/SystemOfUnits.h" 

#include "MCInterfaces/IGenCutTool.h"

/** @class ListOfDaughtersInLHCb ListOfDaughtersInLHCb.h
 *
 *  Tool to keep events with several daughters from signal particles
 *  in LHCb acceptance (AND of all daughters).
 *  Concrete implementation of IGenCutTool.
 *
 *  @author Patrick Robbe
 *  @date   2005-08-24
 */
class ListOfDaughtersInLHCb : public extends<GaudiTool, IGenCutTool> {
 public:
  /// Ordered set of particle ID
  typedef std::set< int > PIDs ;

  /// Standard constructor
  using extends::extends;
  
  /// Initialization
  StatusCode initialize() override;

  /** Accept events with daughters in LHCb acceptance (defined by min and
   *  max angles, different values for charged and neutrals)
   *  Implements IGenCutTool::applyCut.
   */
  bool applyCut( ParticleVector & theParticleVector ,
                 const HepMC::GenEvent * theEvent ,
                 const LHCb::GenCollision * theCollision ) const override;

 private:
  /** Study a particle a returns true when all stable daughters
   *  are in LHCb acceptance
   */
  bool passCuts( const HepMC::GenParticle * theSignal ) const ;

  /** Minimum value of angle around z-axis for charged daughters
   * (set by options)
   */
  Gaudi::Property<double> m_chargedThetaMin{this,"ChargedThetaMin", 10 * Gaudi::Units::mrad,"ChargedThetaMin"} ;

  /** Maximum value of angle around z-axis for charged daughters
   * (set by options)
   */
  Gaudi::Property<double> m_chargedThetaMax{this,"ChargedThetaMax", 400 * Gaudi::Units::mrad,"ChargedThetaMax"} ;

  /** Minimum value of angle around z-axis for neutral daughters
   * (set by options)
   */
  Gaudi::Property<double> m_neutralThetaMin{this,"NeutralThetaMin", 5 * Gaudi::Units::mrad,"NeutralThetaMin"} ;

  /** Maximum value of angle around z-axis for neutral daughters
   * (set by options)
   */
  Gaudi::Property<double> m_neutralThetaMax{this,"NeutralThetaMax", 400 * Gaudi::Units::mrad,"NeutralThetaMax"} ;

  /// PDG Id of particle to select  (set by options)
  PIDs m_daughtersPIDList ;

  /// temporary vector to read PIDs from options
  Gaudi::Property< std::vector< int > > m_pidVector{this,"DaughtersPIDList",{},"DaughtersPIDList"} ;
};
#endif // GENCUTS_LISTOFDAUGHTERSINLHCB_H
