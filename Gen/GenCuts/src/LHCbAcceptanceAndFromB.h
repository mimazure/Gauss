/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// $Id: LHCbAcceptanceAndFromB.h,v 1.1 2007-11-26 13:44:34 jonrob Exp $
#ifndef GENCUTS_LHCBACCEPTANCEANDFROMB_H
#define GENCUTS_LHCBACCEPTANCEANDFROMB_H 1

#include "LHCbAcceptance.h"

/** @class LHCbAcceptanceAndFromB LHCbAcceptanceAndFromB.h
 *
 *  Inherits from LHCbAcceptance adding the additional requirement
 *  the the signal be ultimately from a b decay
 *
 *  @author Chris Jones
 *  @date   22/11/2007
 */

class LHCbAcceptanceAndFromB : public LHCbAcceptance,
                               virtual public IGenCutTool
{

public:

  /// Standard constructor
  LHCbAcceptanceAndFromB( const std::string& type,
                          const std::string& name,
                          const IInterface* parent );

  virtual ~LHCbAcceptanceAndFromB( ); ///< Destructor

  /// Initialization
  StatusCode initialize() override;

  /** Accept events in LHCb acceptance (defined by angle)
   *  Implements IGenCutTool::applyCut.
   */
  bool applyCut( ParticleVector & theParticleVector ,
                 const HepMC::GenEvent * thGeneEvent ,
                 const LHCb::GenCollision * theCollision ) const override;

private: // data

  /// From a b cut tool
  const IGenCutTool * m_fromBcuts;

};

#endif // GENCUTS_LHCBACCEPTANCEANDFROMB_H
