/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// $Id: BiasedBB.h,v 1.3 2007-02-22 13:30:24 robbep Exp $
#ifndef GENCUTS_BIASEDBB_H
#define GENCUTS_BIASEDBB_H 1

// Include files
// from Gaudi
#include "GaudiAlg/GaudiTool.h"
#include "GaudiKernel/SystemOfUnits.h"

#include "MCInterfaces/IGenCutTool.h"

/** @class BiasedBB BiasedBB.h
 *
 *  Tool to keep events passing cuts defining the maximum biased BB sample
 *  for DC06.
 *  Concrete implementation of IGenCutTool.
 *
 *  @author Patrick Robbe
 *  @date   2005-08-24
 */
class BiasedBB : public extends<GaudiTool, IGenCutTool> {
 public:

  
  typedef std::set< int > PIDs ;

  /// Initialization
  StatusCode initialize() override;

  /// Standard constructor
  using extends::extends;
  
  /** Accept events with a cut on eta, pT, v=pT+5.360v and ctau
   *  Implements IGenCutTool::applyCut.
   */
  bool applyCut( ParticleVector & theParticleVector ,
                 const HepMC::GenEvent * theGenEvent ,
                 const LHCb::GenCollision * theCollision ) const override;

 private:
  /// Maximum value of eta (set by options)
  Gaudi::Property<double> m_etaMax{this,"EtaMax",4.7,"Maximum value of eta"} ;
  /// Minimum value of eta (set by options)
  Gaudi::Property<double> m_etaMin{this,"EtaMin",2.2,"Minimum value of eta"} ;
  /// Minimum value of pT (set by options)
  Gaudi::Property<double> m_ptMin{this,"PtMin",8.4 * Gaudi::Units::GeV,"Minimum value of pT"} ;
  /// Minimum value of v=pT+5.360eta (set by options)
  Gaudi::Property<double> m_vMin{this,"VMin", 26. * Gaudi::Units::GeV,"Minimum value of v=pT+5.360eta"} ;
  /// Minimum value of ctau
  Gaudi::Property<double> m_ctauMin{this,"CTauMin",0.16 * Gaudi::Units::mm,"Minimum value of ctau"} ;
  /// Minimum value of radius from 0,0 (set by options)
  Gaudi::Property<double> m_radiusMin{this,"RadiusMin",0.0 * Gaudi::Units::mm,"Minimum value of radius from 0,0"} ;
  /// Minimum number of B's (set by options)
  Gaudi::Property<double> m_nBMin{this,"NumberOfBMin",1,"Minimum number of B's"};
  /// Minimum deltaPhi between B's (set by options)
  Gaudi::Property<double> m_deltaPhiMin{this,"MinBDeltaPhi",-1.,"Minimum deltaPhi between B's"};
  /// Minimum deltaPhi between B's
  bool m_deltaPhiCut{false};
  /// PDG Id of charged particle to select
  PIDs m_chargedPIDList ;
  /// Minimum number of B stable charged daugthers
  Gaudi::Property<int> m_nStableChargedDaugthers{this,"MinChargedDaug",-1,"Minimum number of B stable charged daugthers"} ;


};
#endif // GENCUTS_BIASEDBB_H
