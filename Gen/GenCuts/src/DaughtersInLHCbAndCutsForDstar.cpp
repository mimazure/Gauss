/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// Include files

// local
#include "DaughtersInLHCbAndCutsForDstar.h"

// from Gaudi
#include "GaudiKernel/PhysicalConstants.h"
#include "GaudiKernel/Transform4DTypes.h"

// from Kernel
#include "Kernel/ParticleID.h"
#include "GaudiKernel/Vector4DTypes.h"

// from HepMC
#include "HepMC/GenParticle.h"
#include "HepMC/GenVertex.h"

// from Generators
#include "GenEvent/HepMCUtils.h"

//-----------------------------------------------------------------------------
// Implementation file for class : DaughtersInLHCbAndCutsForDstar
//
// 2012-02-07 : Patrick Robbe
//-----------------------------------------------------------------------------

// Declaration of the Tool Factory

DECLARE_COMPONENT( DaughtersInLHCbAndCutsForDstar )

//=============================================================================
// AndWithMinP function
//=============================================================================
bool DaughtersInLHCbAndCutsForDstar::applyCut( ParticleVector & theParticleVector ,
                                               const HepMC::GenEvent * theEvent ,
                                               const LHCb::GenCollision * theHardInfo )
  const {
  if( ! DaughtersInLHCb::applyCut( theParticleVector, theEvent, theHardInfo ) ) {
    return false;
  }
  ParticleVector::iterator it ;

  for ( it = theParticleVector.begin() ; it != theParticleVector.end() ; ) {
    // Check it is a D*+/-
    if ( abs( (*it) -> pdg_id() ) != 413 )
      Exception( "The signal is not a D*+ or D*-" ) ;

    if ( ! passCuts( *it ) ) {
      it = theParticleVector.erase( it ) ;
    } else ++it ;
  }

  return ( ! theParticleVector.empty() ) ;
}

//=============================================================================
// Functions to test if all daughters are in acceptance
//=============================================================================
bool DaughtersInLHCbAndCutsForDstar::passCuts( const HepMC::GenParticle * theSignal ) const {
  HepMC::GenVertex * EV = theSignal -> end_vertex() ;
  if ( 0 == EV ) return true ;

  typedef std::vector< HepMC::GenParticle * > Particles ;
  HepMC::GenParticle * theSoftPion( 0 ) ;
  HepMC::GenParticle * theD0( 0 ) ;
  HepMC::GenParticle * theParent( 0 );
  Particles d0daughters ;

  HepMC::GenVertex::particle_iterator iter ;

  for ( iter = EV -> particles_begin( HepMC::descendants ) ;
        iter != EV -> particles_end( HepMC::descendants ) ; ++iter ) {

    // The D0
    if ( abs( (*iter) -> pdg_id() ) == 421 ) theD0 = (*iter) ;

    // The soft pion
    theParent = *( (*iter) -> production_vertex() -> particles_in_const_begin() ) ;
    if ( ( 413 == abs( theParent -> pdg_id() ) ) && ( 211 == abs( (*iter) -> pdg_id() ) ) )
      theSoftPion = (*iter) ;
  }

  if ( 0 == theD0 )
    Exception( "No D0 in the signal decay chain !" ) ;

  if ( 0 == theSoftPion )
    Exception( "No soft pion in the decay chain !" ) ;

  // daughters of D0
  EV = theD0 -> end_vertex() ;
  if ( 0 == EV )
    Exception( "The D0 has no daughters" ) ;

  for ( iter = EV -> particles_begin( HepMC::descendants ) ;
        iter != EV -> particles_end( HepMC::descendants ) ; ++iter ) {
    // Fill all daughters but exclude photons (can be radiative photons)
    if ( 0 == (*iter) -> end_vertex() )
      if ( 22 != (*iter) -> pdg_id() )
        d0daughters.push_back( *iter ) ;
  }

  if ( d0daughters.empty() )
    Exception( "No D0 daughters in signal chain !" );


  if ( msgLevel( MSG::DEBUG ) ) debug() << "New event" << endmsg ;

  // Now check other cuts
  if ( msgLevel( MSG::DEBUG ) ) debug() << "Check other cuts" << endmsg ;

  if ( msgLevel( MSG::DEBUG ) ) debug() << "D0 pT = " << theD0 -> momentum().perp() << endmsg ;
  if ( theD0 -> momentum().perp() < m_d0ptCut.value() ) return false ;

  if ( m_d0ctauCut.value() > 0. ) {
    // Apply ctau cut
    const HepMC::FourVector& D0fourmomentum = theD0 -> momentum();
    double gamma = D0fourmomentum . e() / D0fourmomentum . m() ;
    double delta_t = theD0 -> end_vertex() -> position() . t() - theD0 -> production_vertex() -> position() . t() ;
    double ctau = Gaudi::Units::c_light * delta_t / gamma ;
    if ( msgLevel( MSG::DEBUG ) ) debug() << "D0 ctau = " << ctau << endmsg ;
    if ( ctau < m_d0ctauCut.value() ) return false ;
  }

  if ( msgLevel( MSG::DEBUG ) ) debug() << "Soft pion pT = " << theSoftPion -> momentum().perp() << endmsg ;
  if ( theSoftPion -> momentum().perp() < m_softpiptCut.value() ) return false ;

  double minpt = 14.*Gaudi::Units::TeV ;
  double maxpt = 0. ;
  double minp  = 14.*Gaudi::Units::TeV ;

  for ( Particles::const_iterator it = d0daughters.begin() ; it != d0daughters.end() ;
        ++it ) {
    if ( msgLevel( MSG::DEBUG ) ) {
      debug() << "Daughter pT = " << (*it) -> momentum().perp()
              << " p = " << (*it) -> momentum().rho() << endmsg ;
    }
    if ( (*it) -> momentum().perp() > maxpt )
      maxpt = (*it) -> momentum().perp() ;
    if ( (*it) -> momentum().perp() < minpt )
      minpt = (*it) -> momentum().perp() ;
    if ( (*it) -> momentum().rho() < minp )
      minp = (*it) -> momentum().rho() ;
  }

  if ( msgLevel( MSG::DEBUG ) ) {
    debug() << "Min Pt = " << minpt
            << " Max Pt = " << maxpt
            << " Min P = " << minp << endmsg ;
  }

  if ( minpt < m_daughtersptminCut.value() ) return false ;
  if ( maxpt < m_daughtersptmaxCut.value() ) return false ;
  if ( minp  < m_daughterspminCut.value()  ) return false ;

  if ( msgLevel( MSG::DEBUG ) ) debug() << "Event passed !" << endmsg ;

  return true ;
}

