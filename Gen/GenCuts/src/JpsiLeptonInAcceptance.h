/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// $Id: JpsiLeptonInAcceptance.h,v 0.1 2013-10-23 18:24:00 jwimberl Exp $
#ifndef GENCUTS_JPSILEPTONINACCEPTANCE_H
#define GENCUTS_JPSILEPTONINACCEPTANCE_H 1

// Include files
// from Gaudi
#include "GaudiAlg/GaudiTool.h"
#include "GaudiKernel/SystemOfUnits.h"

// from Generators
#include "MCInterfaces/IFullGenEventCutTool.h"


/** @class JpsiLeptonInAcceptance JpsiLeptonInAcceptance.h "JpsiLeptonInAcceptance.h"
 *  component/JpsiLeptonInAcceptance.h
 *
 *  Cut on events with jpsi + mu in LHCb acceptance + minimum pT.
 *  and mass in the Bc mass range M(Jpsi) + M(mu) -- M(Bc)
 *  and a liberal DOCA cut, calculated via CTAU
 *
 *  Implementation of IFullGenEventCutTool.
 *
 *  @author Jack Wimberley
 *  @date   2013-10-23
 */

class JpsiLeptonInAcceptance : public extends<GaudiTool ,IFullGenEventCutTool> {
 public:
  /// Standard constructor
  using extends::extends;
  
  /** Apply cut on full event.
   *  Keep events with a jpsi + mu in angular region around
   *  z axis (forward) and with a minimum pT.
   *  Implements IFullGenEventCutTool::studyFullEvent.
   */
  bool studyFullEvent( LHCb::HepMCEvents * theEvents ,
                       LHCb::GenCollisions * theCollisions ) const override;




 private:

  /// Check if the particle lepton is in the detector acceptance, has minimum p and pT.
  bool isInAcceptance( const HepMC::GenParticle*, double pmin, double ptmin, bool angular) const;


  // Particle types of J/psi (or psi(2S), etc.), dilepton, and lepton
  Gaudi::Property<int> m_JpsiPDG{this,"JpsiID",443,"JpsiID"};
  Gaudi::Property<int> m_JpsiLepPDG {this,"JpsiLepID",13,"JpsiLepID"};
  Gaudi::Property<int> m_BachLepPDG {this,"BachLepID",13,"BachLepID"};

  // PT cuts
  Gaudi::Property<double> m_JpsiPtMin{this,"JpsiPtMin",0*Gaudi::Units::GeV,"JpsiPtMin"};
  Gaudi::Property<double> m_JpsiLepPtMin{this,"JpsiLepPtMin",0*Gaudi::Units::MeV,"JpsiLepPtMin"};
  Gaudi::Property<double> m_BachLepPtMin{this,"BachLepPtMin",0*Gaudi::Units::MeV,"BachLepPtMin"};

  // P cuts
  Gaudi::Property<double> m_JpsiPMin{this,"JpsiPMin",0*Gaudi::Units::GeV,"JpsiPMin"};
  Gaudi::Property<double> m_JpsiLepPMin{this,"JpsiLepPMin",2.9*Gaudi::Units::GeV,"JpsiLepPMin"};
  Gaudi::Property<double> m_BachLepPMin{this,"BachLepPMin",2.9*Gaudi::Units::GeV,"BachLepPMin"};

  // Angular acceptance cuts on leptons
  Gaudi::Property<double> m_thetaMin{this,"LepMinTheta",10*Gaudi::Units::mrad,"LepMinTheta"};
  Gaudi::Property<double> m_thetaMax{this,"LepMaxTheta",400*Gaudi::Units::mrad,"LepMaxTheta"};
  Gaudi::Property<double> m_thetaXZMax{this,"LepMaxHorzTheta",500*Gaudi::Units::mrad,"LepMaxHorzTheta"};
  Gaudi::Property<double> m_thetaYZMax{this,"LepMaxVertTheta",500*Gaudi::Units::mrad,"LepMaxVertTheta"};

  // Preselects the events, applying a cut on the trilepton invariant mass
  Gaudi::Property<bool> m_PreselMass{this,"PreselMass", true,"PreselMass"};
  Gaudi::Property<double> m_minMass{this,"MinMass", 3*Gaudi::Units::GeV,"MinMass"};
  Gaudi::Property<double> m_maxMass{this,"MaxMass", 6.5*Gaudi::Units::GeV,"MaxMass"};

  //  Preselects the events applying a cut on the doca of the two tracks
  Gaudi::Property<bool> m_PreselDoca{this,"PreselDoca", true,"PreselDoca"};
  Gaudi::Property<double> m_docaCut{this,"DocaCut", 10.0*Gaudi::Units::mm,"DocaCut"};

};

#endif // GENCUTS_JPSILEPTONINACCEPTANCE_H
