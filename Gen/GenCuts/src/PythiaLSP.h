/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// $Id: PythiaLSP.h,v 1.2 2009-08-14 13:13:22 robbep Exp $
#ifndef GENCUTS_PYTHIALSP_H
#define GENCUTS_PYTHIALSP_H 1

// Include files
// from Gaudi
#include "GaudiAlg/GaudiTool.h"
#include "GaudiKernel/SystemOfUnits.h"

// from Generators
#include "MCInterfaces/IGenCutTool.h"

// from HepMC
#include "HepMC/GenParticle.h"
#include "HepMC/GenEvent.h"

using namespace Gaudi::Units ;
/** @class PythiaLSP PythiaLSP.h component/PythiaLSP.h
 *
 * CutTool to select events with (Susy) LSP satisfying certain cuts.
 * Usage :
 * Generation.Special.CutTool="PythiaLSP";
 * Condition to apply :
 * LSPCond = 1 : LSP in acceptance,
 *         = 2 : all daughters in DgtsInAcc in acceptance
 *         = 3 : all daughters in acceptance
 * Daughters required to be in acceptance :
 * DgtsInAcc = {3,4,5,23,24}, D = {}
 * Generation.Special.PythiaLSP.LSPCond = 1 ; // =d
 * Number of desired LSP :
 * Generation.Special.PythiaLSP.NbLSP = 1 ; //=d
 * Set if at least NbLSP should respect conditions or just NbLSP
 * Generation.Special.PythiaLSP.LSPID = { 1000022, 1000024 };//d=1000022
 * Availabe cuts :
 * Generation.Special.PythiaLSP.DistToPVMin (Max) = x*mm ; // d=0,infty
 * Generation.Special.PythiaLSP.ZPosMin (Max) = y*mm ;// d=-infty,infty
 * Generation.Special.PythiaLSP.EtaMax = 1.8 ; // =d
 * Generation.Special.PythiaLSP.EtaMin = 4.9 ; // =d
 *
 *  @author Neal Gauvin (Gueissaz)
 *  @date   2008-September-1
 */
class PythiaLSP : public extends<GaudiTool, IGenCutTool> {
public:
  /// Standard constructor
  using extends::extends;
  
  bool applyCut( ParticleVector & theParticleVector ,
                 const HepMC::GenEvent * theEvent ,
                 const LHCb::GenCollision * theCollision ) const override;

protected:

private:

  bool IsQuark( HepMC::GenParticle * ) const;
  bool IsLepton( HepMC::GenParticle * ) const;
  bool IsLSP( HepMC::GenParticle * ) const;
  bool IsDgts( HepMC::GenParticle * ) const;
  double Dist( HepMC::GenVertex *, HepMC::GenVertex *) const;
  std::string Print( HepMC::ThreeVector ) const;
  std::string Print( HepMC::FourVector ) const;

  Gaudi::Property<std::vector<int>> m_LSPID{this,"LSPID",{1000022},"LSPID"};
  Gaudi::Property<std::vector<int>> m_Dgts{this,"DgtsInAcc",{},"DgtsInAcc"};
  Gaudi::Property<int> m_NbLSP{this,"NbLSP",1,"NbLSP"};
  Gaudi::Property<int> m_LSPCond{this,"LSPCond",1,"LSPCond"};
  Gaudi::Property<bool> m_AtLeast{this,"AtLeast",true,"AtLeast"};
  Gaudi::Property<double> m_EtaMin{this,"EtaMin",1.8,"EtaMin"};
  Gaudi::Property<double> m_EtaMax{this,"EtaMax",4.9,"EtaMax"};
  Gaudi::Property<double> m_DistToPVMin{this,"DistToPVMin",-1.,"DistToPVMin"};
  Gaudi::Property<double> m_DistToPVMax{this,"DistToPVMax",-1.0,"DistToPVMax"};
  Gaudi::Property<double> m_ZPosMin{this,"ZPosMin",-500.0*mm,"ZPosMin"};
  Gaudi::Property<double> m_ZPosMax{this,"ZPosMax",1.0*km,"ZPosMax"};

};
#endif // GENCUTS_PYTHIALSP_H
