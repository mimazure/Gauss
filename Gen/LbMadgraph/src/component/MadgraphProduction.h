/*****************************************************************************\
* (c) Copyright 2020 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef LBMADGRAPH_MADGRAPHPRODUCTION_H
#define LBMADGRAPH_MADGRAPHPRODUCTION_H 1

// Gaudi.
#include "GaudiKernel/IRndmEngine.h"
#include "GaudiKernel/IRndmGenSvc.h"

// LbHard.
#include "LbHard/HardProduction.h"

// LbMadgraph.
#include "LHAMadgraph.h"

/**
 * Production tool to generate events with Madgraph. This class
 * utilizes the LHAupMadgraph class from Pythia 8.
 *
 * This class automatically generates hard processes with MadGraph 5
 * and aMC@NLO, reads in the LHEF file output, and performs
 * matching. For tree-level generation MLM matching is performed while
 * FxFx is used for aMC@NLO generation.
 *
 * The user can send commands to MadGraph via the "Commands"
 * configurable. Any string begining with "configure " is used for the
 * initial MadGraph configuration with "configure " stripped from the
 * begining. In general, only the process needs to be provided. Run
 * settings must begin with " set" (note the leading space). The
 * output and launch commands, random seed, and shower choice are
 * automatically handled. For example, the following will produce
 * di-muon events from 13 TeV proton proton collisions at NLO in QCD:
 *
 *    readString("generate p p > mu+ mu- [QCD]")
 *
 * The number of events generated per MadGraph run is controlled by
 * the "Events" configurable. In general the default should be
 * sufficient, but for highly inefficient selections, increasing this
 * number is recommended. The maximum number of jets produced by
 * MadGraph (needed for matching) is automatically determined but can
 * be manually specified with "Jets" configurable. In general these
 * configurables should not need to be touched.
 *
 * Events are generated with MadGraph utilizing the "gridpack" method
 * for MadGraph 5:
 *
 *    https://cp3.irmp.ucl.ac.be/projects/madgraph/wiki/GridDevelopment
 *
 * and an eqivalent method for aMC@NLO:
 *
 *    https://answers.launchpad.net/mg5amcnlo/+question/243268
 *
 * The run directory is set as 
 *
 *    <event number>_<beam 1 energy in GeV>_<beam 2 energy in GeV>
 *
 * and does not need to be deleted between independent runs with the
 * same configuration (excluding random seeds). Indeed, keeping the
 * directory significantly speeds the generation process, particularly
 * for NLO generation with aMC@NLO as the grid initialization can be
 * skipped after the initial run. 
 *
 * Gridpacks can be generated with the "doc/gridpack.sh" script using
 * the command:
 *
 *    ./gridpack.sh <beam configuration> <decay file>
 *
 * This will generate a zipped tarball of the gridpack which can then
 * be stored in "Gen/MadgraphData/gridpacks".
 * 
 *    git clone ssh://git@gitlab.cern.ch:7999/lhcb-datapkg/Gen/MadgraphData.git
 *    cp <gridpack>.tgz MadgraphData/gridpacks/
 *    git -am "Added gridpack for <gridpack>."
 *    git push
 *
 * Note that a gridpack is only valid for a given beam energy
 * configuration and event type. Note that a gridpack is not necessary
 * for this tool to run, but it is highly recommended.
 *
 * @class  MadgraphProduction
 * @file   MadgraphProduction.h
 * @author Philip Ilten
 * @date   2018-08-01
 */
class MadgraphProduction : public HardProduction {
public:

  /// Default constructor.
  MadgraphProduction(const std::string &type, const std::string &name,
		     const IInterface *parent);

  /// Default destructor.
  ~MadgraphProduction();
  
  /**
   * Initialize the hard process tool.
   *
   * This is called by HardProduction::initialize prior to
   * initialization of the shower. Here, the Pythia 8 LHAup pointer is
   * created, and the Madgraph initialization is performed.
   */
  StatusCode hardInitialize() override;

  /// Generate an event.
  StatusCode generateEvent(HepMC::GenEvent *theEvent,
			   LHCb::GenCollision *theCollision) override;

  /// Finalize.
  StatusCode finalize() override;

private:

  // Members.
  IRndmGenSvc             *m_randSvc;   ///< Random number service.
  Pythia8::LHAupMadgraph  *m_madgraph;  ///< The Madgraph LHAup object.
  int                      m_seed;      ///< Current random seed.
  int                      m_events;    ///< Number of events per Madgraph run.
  int                      m_jets;      ///< Number of fixed order jets.
  std::vector<std::string> m_pdf;       ///< Default PDF commands.
  int                      m_eventType; ///< Event type.
  bool                     m_genGridpack; ///< Gridpack generation flag
};

#endif // LBMADGRAPH_MADGRAPHPRODUCTION_H
