/*****************************************************************************\
* (c) Copyright 2020 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// Gaudi.
#include "GaudiKernel/SystemOfUnits.h"
#include "Kernel/ParticleProperty.h"

// Generators.
#include "Generators/IBeamTool.h"

// LbMadgraph.
#include "MadgraphProduction.h"

//-----------------------------------------------------------------------------
//  Implementation file for class: MadgraphProduction
//
//  2018-08-01 : Philip Ilten
//-----------------------------------------------------------------------------

// Declaration of the tool factory.
DECLARE_COMPONENT( MadgraphProduction )

//=============================================================================
// Default constructor.
//=============================================================================
MadgraphProduction::MadgraphProduction(const std::string &type,
				       const std::string &name,
				       const IInterface *parent)
: HardProduction (type, name, parent), m_randSvc(0), m_madgraph(0), m_seed(0){

  // Declare the tool properties.
  declareInterface<IProductionTool>(this);
  declareProperty("Events", m_events = 10000,
		  "Number of events to produce per Madgraph run.");
  declareProperty("Jets", m_jets = -1,
		  "Number of jets produced at the highest fixed order. "
		  "If -1, then try to determine the number of jets.");
  declareProperty("PdfCommands",
		  m_pdf = {" set pdlabel lhapdf", " set lhaid 10770"},
		  "Default PDF commands when no user PDF commands specified.");
  declareProperty("EventType", m_eventType = 0,
		  "Event type, needed when searching for a gridpack.");
  declareProperty("GenGridpack", m_genGridpack = false,
		  "Flag for gridpack generation (only to be used by experts)"
		  "If == true, no gridpack search is performed.");
}

//=============================================================================
// Default destructor.
//=============================================================================
MadgraphProduction::~MadgraphProduction() {
  // No deletion of m_madgraph is required, as this is called by
  // Pythia8Production::finalize().
}

//=============================================================================
// Initialize the hard process tool.
//=============================================================================
StatusCode MadgraphProduction::hardInitialize() {

  // Set the Madgraph module path.
  std::string mgdpath(getenv("MADGRAPHDATAROOT")), pypath(getenv("PYTHONPATH"));
  setenv("PYTHONPATH", (char*)(pypath + ":" + mgdpath + "/modules").c_str(), 1);

  // Set the run directory.
  if (!m_beamTool) return Error("Beam tool not initialized.");
  Gaudi::XYZVector pBeam1, pBeam2;
  m_beamTool->getMeanBeams(pBeam1, pBeam2);
  std::string rundir = std::to_string(abs(m_eventType)) + "_" + 
    std::to_string(int(sqrt(pBeam1.Mag2()) / Gaudi::Units::GeV)) + "_" +
    std::to_string(int(sqrt(pBeam2.Mag2()) / Gaudi::Units::GeV));
  
  // Find and expand the gridpack, if available.
  if (!m_genGridpack) {
    std::string gridpack = mgdpath + "/gridpacks/" + getenv("CMTCONFIG") + "/"
      + rundir + ".tgz";
    if (access(gridpack.c_str(), F_OK) != -1) {
      system(("tar -xzf" + gridpack).c_str());
      system(("find " + rundir + " -name PDFsets | xargs -i sh -c \'unlink {} ; ln -s $LHAPDFSETSDATA {}\'").c_str());
    }
    if (access(rundir.c_str(), F_OK) == -1)
      Exception("Gridpack required but not found. To generate a gridpack see "
        "https://gitlab.cern.ch/lhcb-datapkg/Gen/MadgraphData "
        "for more details and instructions.");
  }
  
  // Retrieve the Pythia 8 production tool.
  if (!m_pythia8) m_pythia8 = dynamic_cast<Pythia8Production*>
                    (tool<IProductionTool>("Pythia8Production", this));
  if (!m_pythia8) return Error("Failed to retrieve Pythia8Production tool.");
  m_hard = m_pythia8;
  m_pythia8->m_beamToolName = m_beamToolName;

  // Initialize the random number service.
  try {m_randSvc = svc<IRndmGenSvc>("RndmGenSvc", true);}
  catch (const GaudiException& e) 
    {Exception("Failed to initialize the RndmGenSvc.");}
  
  // Set Pythia 8 LHAup and UserHooks (tool automatically sets its own).
  if (m_madgraph) delete m_madgraph;
  m_madgraph = new Pythia8::LHAupMadgraph
    (m_pythia8->m_pythia, true, rundir, getenv("MADGRAPHEXE"));
  m_lhaup = m_madgraph;
  for (std::vector<int>::size_type hooks = 0; hooks < (m_pythia8->m_hooks.size()); ++hooks) {
    if (m_pythia8->m_hooks[hooks]) {
      m_pythia8->m_hooks[hooks]; m_pythia8->m_hooks[hooks] = 0;
    }
  }
  m_pythia8->m_pythia->settings.mode("Random:seed", 1);
  
  // Default process.
  if (!m_userSettings.size())
    m_userSettings.value().push_back("generate p p > mu+ mu-");

  // Set number of cores to one in normal running mode.
  if (m_eventType > 0) {
    m_userSettings.value().push_back("configure run_mode = 0");
    m_userSettings.value().push_back("configure nb_core = 1");
  }

  // Set the PDF (do not override user PDF settings if present).
  bool setPdf(true);
  for (std::string const &setting : m_userSettings) {
    if (setting.find("pdflabel") != std::string::npos) {setPdf = false; break;}
    if (setting.find("lhaid") != std::string::npos) {setPdf = false; break;}
  }
  if (setPdf) {
    for (std::string const &setting : m_pdf) m_userSettings.value().push_back(setting);
  }
  
  // Set the energy.
  m_userSettings.value().push_back
    (" set ebeam1 " + std::to_string(sqrt(pBeam1.Mag2()) / Gaudi::Units::GeV));
  m_userSettings.value().push_back
    (" set ebeam2 " + std::to_string(sqrt(pBeam2.Mag2()) / Gaudi::Units::GeV));
  
  // Read user settings.
  for (std::string const &setting : m_userSettings) {
    debug() << setting << endmsg;
    if (!m_madgraph->readString(setting))
      Warning ("Failed to read the command " + setting + ".").ignore();
  } 
  m_madgraph->readString("", Pythia8::LHAupMadgraph::Configure);
  m_madgraph->setJets(m_jets);
  m_madgraph->setEvents(1);
  // Set seed negative so Madgraph is rerun in generateEvent.
  if (m_seed) {m_seed = -abs(m_seed + 1);}
  return StatusCode::SUCCESS;
}

//=============================================================================
// Generate an event.
//=============================================================================
StatusCode MadgraphProduction::generateEvent(HepMC::GenEvent *theEvent,
					     LHCb::GenCollision *theCollision) {

  // Set the first seed if needed.
  if (!m_seed) {
    std::vector<long> seeds;
    m_randSvc->engine()->seeds(seeds).ignore();
    m_seed = -abs(seeds[0]);
  }

  // If seed has been changed, force rerun of Madgraph.
  if (m_seed < 0) {
    m_seed = abs(m_seed);
    m_madgraph->setEvents(m_events);
    m_madgraph->setSeed(m_seed);
    if (!m_madgraph->run(m_events)) return StatusCode::FAILURE;
    if (!m_madgraph->reader(false)) return StatusCode::FAILURE;
  }
  
  // Run Pythia 8.
  if (!m_shower) return StatusCode::FAILURE;
  ++m_nEvents;
  return m_shower->generateEvent(theEvent, theCollision);
}

//=============================================================================
// Finalize the tool.
//=============================================================================
StatusCode MadgraphProduction::finalize() {
  always() << m_madgraph->stat() << endmsg;
  return HardProduction::finalize();
}

//=============================================================================
// The END.
//=============================================================================
