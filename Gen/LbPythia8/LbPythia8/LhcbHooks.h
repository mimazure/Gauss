/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef LBPYTHIA8_LHCBHOOKS_H
#define LBPYTHIA8_LHCBHOOKS_H 1

#include "Pythia8/Pythia.h"

/** @class pTDampingHook LhcbHooks.h LbPythia8/LhcbHooks.h
 *
 *  Custom LHCb user hooks used to modify default Pythia 8 event generation.
 *  Currently the hooks are used to provide pT damping of the QCD 2 -> 2
 *  processes.
 *
 *  @author Philip Ilten
 *  @date   2014-04-06
 */

namespace Pythia8 {
  class pTDampingHook : public UserHooks {

  public:

    /// Constructor.
    pTDampingHook() {isInit = false;}

    /// Modifiy cross-section.
    bool canModifySigma() override {return true;}

    /// Cross-section weight.
    double multiplySigmaBy(const SigmaProcess* sigmaProcessPtr,
				   const PhaseSpace* phaseSpacePtr, bool) override;

  private:

    // Members.
    bool   isInit;      ///< Flag whether the object has been initialized.
    double pT20;        ///< Stored value of the pT damping parameter.
    AlphaStrong alphaS; ///< Stored value of alpha strong.
  };

/** @class InclusivebHook LhcbHooks.h LbPythia8/LhcbHooks.h
 *  Unbiased Inclusive hook which will veto the unwanted events at early 
 *  stages of evolution 
 */

   class InclusivebHook : public UserHooks {

    public:
      /// Constructor.
      InclusivebHook() {isInit = false;}
      
      /// Destructor
      ~InclusivebHook(){}

    ///to check the event after the hardest interaction
    bool canVetoMPIStep() override {return true;}

    /// to look the event at MPI_1 stage which is soft QCD process stage
    int numberVetoMPIStep() override {return 1;}

    /// Decide whether to veto current event or not
    bool doVetoMPIStep(int nMPI, const Event& event) override;


    /// Allow a veto for the interleaved evolution in pT
    bool canVetoPT() override {return true;}

    /// Transverse-momentum scale for veto test for heavy quark.
    double scaleVetoPT() override {return pTHatThreshold;}

    /// Decide whether to veto current event or not
    bool doVetoPT( int iPos , const Event& event ) override;

  private:

    // Members
    bool   isInit;                    ///< Flag whether the object has been initialized.
    double pTHatVal;   		            ///< value of pT Hat
    bool   hardB;  			              ///< Flag whether there is a b quark at process level
    bool   showerB;			              ///< Flag whether there is a b quark at parton level
    double pTHatThreshold;            ///< value of pT Hat where you want to access the event in the interleaved evolution
    const int quarkID = 5;            ///< pdg ID of b/bbar quark we are looking for                                        
  };
}

#endif // LBPYTHIA8_LHCBHOOKS_H
