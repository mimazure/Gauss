/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef PARTICLEGUNS_BEAMSHAPE_H
#define PARTICLEGUNS_BEAMSHAPE_H 1

// Include files
// from Gaudi
#include "GaudiAlg/GaudiTool.h"
#include "GaudiKernel/RndmGenerators.h"
#include "GaudiKernel/SystemOfUnits.h" 

// from ParticleGuns
#include "LbPGuns/IParticleGunTool.h"

/** @class BeamShape BeamShape.h "BeamShape.h"
 *
 *  @author M. Lieng
 *  @date 2009-10-23
 */
class BeamShape : public extends<GaudiTool , IParticleGunTool> {
 public:

  /// Constructor
  using extends::extends;
  /// Initialize method
  StatusCode initialize() override;

  /// Generate the particle
  void generateParticle( Gaudi::LorentzVector & fourMomentum ,
                         Gaudi::LorentzVector & origin ,
                         int & pdgId ) override;

  /// Print counters
  void printCounters( ) override { ; } ;

 private:
  // Setable Properties
  Gaudi::Property<double> m_xCenter{this,"xCenter",0.0 * Gaudi::Units::mm ,"Beam center position"};   // Beam center position
  Gaudi::Property<double> m_yCenter{this,"yCenter",0.0 * Gaudi::Units::mm ,"Beam center position"};   // Beam center position
  Gaudi::Property<double> m_zCenter{this,"zCenter",-1000.0 * Gaudi::Units::mm ,"Production point"};   // Production point
  Gaudi::Property<int>    m_zDir{this,"BeamDirection",1,"Beam direction (Beam 1: 1, Beam 2: -1)"};         // Beam direction (Beam 1: 1, Beam 2: -1)
  Gaudi::Property<double> m_theta{this,"ThetaCenter",0.0 * Gaudi::Units::rad ,"Beam angle (inclination)"};     // Beam angle (inclination)
  Gaudi::Property<double> m_phi{this,"PhiCenter",0.0 * Gaudi::Units::rad ,"Beam angle (azimuth)"};       // Beam angle (azimuth)
  Gaudi::Property<double> m_momentum{this,"Momentum",450.0 * Gaudi::Units::GeV ,"Beam particle momentum"};  // Beam particle momentum
  Gaudi::Property<double> m_xEmm{this,"xEmmitance",7.82e-9 * Gaudi::Units::m,"Transverse emmitance"};      // Transverse emmitance
  Gaudi::Property<double> m_yEmm{this,"yEmmitance",7.82e-9 * Gaudi::Units::m,"Transverse emmitance"};      // Transverse emmitance
  Gaudi::Property<double> m_xBeta{this,"xBeta",10.0 * Gaudi::Units::m ,"Optical beta function"};     // Optical beta function
  Gaudi::Property<double> m_yBeta{this,"yBeta",10.0 * Gaudi::Units::m ,"Optical beta function"};     // Optical beta function
  Gaudi::Property<double> m_xAlpha{this,"xAlpha",4.18e-7 ,"Correlation function"};    // Correlation function
  Gaudi::Property<double> m_yAlpha{this,"yAlpha",1.18e-7 ,"Correlation function"};    // Correlation function
  Gaudi::Property<int>    m_pdgCode{this,"PdgCode",2212 ,"PDG particle id number"};     // PDG particle id number

	// Local Member Data:
  double m_xSigma{0.};
  double m_ySigma{0.};
  double m_pxCenter{0.};
  double m_pyCenter{0.};
  double m_pxSigma{0.};
  double m_pySigma{0.};
  double m_mass{0.};

  // Random generators:
  Rndm::Numbers m_flatGenerator ;
  Rndm::Numbers m_gaussGenerator ;
} ;

#endif // PARTICLEGUNS_BEAMSHAPE_H
