//--------------------------------------------------------------------------
//
// Copyright Information: See EvtGen/COPYRIGHT
//
// Module: EvtBnoCB0to4piCP
//
// Description: Model for generating flat phase space for the charmless 
//              region with time-dependent CP violation. Necessary due to 
//              limitations with generator-level cuts in LoKi; inability to 
//              handle Bose-symmetric final states with PHOTOS
//
// Modification history:
//
//    Jeremy Dalseno     Feb 2019     Module created
//
//------------------------------------------------------------------------

#include "EvtGenBase/EvtParticle.hh"
#include "EvtGenBase/EvtPDL.hh"
#include "EvtGenBase/EvtCPUtil.hh"

#include "EvtGenModels/EvtBnoCB0to4piCP.hh"

// Constructor defining parameters from CKMfitter and PDG 2018
EvtBnoCB0to4piCP::EvtBnoCB0to4piCP() :
    _phi2(1.507964474),
    _Dmd(0.5064e12),
    _m2pi_ll(0.0),
    _m2pi_ul(1.86483),
    _m3pi_ll(0.0),
    _m3pi_ul(1.86965),
    _cPhi2(cos(2.0*_phi2)),
    _sPhi2(sin(2.0*_phi2)),
    _I(EvtComplex(0.0, 1.0))
{
}

void EvtBnoCB0to4piCP::init()
{
  // Check there are 0 or 6 arguments
  checkNArg(0,6);
  checkNDaug(4);

  // Check initial and final state particles
  if (getParentId() != EvtPDL::getId("B0") &&
      getParentId() != EvtPDL::getId("anti-B0")) {
      EvtGenReport(EVTGEN_ERROR,"EvtGenExtras") << getModelName()
						<< " generator expects initial state to be either B0 or anti-B0"
						<< std::endl;
      ::abort();
  }

  for (unsigned int i = 0; i < static_cast<unsigned int>(getNDaug()); ++i) {
      if (getDaug(i) != EvtPDL::getId("pi+") && getDaug(i) != EvtPDL::getId("pi-")) {
	  EvtGenReport(EVTGEN_ERROR,"EvtGenExtras") << getModelName()
						    << " generator expects final state particle " << i
						    << " to be either pi+ or pi-" << std::endl;
	  ::abort();
      }
  }

  // Assign arguments
  if (getNArg() == 6) {

    // Time-dependent CP violation
    _phi2 = getArg(0);
    _Dmd = getArg(1);

    double twoPhi2 = 2.0*_phi2;
    _cPhi2 = cos(twoPhi2);
    _sPhi2 = sin(twoPhi2);

    // Lower and upper 2pi invariant mass limits    
    _m2pi_ll = getArg(2);
    _m2pi_ul = getArg(3);

    // Lower and upper 3pi invariant mass limits
    _m3pi_ll = getArg(4);
    _m3pi_ul = getArg(5);

  }

}

void EvtBnoCB0to4piCP::decay(EvtParticle* p)
{
  // Generate 4-vectors
  p->initializePhaseSpace( getNDaug(), getDaugs() );

  // Apply charmless selection
  const EvtVector4R p1 = p->getDaug(0)->getP4();
  const EvtVector4R p2 = p->getDaug(1)->getP4();
  const EvtVector4R p3 = p->getDaug(2)->getP4();
  const EvtVector4R p4 = p->getDaug(3)->getP4();

  const double m12 = (p1+p2).mass();
  const double m14 = (p1+p4).mass();
  const double m23 = (p2+p3).mass();
  const double m34 = (p3+p4).mass();

  const double m123 = (p1+p2+p3).mass();
  const double m124 = (p1+p2+p4).mass();
  const double m134 = (p1+p3+p4).mass();
  const double m234 = (p2+p3+p4).mass();

  EvtComplex amp(0.0,0.0);

  if ( ((m12 > _m2pi_ll && m12 < _m2pi_ul) &&
	(m34 > _m2pi_ll && m34 < _m2pi_ul)) ||
       ((m14 > _m2pi_ll && m14 < _m2pi_ul) &&
	(m23 > _m2pi_ll && m23 < _m2pi_ul)) ||
       (m123 > _m3pi_ll && m123 < _m3pi_ul) ||
       (m124 > _m3pi_ll && m124 < _m3pi_ul) ||
       (m134 > _m3pi_ll && m134 < _m3pi_ul) ||
       (m234 > _m3pi_ll && m234 < _m3pi_ul) ) {

      // Generate time distribution with mixing
      EvtId other_b;
      double t(0.0);
      EvtCPUtil::getInstance()->OtherB(p, t, other_b, 0.5);

      double arg = 0.5*_Dmd*t/EvtConst::c;

      if (other_b == EvtPDL::getId("anti-B0")) {
	  amp = EvtComplex(_cPhi2, -_sPhi2);
      } else {
	  amp = EvtComplex(_cPhi2, _sPhi2);
      }

      amp = amp*_I*sin(arg) + cos(arg);

  }

  vertex(amp);

}
