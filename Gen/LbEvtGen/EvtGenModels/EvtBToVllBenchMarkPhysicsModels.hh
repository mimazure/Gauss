/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef BENCHMARKPHYSICSMODELS_H_
#define BENCHMARKPHYSICSMODELS_H_

#include "EvtBToVllPhysicsModel.hh"
#include "EvtBToVllWC.hh"
#include "EvtBToVllQCDUtils.hh"

namespace qcd{

class SMPhysicsModel : public IPhysicsModel{
public:
	std::string getModelName() const override {
		return "Standard_Model";
	}
	qcd::WCPtr getLeftWilsonCoefficientsMW() const override;
	qcd::WCPtr getLeftNewPhysicsDeltasMW() const override {
		return getZeroWCs();
	}
	qcd::WCPtr getRightWilsonCoefficientsMW() const override {
		return getZeroWCs();
	}
protected:
	qcd::WCPtr getZeroWCs() const;
};

class LHTPhysicsModel : public SMPhysicsModel {
public:
	std::string getModelName() const override {
			return "LHT_Model";
	}
	qcd::WCPtr getLeftNewPhysicsDeltasMW() const override;
};

class UEDPhysicsModel : public SMPhysicsModel {
public:
	std::string getModelName() const override {
			return "UED_Model";
	}
	qcd::WCPtr getLeftNewPhysicsDeltasMW() const override;
};

class FBMSSMPhysicsModel : public SMPhysicsModel {
public:
	std::string getModelName() const override {
			return "FBMSSM_Model";
	}
	qcd::WCPtr getLeftNewPhysicsDeltasMW() const override;
	qcd::WCPtr getRightWilsonCoefficientsMW() const override;
};

class GMSSMPhysicsModel : public SMPhysicsModel {
public:
	std::string getModelName() const override {
			return "GMSSM_Model";
	}
	qcd::WCPtr getLeftNewPhysicsDeltasMW() const override;
	qcd::WCPtr getRightWilsonCoefficientsMW() const override;
};

}
#endif /*BENCHMARKPHYSICSMODELS_H_*/
