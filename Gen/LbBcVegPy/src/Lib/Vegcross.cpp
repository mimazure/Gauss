/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// $Id: Vegcross.cpp,v 1.1.1.1 2006-04-24 21:45:50 robbep Exp $
// access BcGen common Vegcross
#include "LbBcVegPy/Vegcross.h"

// set pointer to zero at start
Vegcross::VEGCROSS* Vegcross::s_vegcross =0;

// Constructor
Vegcross::Vegcross() : m_dummy( 0 ) , m_realdummy( 0. ) { }

// Destructor
Vegcross::~Vegcross() { }

// access vegsec in common
double& Vegcross::vegsec() {
  init(); // check COMMON is initialized
  return s_vegcross->vegsec;
}

// access vegerr in common
double& Vegcross::vegerr() {
  init(); // check COMMON is initialized
  return s_vegcross->vegerr;
}

// access iveggrade in common
int& Vegcross::iveggrade() {
  init(); // check COMMON is initialized
  return s_vegcross->iveggrade;
}
