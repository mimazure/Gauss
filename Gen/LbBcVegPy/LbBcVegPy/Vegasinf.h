/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// $Id: Vegasinf.h,v 1.2 2006-05-03 08:24:32 robbep Exp $

#ifndef LBBCVEGPY_VEGASINF_H
#define LBBCVEGPY_VEGASINF_H 1

#ifdef WIN32
extern "C" {
  void* __stdcall VEGASINF_ADDRESS(void) ;
}
#else
extern "C" {
  void* vegasinf_address__(void) ;
}
#endif

class Vegasinf {
public:
  Vegasinf();
  ~Vegasinf();

  int& number();
  int& nitmx();

  inline void init(); // inlined for speed of access (small function)

private:
  struct VEGASINF;
  friend struct VEGASINF;
  
  struct VEGASINF {
    int number;
    int nitmx;
  };
  int m_dummy;
  double m_realdummy;
  static VEGASINF* s_vegasinf;
};

// Inline implementations for Vegasinf
// initialise pointer
#ifdef WIN32
void Vegasinf::init(void) {
  if ( 0 == s_vegasinf ) s_vegasinf = static_cast<VEGASINF*>(VEGASINF_ADDRESS());
}
#else
void Vegasinf::init(void) {
  if ( 0 == s_vegasinf ) s_vegasinf = static_cast<VEGASINF*>(vegasinf_address__());
}
#endif
#endif // LBBCVEGPY_VEGASINF_H
 
