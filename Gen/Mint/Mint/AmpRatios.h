/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef DALITZ_AMP_RATIOS_HH
#define DALITZ_AMP_RATIOS_HH
// author: Jonas Rademacker (Jonas.Rademacker@bristol.ac.uk)
// status:  Mon 9 Feb 2009 19:18:05 GMT

#include "Mint/DalitzMCIntegrator.h"
#include "Mint/DalitzEventPattern.h"
#include "Mint/FitAmpSum.h"

class AmpRatios{

  DalitzMCIntegrator _integrator;
  DalitzEventPattern _pattern;

 public:
  AmpRatios(const DalitzEventPattern& pattern);

  bool getRatios(FitAmpSum& ampSum);
};

#endif
//
