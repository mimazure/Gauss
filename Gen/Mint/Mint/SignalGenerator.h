/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef SIGNAL_GENERATOR_HH
#define SIGNAL_GENERATOR_HH
// author: Jonas Rademacker (Jonas.Rademacker@bristol.ac.uk)
// status:  Mon 9 Feb 2009 19:17:59 GMT

#include "Mint/IEventGenerator.h"
#include "Mint/IDalitzEvent.h"
#include "Mint/IDalitzEventList.h"

#include "Mint/BaseGenerator.h"

#include "Mint/DalitzEventPattern.h"
#include "Mint/MinuitParameterSet.h"

#include "Mint/DalitzEventList.h"
#include "Mint/DiskResidentEventList.h"

#include "Mint/IFastAmplitudeIntegrable.h"
#include "Mint/DalitzBWBoxSet.h"

#include "Mint/counted_ptr.h"

#include <string>

class SignalGenerator
: public BaseGenerator
, virtual public MINT::IEventGenerator<IDalitzEvent>{
 protected:
  MINT::MinuitParameterSet _myOwnPSet;
  MINT::counted_ptr<IFastAmplitudeIntegrable> _counted_amps;
  IFastAmplitudeIntegrable* _amps;
  MINT::counted_ptr<MINT::IUnweightedEventGenerator<IDalitzEvent> >  _boxes;

  bool makeBoxes();
 public:
  SignalGenerator(const DalitzEventPattern& pat, TRandom* rnd=gRandom);
  SignalGenerator(IFastAmplitudeIntegrable* amps, TRandom* rnd=gRandom);
  SignalGenerator(const DalitzEventPattern& pat
		  , double rB
		  , double phase
		  , TRandom* rnd=gRandom);
  virtual MINT::counted_ptr<IDalitzEvent> tryDalitzEvent();
  virtual MINT::counted_ptr<IDalitzEvent> newDalitzEvent();

  // this one below is required by MINT::IEventGenerator<IDalitzEvent>
  // similar to above, except it's a pointer to
  // IDalitzEvent, not DalitzEvent.
  //virtual MINT::counted_ptr<IDalitzEvent> newEvent();
  MINT::counted_ptr<IDalitzEvent> newEvent() override;

  bool exhausted() const override {return false;}

  bool ensureFreshEvents() override;
};

#endif
//
