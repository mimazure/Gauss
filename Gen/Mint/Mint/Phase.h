/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef PHASE_HH
#define PHASE_HH
// author: Jonas Rademacker (Jonas.Rademacker@bristol.ac.uk)
// status:  Mon 9 Feb 2009 19:17:57 GMT

#include <iostream>

namespace MINT{
class Phase{
 protected:
  double _ph;
  const static double _minPh;
  void toRange();
 public:
  Phase(const Phase& other);
  Phase(double ph_in=0);
  operator double() const{
    return _ph;
  }

  static double rangeMax();
  static double rangeMin();

  Phase& operator*=(double rhs);
  Phase& operator/=(double rhs);
  Phase& operator+=(const Phase& rhs);
  Phase  operator+ (const Phase& rhs) const;
  const Phase&  operator+ () const;
  Phase& operator-=(const Phase& rhs);
  Phase  operator- (const Phase& rhs) const;
  Phase  operator- () const;

  double inDegrees() const;
  void testPhase(std::ostream& os = std::cout) const;
};
}//namespace MINT

#endif
