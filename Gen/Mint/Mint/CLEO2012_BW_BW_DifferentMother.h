/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef CLEO2012_BW_BW_DIFFERENT_MOTHER_HH
#define CLEO2012_BW_BW_DIFFERENT_MOTHER_HH
// author: Jonas Rademacker (Jonas.Rademacker@bristol.ac.uk)
// status:  Mon 9 Feb 2009 19:18:04 GMT

#include "Mint/CLEO2012_BW_BW.h"

// used for CrystalBarrelRhoOmega
class ParticlePropeties;

class CLEO2012_BW_BW_DifferentMother : public CLEO2012_BW_BW, virtual public ILineshape{
 protected:
  int _alternativeMumPDG;
  const ParticleProperties* mumsProperties() const override;
 public:
  CLEO2012_BW_BW_DifferentMother(const AssociatedDecayTree& decay
				 , IDalitzEventAccess* events
				 , int newMumID
				 );
};

#endif
//
