/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef NONRES_LINESHAPE_HH
#define NONRES_LINESHAPE_HH
// author: Nunya Business (ya.mum@bristol.ac.uk)
// status:  Mon 22 Jul 2014 19:17:59 GMT

#include <complex>

#include "Mint/ILineshape.h"
#include "Mint/BW_BW.h"
#include "Mint/NamedParameter.h"

class NonRes : public BW_BW, virtual public ILineshape{
 public:
  /**
     Constructor
   */
  NonRes( const AssociatedDecayTree& tree,
	  IDalitzEventAccess* events,
	  const std::string& type="" );

  /**
     Evaluate rho0-omega lineshape from the Crystal Barrel measurement
   */
  std::complex<double> getVal() override;

  /**
     Print decay
   */
  void print( std::ostream& out = std::cout ) const override;

  /**
     Print decay
   */
  void print( std::ostream& out = std::cout ) override;

  /**
     Decay Name
   */
  std::string name() const override {
    return "NonRes("+_theDecay.oneLiner() +")";
  }

  /**
     Destructor
   */
  virtual ~NonRes(){}

 protected:
  /**
     Return shape parameter alpha
     Not possible to vary this in the fit in the current implementation
   */
  double GetAlpha() const;

  const std::string _type;
  MINT::NamedParameter<double> _alpha;
};

std::ostream& operator<<( std::ostream& out, const NonRes& amp );

#endif
//
