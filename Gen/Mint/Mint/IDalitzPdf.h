/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef MINTDALITZ_IDALITZ_PDF_HH
#define MINTDALITZ_IDALITZ_PDF_HH

#include "Mint/IGetRealEvent.h"
#include "Mint/IDalitzEvent.h"
#include "Mint/IPdf.h"
#include "Mint/DalitzHistoSet.h"

class IDalitzPdf
: virtual public MINT::IGetRealEvent<IDalitzEvent>
, virtual public MINT::IPdf<IDalitzEvent>
{
 public:
  double getVal() override =0;
  virtual double getVal_noPs()=0;
  virtual double getVal_withPs()=0;

  virtual double getVal(IDalitzEvent* evt)=0;
  virtual double getVal_noPs(IDalitzEvent* evt)=0;
  virtual double getVal_withPs(IDalitzEvent* evt)=0;
    virtual DalitzHistoSet histoSet()=0;
  //virtual double RealVal()=0;
};

#endif
//
