/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef MINT_SYM_MULTI_POLY_TERM_HH
#define MINT_SYM_MULTI_POLY_TERM_HH

#include "Mint/symPolyTerm.h"

#include <vector>
#include <iostream>

class symMultiPolyTerm : public std::vector< symPolyTerm >{

  bool createTerms(int dimension, int order);
  bool createTerms(int dimension, int order
		   , std::vector<std::vector<int> >& v);
  bool init(int dimension, int order);

 public:
  symMultiPolyTerm(int dimension, int order=0);
  symMultiPolyTerm(const symMultiPolyTerm& other);

  virtual ~symMultiPolyTerm(){}

  void print(std::ostream& os=std::cout) const;
};

std::ostream& operator<<(std::ostream& os, const symMultiPolyTerm& smpt);

#endif
//
