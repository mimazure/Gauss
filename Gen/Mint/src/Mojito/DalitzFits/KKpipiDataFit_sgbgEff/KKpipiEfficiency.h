/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef KKPIPI_EFFICIENCY_HH
#define KKPIPI_EFFICIENCY_HH

#include <string>
#include "IGetDalitzEvent.h"
#include "DalitzEventAccess.h"

class TH1F;
class TFile;
class IDalitzEventAccess;

class KKpipiEfficiency : virtual public IGetDalitzEvent, public DalitzEventAccess{
  TH1F* _hKFast, *_hKSlow, *_hPiFast, *_hPiSlow;
  double _pKFast, _pKSlow, _pPiFast, _pPiSlow;

  void resetP();
  bool getP();
  static double oneEff(TH1F* h, double p);
  static TH1F* get_h(TFile* f, const std::string& hname);
  double simple();
  double eff();

public:
  KKpipiEfficiency(const std::string& fname="Ratios.root"
		   , IDalitzEventAccess* daddy=0);

  virtual double RealVal();
};


#endif
//
