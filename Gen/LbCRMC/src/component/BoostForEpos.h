/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef LBCRMC_BOOSTFOREPOS_H
#define LBCRMC_BOOSTFOREPOS_H 1

// Include files
// from Gaudi
#include "GaudiAlg/GaudiAlgorithm.h"
#include "GaudiKernel/Transform4DTypes.h"
#include "GaudiKernel/Vector4DTypes.h"

// HepMC
#include "Event/HepMCEvent.h"

/** @class BoostForEpos BoostForEpos.h
 *
 *
 *  @author Patrick Robbe
 *  @date   2016-10-21
 */
class BoostForEpos : public GaudiAlgorithm {
public:
  /// Standard constructor
  using GaudiAlgorithm::GaudiAlgorithm;

  StatusCode initialize() override;    ///< Algorithm initialization
  StatusCode execute   () override;    ///< Algorithm execution
  StatusCode finalize  () override;    ///< Algorithm finalization

protected:

private:
  Gaudi::Property<std::string> m_inputHepMCEvent{this,"InputHepMCEvent",LHCb::HepMCEventLocation::Default,"HepMC event location"}  ;
  Gaudi::Property<double> m_px{this,"p_x",0.,"Px component of Boost four-vector"} ;
  Gaudi::Property<double> m_py{this,"p_y",0.,"Py component of Boost four-vector"} ;
  Gaudi::Property<double> m_pz{this,"p_z",0.,"Pz component of Boost four-vector"} ; 
  Gaudi::Property<double> m_e{this,"e",0.,"E component of Boost four-vector"} ;
  ROOT::Math::Boost m_boost ;
};
#endif // LBCRMC_BOOSTFOREPOS_H
