/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef LBCRMC_CRMCPRODUCTION_H
#define LBCRMC_CRMCPRODUCTION_H 1

#include "GaudiAlg/GaudiTool.h"

#include "GaudiKernel/RndmGenerators.h"

#include "Generators/IProductionTool.h"
//epos
#include "LbCRMC/CRMCWrapper.h"
// forward declarations
class CRMCWrapper ;
class IBeamTool ;


//-----------------------------------------------------------------------------
//  Interface file for class: CRMCProduction
//
//  2014-02-03 : Dmitry Popov
// 2015-08-25 :modification to go from EPOS Fortan to HepMC C++ (Laure Massacrier)
//-----------------------------------------------------------------------------


class CRMCProduction : public extends<GaudiTool,IProductionTool> {

public:
  typedef std::vector<std::string> CommandVector;

  using extends::extends;

  // Tool initialization
  StatusCode initialize() override;

  // Tool finilization
  StatusCode finalize() override;

  // Initialize the generator
  StatusCode initializeGenerator() override;

  // Generate an event
  StatusCode generateEvent(HepMC::GenEvent *theEvent, LHCb::GenCollision *theCollision) override;

  // This method is not implemented
  void setStable(const LHCb::ParticleProperty *thePP) override;

  // This method is not implemented
  void updateParticleProperties(const LHCb::ParticleProperty *thePP) override;

  // This method is not implemented
  bool isSpecialParticle(const LHCb::ParticleProperty *thePP) const override;

  // This method is not implemented
  void turnOnFragmentation() override;

  // This method is not implemented
  void turnOffFragmentation() override;

  // This method is not implemented
  StatusCode setupForcedFragmentation(const int thePdgId) override;

  // This method is not implemented
  StatusCode hadronize(HepMC::GenEvent *theEvent, LHCb::GenCollision *theCollision) override;

  // This method is not implemented
  void savePartonEvent(HepMC::GenEvent *theEvent) override;

  // This method is not implemented
  void retrievePartonEvent(HepMC::GenEvent *theEvent) override;

  // This method is not implemented
  void printRunningConditions() override;

  // Create temporary CRMC parameters file with default options
  StatusCode writeTempGeneratorParamFiles(CommandVector &options);

  // Printout out how the generator was configured to run
  void printOutGeneratorConfiguration();

private:
  CommandVector m_defaultSettings;  // Default settings
  Gaudi::Property<CommandVector> m_userSettings{this,"Commands",{},"User settings"};     // User settings

  Gaudi::Property<bool> m_printEvent{this,"PrintEvents",false,"Flag to print events on screen"};                // Flag to print events on screen
  int m_nEvents{0};                    // Generated events counter
  std::string m_tempParamFileName{""};  // Temporary file for CRMC parameters
  std::string m_tempParamFileName_backup{""};

  CRMCWrapper *m_CRMCEngine{nullptr};        // CRMC engine

  Gaudi::Property<bool> m_reseedCRMCRandGen{this,"ReSeedCRMCRandGenEveryEvt",false,"Use CRMC random generator over Gaudi"};         // Use CRMC random generator over Gaudi
  Rndm::Numbers m_random;           // Random number generator

  Gaudi::Property<std::string> m_beamToolName{this,"BeamToolName","CollidingBeams","Beam tool name"};       // Beam tool name
  IBeamTool *m_beamTool{nullptr};            // Beam tool

  // CRMC configuration flags
  Gaudi::Property<double>      m_seed{this,"RandomSeed",-1,"Random number generator seed"}; // Random number generator seed
  Gaudi::Property<int>         m_HEModel{this,"HEModel",crmc_generators::EPOS_LHC,"Flag for the MC model"} ;// Flag for the MC model
  Gaudi::Property<double>      m_projectileMomentum{this,"ProjectileMomentum",0.123456789,"Projectile Momentum"};
  Gaudi::Property<double>      m_targetMomentum{this,"TargetMomentum",0.123456789,"Target Momentum"};
  Gaudi::Property<int>         m_projectileID{this,"ProjectileID",2212,"Projectile ID"};
  Gaudi::Property<int>         m_targetID{this,"TargetID",2212,"Target ID"};
  Gaudi::Property<double>      m_minDecayLength{this,"MinDecayLength",1.0,"CRMC variable to determine stable particles"};          // CRMC variable to determine stable particles
  Gaudi::Property<std::string> m_paramFileName{this,"EPOSParamFileName","","Path to the CRMC parameters file"};      // Path to the CRMC parameters file
  Gaudi::Property<bool>        m_switchOffEventTruncation{this,"SwitchOffEventTruncation",true,"Switch off event truncation"};  // Switch off event truncation
  Gaudi::Property<bool>        m_produceTables{this,"ProduceTables",false,"Produce EPOS tables"}; //Produce EPOS tables
  Gaudi::Property<bool>        m_impactParameter{this,"ImpactParameter",false,"To define a user impact parameter range"}; //To define a user impact parameter range
  Gaudi::Property<double>      m_minImpactParameter{this,"MinImpactParameter",0.,"min value of the impact parameter"}; //min value of the impact parameter
  Gaudi::Property<double>      m_maxImpactParameter{this,"MaxImpactParameter",20.,"max value of the impact parameter"}; //max value of the impact parameter
  Gaudi::Property<bool>        m_addUserSettingsToDefault{this,"AddUserSettingsToDefault",false,"add user settings to default settings"}; //add user settings to default settings
  Gaudi::Property<std::string> m_frame{this,"Frame","","give the frame for the outputs"}; //give the frame for the outputs

  // Return the particle mass (GeV), selected from the ParticlePropertiesService by mapping CRMC id to PDG id
  double particleMass(int pID);

  // Create default CRMC configuration file
  void createDefaultCRMCConfiguration();

  // Fill HepMCEvent
  HepMC::GenEvent* FillHepMC(HepMC::GenEvent *theEvent);
};

#endif
