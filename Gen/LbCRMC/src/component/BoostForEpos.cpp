/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// Include file
#include <cmath>

 // from Gaudi
#include "GaudiKernel/SystemOfUnits.h"
#include "GaudiKernel/PhysicalConstants.h"

// local
#include "BoostForEpos.h"

//-----------------------------------------------------------------------------
// Implementation file for class : BoostForEpos
//
// 2016-10-21 : Patrick Robbe
//-----------------------------------------------------------------------------

// Declaration of the Algorithm Factory
DECLARE_COMPONENT( BoostForEpos )

//=============================================================================
// Initialization
//=============================================================================
StatusCode BoostForEpos::initialize() {
  StatusCode sc = GaudiAlgorithm::initialize(); // must be executed first
  if ( sc.isFailure() ) return sc;  // error printed already by GaudiAlgorithm

  if ( msgLevel(MSG::DEBUG) ) debug() << "==> Initialize" << endmsg;

  Gaudi::LorentzVector vec( m_px.value() , m_py.value(), m_pz.value() , m_e ) ;

  m_boost = ROOT::Math::Boost( -vec.BoostToCM() ) ;

  return StatusCode::SUCCESS;
}

//=============================================================================
// Main execution
//=============================================================================
StatusCode BoostForEpos::execute() {

  if ( msgLevel(MSG::DEBUG) ) debug() << "==> Execute" << endmsg;

    // loop over input HepMC and find EPOS event
  SmartDataPtr< LHCb::HepMCEvents > hepMCptr( eventSvc() , 
                                              m_inputHepMCEvent.value() ) ;
  
  HepMC::GenEvent * theEPOSevent( 0 ) ;

  if ( 0 == hepMCptr ) {
    info() << "No HepMCEvents at location " << m_inputHepMCEvent.value() 
           << endmsg ;
    return StatusCode::FAILURE ;
  } else {
    if ( hepMCptr->size() == 1 ) {
      theEPOSevent = (*hepMCptr)( 0 ) -> pGenEvt() ; 
    } else {
      theEPOSevent = (*hepMCptr)( 1 ) -> pGenEvt() ;
    }
  }

  // loop over particles and apply boost
  for ( HepMC::GenEvent::particle_iterator itP = theEPOSevent -> particles_begin() ;
          itP != theEPOSevent -> particles_end() ; ++itP ) {
    Gaudi::LorentzVector momentum( (*itP) -> momentum() ) ;
    Gaudi::LorentzVector newMomentum = m_boost( momentum ) ;
    (*itP) -> set_momentum( HepMC::FourVector( newMomentum.px() , 
                                               newMomentum.py() , 
                                               newMomentum.pz() ,                   
                                               newMomentum.e() ) ) ;
  }

  // Find PV position
  HepMC::GenParticle * theBeam = theEPOSevent -> beam_particles().first ;
  HepMC::FourVector thePV = theBeam -> end_vertex() -> position() ;
  // correct time units
  thePV.setT( thePV.t() * Gaudi::Units::c_light ) ;

  // loop over vertices and apply boost
  for ( HepMC::GenEvent::vertex_iterator itV = theEPOSevent -> vertices_begin() ;
          itV != theEPOSevent -> vertices_end() ; ++itV ) {
    Gaudi::LorentzVector position( (*itV) -> position().x() - thePV.x() , 
                                   (*itV) -> position().y() - thePV.y() ,
                                   (*itV) -> position().z() - thePV.z() , 
                                   (*itV) -> position().t() * Gaudi::Units::c_light - thePV.t() ) ;

    Gaudi::LorentzVector newPosition = m_boost( position ) + thePV ;
    // and change back time units after Boost 
    (*itV) -> set_position( HepMC::FourVector( newPosition.x() , 
                                               newPosition.y() , 
                                               newPosition.z() ,                   
                                               newPosition.t() / Gaudi::Units::c_light ) ) ;
  }

  return StatusCode::SUCCESS;
}

//=============================================================================
//  Finalize
//=============================================================================
StatusCode BoostForEpos::finalize() {

  if ( msgLevel(MSG::DEBUG) ) debug() << "==> Finalize" << endmsg;

  return GaudiAlgorithm::finalize();  // must be called after all other actions
}

//=============================================================================
