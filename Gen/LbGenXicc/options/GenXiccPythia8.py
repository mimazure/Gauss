###############################################################################
# (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
# Import the necessary modules.
from Configurables import Generation, MinimumBias, Special, Pythia8Production
from Configurables import GenXiccProduction

# Add Pythia as minimum bias production tool.
Generation().addTool(MinimumBias)
Generation().MinimumBias.ProductionTool = "Pythia8Production"
Generation().MinimumBias.addTool(Pythia8Production)

# Add GenXicc as special production tool.
Generation().addTool(Special)
Generation().Special.ProductionTool = "GenXiccProduction"
Generation().Special.addTool(GenXiccProduction)
Generation().Special.GenXiccProduction.ShowerToolName = "Pythia8Production"
Generation().Special.PileUpProductionTool = "Pythia8Production/Pythia8PileUp"
Generation().Special.ReinitializePileUpGenerator  = False

