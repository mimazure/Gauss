/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// F. Zhang 01-04-11
#ifndef LBGENXICC_COUNTER_H
#define LBGENXICC_COUNTER_H 1

#ifdef WIN32
extern "C" {
  void* __stdcall COUNTER_ADDRESS(void) ;
}
#else
extern "C" {
  void* counter_address__(void) ;
}
#endif

class Counter {
public:
  Counter();
  ~Counter();
  // F. Zhang 01-04-11
  // int& ibcstate();
  int& ixiccstate();
  int& nev();
  double& xmaxwgt();
  inline void init(); // inlined for speed of access (small function)
private:
  struct COUNTER;
  friend struct COUNTER;
  
  struct COUNTER {
    //int ibcstate;
    int ixiccstate; // F. Zhang 01-04-11
    int nev;
    double xmaxwgt;
  };
  int m_dummy;
  double m_realdummy;
  static COUNTER* s_counter;
};

// Inline implementations for Counter
// initialise pointer
#ifdef WIN32
void Counter::init(void) {
  if ( 0 == s_counter ) s_counter = static_cast<COUNTER*>(COUNTER_ADDRESS());
}
#else
void Counter::init(void) {
  if ( 0 == s_counter ) s_counter = static_cast<COUNTER*>(counter_address__());
}
#endif
#endif // LBGENXICC_COUNTER_H
 
