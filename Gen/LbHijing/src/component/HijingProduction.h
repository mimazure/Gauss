/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// $Id: HijingProduction.h,v 1.2 2007-03-09 12:47:33 gcorti Exp $
#ifndef LBHIJING_HIJINGPRODUCTION_H
#define LBHIJING_HIJINGPRODUCTION_H 1

// Include files
// from Gaudi
#include "GaudiAlg/GaudiTool.h"
#include "Generators/IProductionTool.h"

/** @class HijingProduction HijingProduction.h
 *
 *  Interface tool to produce events with Hijing
 *
 *  @author Daniele Gregori
 *  @date   2006-02-25
 */
class HijingProduction : public GaudiTool, virtual public IProductionTool {
public:
  typedef std::vector< std::string > CommandVector ;

  /// Standard constructor
  HijingProduction( const std::string& type,
                    const std::string& name,
                    const IInterface* parent);

  StatusCode initialize( ) override;   ///< Initialize method

  StatusCode finalize( ) override;   ///< Finalize method
  StatusCode generateEvent( HepMC::GenEvent * theEvent ,
                            LHCb::GenCollision * theCollision ) override;

  StatusCode initializeGenerator( ) override;

  void setStable( const LHCb::ParticleProperty * thePP ) override;

  void updateParticleProperties( const LHCb::ParticleProperty * thePP ) override;

  bool isSpecialParticle( const LHCb::ParticleProperty * thePP ) const override;

  void turnOnFragmentation( ) override;

  void turnOffFragmentation( ) override;

  StatusCode setupForcedFragmentation( const int thePdgId ) override;

  StatusCode hadronize( HepMC::GenEvent * theEvent ,
                        LHCb::GenCollision * theCollision ) override;

  void savePartonEvent( HepMC::GenEvent * theEvent ) override;

  void retrievePartonEvent( HepMC::GenEvent * theEvent ) override;

  void printRunningConditions( ) override;

protected:
  StatusCode parseHijingCommands( const CommandVector & theVector ) ;

private:
  int m_beam_to_use    ; // 1 = beam1, 2 = beam2
  double m_efrm        ; // colliding energy
  std::string m_frame  ; // frame type
  std::string m_proj   ; // projectile type
  std::string m_targ   ; // target type
  int m_iap            ; // projectile mass number
  int m_izp            ; // projectile charge number
  int m_iat            ; // target mass number
  int m_izt            ; // target charge number
  double m_bmin        ; // minimum impact parameter
  double m_bmax        ; // maximum impact parameter

  CommandVector m_defaultSettings ;
  Gaudi::Property<CommandVector> m_commandVector{this,"Commands",{},"Commands to setup hijing"} ; ///< Commands to setup hijing

  double m_beta        ; // velocity of NN pairs in LAB: zhwyang
  double m_gamma       ; // Lorentz boost factor, 1/sqrt(1.-m_beta*m_beta): zhwyang

};
#endif // LBHIJING_HIJINGPRODUCTION_H
