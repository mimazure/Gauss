/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// $Id: Hijing.cpp,v 1.1.1.1 2006-07-03 17:23:35 gcorti Exp $
// Include files

// local
#include "LbHijing/Hijing.h"

#include <string.h>
#include <cstdio>

//-----------------------------------------------------------------------------
// Implementation file for class : Hijing
//
// 2006-02-25 : Daniele Gregori
//-----------------------------------------------------------------------------

// HIJSET Fortran function
extern "C" {
#ifdef WIN32
  void __stdcall HIJSET(const double*, const char*, int, const char*, int,
                        const char*, int,  const int*, const int*, const int*,
                        const int*) ;
#else
  void hijset_ (double&, const char*, const char*, const char*, 
                int&, int&, int&, int&, int, int, int);
#endif
}

void Hijing::HijingInit(double l_efrm, const std::string l_frame,
                        const std::string l_proj, const std::string l_targ,
                        int l_iap, int l_izp, int l_iat, int l_izt) {
  double efrm=l_efrm;
  
  char frame[9]; memset(frame,' ',9); snprintf(frame, 9, "%s", l_frame.c_str());
  char proj[9]; memset(proj,' ',9); snprintf(proj, 9, "%s", l_proj.c_str());
  char targ[9]; memset(targ,' ',9); snprintf(targ, 9, "%s", l_targ.c_str());
  
  int iap=l_iap;
  int izp=l_izp;
  int iat=l_iat;
  int izt=l_izt;
  
#ifdef WIN32
  HIJSET( &efrm, frame, strlen(frame), proj, strlen(proj), targ,
          strlen(targ), &iap, &izp, &iat, &izt);
#else
  hijset_ ( efrm, frame, proj, targ, iap, izp, iat, izt,
            strlen(frame), strlen(proj), strlen(targ));
#endif
}

// HIJING Fortran function
extern "C" {
#ifdef WIN32
  void __stdcall HIJING( const char*, int, const double*, const double* ) ;
#else
  void hijing_ (const char*, double&, double&, int ) ;
#endif
}

// LUHEPC Fortran function
extern "C" {
#ifdef WIN32
  void __stdcall HILUNHEP( int * ) ;
#else
  void hilunhep_( int & ) ;
#endif 
}

void Hijing::LunHep( int mcconv ) {
#ifdef WIN32
  HILUNHEP( &mcconv) ;
#else
  hilunhep_( mcconv ) ;
#endif
}

void Hijing::HijingEvnt( const std::string l_frame, double l_bmin, double l_bmax ) {
  char frame[9]; memset(frame,' ',9); snprintf(frame, 9, "%s", l_frame.c_str());
  double bmin=l_bmin;
  double bmax=l_bmax;
  
#ifdef WIN32
  HIJING( frame, strlen(frame), &bmin, &bmax) ;
#else
  hijing_( frame , bmin , bmax , strlen(frame) ) ;
#endif
}

