###############################################################################
# (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from Configurables import Gauss
from Configurables import Generation
from Configurables import Special
from GaudiKernel   import SystemOfUnits
from Configurables import OniaPairsProduction

Generation().SampleGenerationTool = "Special"
Generation().addTool( Special )
Generation().Special.ProductionTool = "OniaPairsProduction"
Generation().Special.addTool( OniaPairsProduction )
Generation().Special.OniaPairsProduction.Ecm = 2 * Gauss().BeamMomentum / SystemOfUnits.GeV

