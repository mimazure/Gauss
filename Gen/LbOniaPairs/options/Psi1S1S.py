###############################################################################
# (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from Configurables import Generation, Gauss
from GaudiKernel import SystemOfUnits
Generation().Special.OniaPairsProduction.Ecm = 2 * Gauss().BeamMomentum / SystemOfUnits.GeV
Generation().Special.OniaPairsProduction.Psi1S1S = 1
Generation().Special.OniaPairsProduction.Psi1S2S = 1
Generation().Special.OniaPairsProduction.Psi2S2S = 1
