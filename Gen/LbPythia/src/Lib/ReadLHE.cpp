/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// $Id: ReadLHE.cpp,v 1.2 2007-07-31 12:49:53 ibelyaev Exp $
// ============================================================================
// Include files 
// ============================================================================
// Gaudi
// ============================================================================
// ============================================================================
// Generators  
// ============================================================================
#include "Generators/F77Utils.h"
// ============================================================================
// LbPythia
// ============================================================================
#include "LbPythia/Pypars.h"
#include "LbPythia/Pythia.h"
#include "LbPythia/PythiaProduction.h"
// ============================================================================
// Local 
// ============================================================================
#include "LbPythia/ReadLHE.h"
// ============================================================================
/** @file
 *  Implementation file for class LbPythia::ReadLHE 
 *  @author Vanya BELYAEV ibelyaev@physics.syr.edu
 *  @date 2007-06-29
 */
// ============================================================================
/* standard constructor 
 *  @param type tool type (?)
 *  @param name tool name 
 *  @param parent tool parent 
 */
// ============================================================================
LbPythia::ReadLHE::ReadLHE
( const std::string& type,
  const std::string& name,
  const IInterface* parent ) 
  : LbPythia::ReadFile ( type , name , parent ) 
  , m_LUN ( 0 )
{
  // see LHEREADPROCESS
  PythiaProduction::m_userProcess    = 3 ; ///< see LHEREADPROCESS
}
// ============================================================================
// destructor 
// ============================================================================
LbPythia::ReadLHE::~ReadLHE(){}
// ============================================================================
// tool initialization 
// ============================================================================
StatusCode LbPythia::ReadLHE::initialize () 
{
  // check input file name 
  if ( file().empty() ) { return Error("Input file name is not specified!") ; }
  // get free fortran Unit 
  m_LUN = F77Utils::getUnit ( msgLevel ( MSG::DEBUG ) ) ;
  if ( 0 >= m_LUN ) { return Error("No free FORTRAN unit available ") ; }    
  // open input LHE file 
  StatusCode sc = F77Utils::openOld ( m_LUN , file() , msgLevel ( MSG::INFO ) ) ;
  if ( sc.isFailure() ) 
  { return Error ( "Could not open the file '" + file() + "'" ) ; }
  //
  Pythia::pypars().mstp ( 161 ) = m_LUN ;                         // ATTENTION!
  Pythia::pypars().mstp ( 162 ) = m_LUN ;                         // ATTENTION!
  //
  PythiaProduction::m_userProcess    = 3 ; ///< see LHEREADPROCESS
  //
  return LbPythia::ReadFile::initialize () ;
}
// ============================================================================
// tool finalization 
// ============================================================================
StatusCode LbPythia::ReadLHE::finalize   () 
{
  // close input LHE file 
  StatusCode sc = F77Utils::close ( m_LUN , msgLevel ( MSG::INFO ) ) ;
  if ( sc.isFailure() ) 
  { Error ( "Error in closing '" + file() + "'" , sc ).ignore(/* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */); } // NO RETURN !
  m_LUN = 0 ;
  Pythia::pypars().mstp ( 161 ) = 0 ;                      // ATTENTION !
  Pythia::pypars().mstp ( 162 ) = 0 ;                      // ATTENTION !
  //
  return LbPythia::ReadFile::finalize() ;
}
// ============================================================================
// The END 
// ============================================================================
