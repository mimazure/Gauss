/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// $Id: PythiaProduction.h,v 1.11 2008-10-23 15:34:15 robbep Exp $
#ifndef LBPYTHIA_PYTHIAPRODUCTION_H
#define LBPYTHIA_PYTHIAPRODUCTION_H 1
// ============================================================================
// Include files
// ============================================================================
// From STL
#include <fstream>
//
// from Gaudi
// ============================================================================
#include "GaudiAlg/GaudiTool.h"
#include "Generators/IProductionTool.h"
#include "Generators/ICounterLogFile.h"

#include "GaudiKernel/SystemOfUnits.h"

#include "Event/GenFSR.h"
// ============================================================================
// Forward declaration
class IBeamTool ;
namespace LHCb {
  class ParticleProperty ;
}

/** @class PythiaProduction PythiaProduction.h
 *
 *  Interface tool to produce events with Pythia
 *
 *  @author Patrick Robbe
 *  @date   2005-08-16
 */
class PythiaProduction : public extends<GaudiTool, IProductionTool> {
public:
  typedef std::vector<std::string> CommandVector ;

  /// Standard constructor
  using extends::extends;
  
  StatusCode initialize( ) override;   ///< Initialize method

  StatusCode finalize( ) override;   ///< Finalize method

  StatusCode generateEvent( HepMC::GenEvent * theEvent ,
                            LHCb::GenCollision * theCollision ) override;

  StatusCode initializeGenerator( ) override;

  void setStable( const LHCb::ParticleProperty * thePP ) override;

  void updateParticleProperties( const LHCb::ParticleProperty * thePP ) override;

  void turnOnFragmentation( ) override;

  void turnOffFragmentation( ) override;

  StatusCode hadronize( HepMC::GenEvent * theEvent ,
                        LHCb::GenCollision * theCollision ) override;

  void savePartonEvent( HepMC::GenEvent * theEvent ) override;

  void retrievePartonEvent( HepMC::GenEvent * theEvent ) override;

  void printRunningConditions( ) override;

  bool isSpecialParticle( const LHCb::ParticleProperty * thePP ) const override;

  StatusCode setupForcedFragmentation( const int thePdgId ) override;

private:
  /// Location where to store FSR counters (set by options)
  Gaudi::Property<std::string>  m_FSRName{this,"GenFSRLocation",
      LHCb::GenFSRLocation::Default,"Location where to store FSR counters"};
  ICounterLogFile * m_xmlLogTool{nullptr} ; ///< XML Log file to store cross-sections

protected:
  /// Parse Pythia commands from a string vector
  StatusCode parsePythiaCommands( const CommandVector & theVector ) ;

  /// Print Pythia parameters
  void printPythiaParameter( ) ;

  /// Retrieve hard process information
  void hardProcessInfo( LHCb::GenCollision * theCollision ) ;

public:
  int m_userProcess{0} ; ///< type of User process
  std::string m_frame{"3MOM"}   ;  ///< FRAME string
  std::string m_beam{"p+"}    ;  ///< BEAM string
  std::string m_target{"p+"}  ;  ///< TARGET string

protected:

  void setPygive ( const CommandVector& vct ) { m_pygive.value() = vct ; }
  void addPygive ( const std::string&   item ) { m_pygive.value().push_back ( item ) ; }
  const CommandVector& pygive() const { return m_pygive.value() ; }

  /// PYTHIA -> HEPEVT -> HEPMC conversion
  StatusCode toHepMC
  ( HepMC::GenEvent*     theEvent    ,
    LHCb::GenCollision * theCollision ) ;

protected:

  double m_win{0.}          ;  ///< WIN


  CommandVector m_defaultSettings{
    // Set the default settings for Pythia here:
    "pysubs msel 0" ,
    "pysubs msub 11 1",
    "pysubs msub 12 1",
    "pysubs msub 13 1",
    "pysubs msub 28 1",
    "pysubs msub 53 1",
    "pysubs msub 68 1",
    "pysubs msub 91 1",
    "pysubs msub 92 1",
    "pysubs msub 93 1",
    "pysubs msub 94 1",
    "pysubs msub 95 1",
    "pysubs msub 421 1",
    "pysubs msub 422 1",
    "pysubs msub 423 1",
    "pysubs msub 424 1",
    "pysubs msub 425 1",
    "pysubs msub 426 1",
    "pysubs msub 427 1",
    "pysubs msub 428 1",
    "pysubs msub 429 1",
    "pysubs msub 430 1",
    "pysubs msub 431 1",
    "pysubs msub 432 1",
    "pysubs msub 433 1",
    "pysubs msub 434 1",
    "pysubs msub 435 1",
    "pysubs msub 436 1",
    "pysubs msub 437 1",
    "pysubs msub 438 1",
    "pysubs msub 439 1",
    "pysubs msub 461 1",
    "pysubs msub 462 1",
    "pysubs msub 463 1",
    "pysubs msub 464 1",
    "pysubs msub 465 1",
    "pysubs msub 466 1",
    "pysubs msub 467 1",
    "pysubs msub 468 1",
    "pysubs msub 469 1",
    "pysubs msub 470 1",
    "pysubs msub 471 1",
    "pysubs msub 472 1",
    "pysubs msub 473 1",
    "pysubs msub 474 1",
    "pysubs msub 475 1",
    "pysubs msub 476 1",
    "pysubs msub 477 1",
    "pysubs msub 478 1",
    "pysubs msub 479 1",
    "pysubs msub 480 1",
    "pysubs msub 481 1",
    "pysubs msub 482 1",
    "pysubs msub 483 1",
    "pysubs msub 484 1",
    "pysubs msub 485 1",
    // Allows generation of resonances (psi(3770) for example) in 2 -> 2 processes,
    "pysubs ckin 41 3.0",
    "pypars mstp 2 2",
    "pypars mstp 33 3",
    "pypars mstp 128 2",
    "pypars mstp 81 21",
    "pypars mstp 82 3",
    "pypars mstp 52 2",
    "pypars mstp 51 10042",
    "pypars mstp 142 2",
    "pypars parp 67 1.0",
    "pypars parp 82 4.28",
    "pypars parp 89 14000",
    "pypars parp 90 0.238",
    "pypars parp 85 0.33",
    "pypars parp 86 0.66",
    "pypars parp 91 1.0",
    "pypars parp 149 0.02",
    "pypars parp 150 0.085",
    "pydat1 parj 11 0.4",
    "pydat1 parj 12 0.4",
    "pydat1 parj 13 0.769",
    "pydat1 parj 14 0.09",
    "pydat1 parj 15 0.018",
    "pydat1 parj 16 0.0815",
    "pydat1 parj 17 0.0815",
    "pydat1 mstj 26 0",
    "pydat1 parj 33 0.4",
  } ;
	Gaudi::Property<CommandVector> m_commandVector{this,"Commands",{},"Commands to setup pythia"} ; ///< Commands to setup pythia

  Gaudi::Property<CommandVector> m_pygive{this,"PygiveCommands",{},"Commands in 'Pygive' format"}        ; ///< Commands in "Pygive" format

  bool m_variableEnergy{false} ;

  // event listing level for "generateEvent"
  int m_eventListingLevel{-1}  ;
  // event listing level for "hadronize"
  int m_eventListingLevel2{-1} ;
  int m_initializationListingLevel{1} ;
  int m_finalizationListingLevel{-1} ;

public:
  /// Beam tool name, needs to be externally accessible for hard production.
  Gaudi::Property<std::string> m_beamToolName{this,"BeamToolName", "CollidingBeams","Beam tool name, needs to be externally accessible for hard production."} ;

protected:
  std::string m_pythiaListingFileName{""} ;
  int m_pythiaListingUnit{0} ;

  int         m_particleDataUnit{59}   ;
  std::string m_particleDataOutput{""} ;
  std::string m_particleDataInput{""}  ;
  int         m_particleDataLevel{0}  ;

  IBeamTool * m_beamTool{nullptr} ;

private:

  void writePythiaEntryHeader(std::ofstream &outdec,
                              const LHCb::ParticleProperty * thePP ) ;

  // MSTU(1)/MSTU(2) for initialization PYLIST
  int m_ini_mstu_1{0} ;
  int m_ini_mstu_2{0} ;
  // MSTU(1)/MSTU(2) for "generateEvent" PYLIST
  int m_eve_mstu_1{0} ;
  int m_eve_mstu_2{0} ;
  // MSTU(1)/MSTU(2) for "hadronize" PYLIST
  int m_had_mstu_1{0} ;
  int m_had_mstu_2{0} ;
  // list of particles to be printed
  Gaudi::Property<std::vector<int>> m_pdtlist{this,"PDTList",{},
  "list of particles to be printed"} ;
  int m_nEvents{0} ; ///< Internal event counter

  Gaudi::Property<double> m_widthLimit{this,"WidthLimit",1.5e-6 * Gaudi::Units::GeV ,
  "Limit to consider a particle with no lifetime"} ; ///< Limit to consider a particle with no lifetime

  /** Name of optional SLHA decay file (to be placed in Gen/DecFiles/dkfiles
   *  directory)
   */
  Gaudi::Property<std::string> m_slhaDecayFile{this,"SLHADecayFile", "empty","Name of optional SLHA decay file"} ;
  Gaudi::Property<std::vector< int >> m_pdecaylist{this,"PDecayList", {},"PDecayList"} ;
  Gaudi::Property<std::string> m_slhaSpectrumFile{this,"SLHASpectrumFile", "empty","Name of optional SLHA spectrum file"} ;
  // ==========================================================================
  /// boolean flag to force the valiadation of IO_HEPEVT
  Gaudi::Property<bool> m_validate_HEPEVT{this,"ValidateHEPEVT",
  false ,"The flag to force the validation (mother&daughter) of HEPEVT"} ; // force the valiadation of IO_HEPEVT
  // ==========================================================================
  /// the file to dump the HEPEVT incinsistencies
  Gaudi::Property<std::string  > m_inconsistencies{this,"Inconsistencies",
  "HEPEVT_inconsistencies.out","The file to dump HEPEVT inconsinstencies"} ; // the file to dump the HEPEVT incinsistencies
  // ==========================================================================
  std::ostream* m_HEPEVT_errors{nullptr} ;
  // ==========================================================================

  Gaudi::Property<std::vector<int>> m_updatedParticles{this,"UpdatedParticles",{},
  "list of particles whose properties should always be taken from the ParticlePropertySvc"};

  Gaudi::Property<std::vector< int >> m_particlesToAdd{this,"ParticlesToAdd",
  {30443,200553,300553,9000553},"list of PIDs to add to Pythia"} ;
};
#endif // LBPYTHIA_PYTHIAPRODUCTION_H
