/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
//=============================================================================
// Include files.
//=============================================================================

// Gaudi.
#include "GaudiAlg/GaudiTool.h"
#include "GaudiKernel/System.h"
#include "Kernel/ParticleProperty.h"
#include "Kernel/IParticlePropertySvc.h"
#include "GaudiKernel/PhysicalConstants.h"
#include "GaudiKernel/IRndmGenSvc.h"
#include "GaudiKernel/IRndmEngine.h"
#include "GaudiKernel/RndmGenerators.h"

// Event.
#include "Event/GenCollision.h"

// Generators.
#include "Generators/StringParse.h"
#include "Generators/IProductionTool.h"

// HepMC.
#include "HepMC/GenEvent.h"
#include "HepMC/GenParticle.h"
#include "HepMC/GenVertex.h"

// StarLight.
#include "starlight.h"
#include "upcevent.h"

// Local.
#include "StarLightProduction.h"

//-----------------------------------------------------------------------------
//  Implementation file for class: StarLightProduction
//
//  2017-02-02 : Mingrui Zhao, Philip Ilten
//-----------------------------------------------------------------------------

//=============================================================================
// Initialize the Gaudi random number generator.
//=============================================================================
StatusCode GaudiRandomForStarLight::initialize(IRndmGenSvc *svc) {
  return m_rndm.initialize(svc, Rndm::Flat(0, 1));
}

//=============================================================================
// Finalize the Gaudi random number generator.
//=============================================================================
StatusCode GaudiRandomForStarLight::finalize() {
  return m_rndm.finalize();
} 

//=============================================================================
// Flat random function used by StarLight, argument is not used.
//=============================================================================
double GaudiRandomForStarLight::Rndom(int) {
  return m_rndm();
} 

//=============================================================================
// Destructor
//=============================================================================
GaudiRandomForStarLight::~GaudiRandomForStarLight() {
  m_rndm.finalize( ).ignore(/* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */);
}

// Declaration of tool factory.
DECLARE_COMPONENT(StarLightProduction)



//=============================================================================
// Initialize the tool.
//=============================================================================
StatusCode StarLightProduction::initialize() {
  
  // Print the initialization banner.
  always() << "============================================================="
	   << "=====" << endmsg;
  always() << "Using as production engine " << this->type() << endmsg;
  always() << "============================================================="
	   << "=====" << endmsg;
  
  // Initialize the Gaudi tool.
  always() << "Entered initialize." << endmsg;
  StatusCode sc = GaudiTool::initialize();
  if (sc.isFailure())
    Exception("Gaudi Tool failed to initialize");

  // Initialize the random number generator.
  IRndmGenSvc* rndm(0);
  try {rndm = svc<IRndmGenSvc>("RndmGenSvc", true);}
  catch (const GaudiException& e) 
    {Exception("Failed to initialize the RndmGenSvc.");}
  sc = m_rndm.initialize(rndm);
  if (!sc.isSuccess()) 
    Exception("Failed to initialize GaudiRandomForStarLight.");
  release(rndm).ignore(/* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */);

  // Read the StarLight settings.
  sc = parseSettings(m_defaultSettings);
  if (sc.isFailure()) return Error("Failed to parse default settings.");
  sc = parseSettings(m_userSettings.value());
  if (sc.isFailure()) return Error("Failed to parse settings.");

  // Set the resonance production (0 if not resonance).
  int res = m_pars.prodParticleId();
  if      (res == 221)     m_res = 221;     // gm gm -> eta.
  else if (res == 331)     m_res = 331;     // gm gm -> eta'.
  else if (res == 441)     m_res = 441;     // gm gm -> eta_c.
  else if (res == 9010221) m_res = 9010221; // gm gm -> f_0(980).
  else if (res == 225)     m_res = 225;     // gm gm -> f_2.
  else if (res == 115)     m_res = 115;     // gm gm -> a_20.
  else if (res == 335)     m_res = 335;     // gm gm -> f'_2(1525).
  else if (res == 33)      m_res = 0;       // gm gm -> rh0 rho0.
  else if (res == 11)      m_res = 25;       // gm gm -> e+ e-.
  else if (res == 13)      m_res = 25;       // gm gm -> mu+ mu-.
  else if (res == 15)      m_res = 25;       // gm gm -> tau+ tau-.
  else if (res == 113)     m_res = 113;     // pm gm -> rho0.
  else if (res == 223)     m_res = 223;     // pm gm -> omega.
  else if (res == 333)     m_res = 333;     // pm gm -> phi.
  else if (res == 443011)  m_res = 443;     // pm gm -> psi(1S)[e+ e-].
  else if (res == 443013)  m_res = 443;     // pm gm -> psi(1S)[mu+ mu-].
  else if (res == 444011)  m_res = 100443;  // pm gm -> psi(2S)[e+ e-].  
  else if (res == 444013)  m_res = 100443;  // pm gm -> psi(2S)[mu+ mu-].
  else if (res == 553011)  m_res = 553;     // pm gm -> Upsilon(1S)[e+ e-].  
  else if (res == 553013)  m_res = 553;     // pm gm -> Upsilon(1S)[mu+ mu-].
  else if (res == 554011)  m_res = 100553;  // pm gm -> Upsilon(2S)[e+ e-].  
  else if (res == 554013)  m_res = 100553;  // pm gm -> Upsilon(2S)[mu+ mu-].
  else if (res == 555011)  m_res = 200553;  // pm gm -> Upsilon(3S)[e+ e-].  
  else if (res == 555013)  m_res = 200553;  // pm gm -> Upsilon(3S)[mu+ mu-].
  else if (res == 913)     m_res = 0;       // pm gm -> pi+ pi-.
  else if (res == 999)     m_res = 100113;  // pm gm -> rho0[pi+ pi- pi+ pi-].
  else if (res == 88)      m_res = 9010221;      // gm gm -> axion[gm gm]. (call the axion a f0 to avoid trouble at a later stage.)
  else return Error("The requested process is not supported.");

  // Check configuration and initialize the parameters.
  if (m_pars.beam1A() == 0 && m_pars.beam1Z() == 0)
    return Error("Beam 1 A and Z not configured.");
  if (m_pars.beam2A() == 0 && m_pars.beam2Z() == 0)
    return Error("Beam 2 A and Z not configured.");
  if (m_pars.beam1LorentzGamma() < 0)
    return Error("Beam 1 gamma not configured.");
  if (m_pars.beam2LorentzGamma() < 0)
    return Error("Beam 2 gamma not configured.");
  if (m_pars.beam1A() < m_pars.beam1Z())
    return Error("Beam 1 configuration with A < Z.");
  if (m_pars.beam2A() < m_pars.beam2Z())
    return Error("Beam 2 configuration with A < Z.");
  if (!m_pars.init())
    return Error("Failed to initialize the StarLight input parameters.");
  return StatusCode::SUCCESS;
}

//=============================================================================
// Finalize method.
//=============================================================================
StatusCode StarLightProduction::finalize() {

  // Print summary info.
  always() << "==============================================================="
	"===============\n";
  always() << "StarLight Event Summary\n";
  always() << "==============================================================="
	"===============\n";

  // Finalize StarLight.
  m_rndm.finalize().ignore(/* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */);
  return GaudiTool::finalize();
}

//=============================================================================
// Initialize the StarLight generator.
//=============================================================================
StatusCode StarLightProduction::initializeGenerator() {
  m_generator.setRandomGenerator(&m_rndm);
  m_generator.setInputParameters(&m_pars);
  return StatusCode(m_generator.init());
}

//=============================================================================
// Generate an event.
//=============================================================================
StatusCode StarLightProduction::generateEvent(HepMC::GenEvent *theEvent,
					      LHCb::GenCollision *
					      /*theCollision*/) {
  // Perform event generation.
  upcEvent theUPCEvent = m_generator.produceEvent();
  
  // Convert the event to HepMC and return.
  toHepMC(theUPCEvent, theEvent).ignore(/* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */);
  return StatusCode::SUCCESS;
}

//=============================================================================
// Convert the StarLight HepEvt record to HepMC format.
//=============================================================================
StatusCode StarLightProduction::toHepMC(upcEvent &theUPCEvent, 
					HepMC::GenEvent *theEvent) {
  // Add dummy beams (beam particles not available from StarLight).
  HepMC::GenParticle* beam1 = new HepMC::GenParticle
    (HepMC::FourVector(0, 0, 0, 0), 22);
  HepMC::GenParticle* beam2 = new HepMC::GenParticle
    (HepMC::FourVector(0, 0, 0, 0), 22);
  theEvent->set_beam_particles(std::make_pair(beam1, beam2));

  // Create the vertices.
  HepMC::GenVertex *pv = new HepMC::GenVertex
    (HepMC::FourVector(0, 0, 0, 0));
  HepMC::GenVertex *dv = (m_res != 0 && m_decays.value()) ? new HepMC::GenVertex
    (HepMC::FourVector(0, 0, 0, 0)) : 0;

  // Add the final state particles.
  lorentzVector p, v;
  pv->add_particle_in(beam1);
  pv->add_particle_in(beam2);
  for (std::vector<starlightParticle>::const_iterator 
	 prt = theUPCEvent.getParticles()->begin();
       prt != theUPCEvent.getParticles()->end(); prt++) {
    HepMC::GenParticle *out = new HepMC::GenParticle
      (HepMC::FourVector(
	  prt->GetPx()*Gaudi::Units::GeV, 
	  prt->GetPy()*Gaudi::Units::GeV, 
	  prt->GetPz()*Gaudi::Units::GeV,
	  prt->GetE() *Gaudi::Units::GeV), prt->getPdgCode());
    out->set_status(LHCb::HepMCEvent::StableInProdGen);
    p += (*prt); v = prt->getVertex();
    if (m_res == 0 || m_decays.value()) (dv ? dv : pv)->add_particle_out(out);
  }

  // Add the resonance if needed.
  if (m_res != 0) {
    HepMC::GenParticle *res = new HepMC::GenParticle
      (HepMC::FourVector(
	  p.GetPx()*Gaudi::Units::GeV, 
	  p.GetPy()*Gaudi::Units::GeV, 
	  p.GetPz()*Gaudi::Units::GeV,
	  p.GetE() *Gaudi::Units::GeV), m_res);
    res->set_status(LHCb::HepMCEvent::StableInProdGen);
    pv->add_particle_out(res);
    if (dv) {
      dv->add_particle_in(res);
      dv->set_position
	(HepMC::FourVector(v.GetPx(), v.GetPy(), v.GetPz(), v.GetE()));
      res->set_status(LHCb::HepMCEvent::DecayedByProdGen);
    }
  }

  // Add the vertices.
  theEvent->add_vertex(pv);
  theEvent->set_signal_process_vertex(pv);
  if (dv) theEvent->add_vertex(dv);
  return StatusCode::SUCCESS;
}

//=============================================================================
// Update particles properties.
//=============================================================================
void StarLightProduction::updateParticleProperties(const LHCb::ParticleProperty
						   *thePP) {
  // Set the parameter strings.
  stringstream mass, width, spin;
  int pid(thePP->pid().abspid());
  if        (pid == 2112) {
    mass << "protonMass";
  } else if (pid == 211) {
    mass << "pionChargedMass";
  } else if (pid == 111) {
    mass << "pionNeutralMass";
  } else if (pid == 321) {
    mass << "kaonChargedMass";
  } else if (pid == 11) {
    mass << "mel";
  } else if (pid == 13) {
    mass << "muonMass";
  } else if (pid == 15) {
    mass << "tauMass";
  } else if (pid == 9010221) {
    mass << "f0Mass"; width << "f0Width"; spin << "f0Spin";
  } else if (pid == 221) {
    mass << "etaMass"; width << "etaWidth"; spin << "etaSpin";
  } else if (pid == 331) {
    mass << "etaPrimeMass"; width << "etaPrimeWidth"; spin << "etaPrimeSpin";
  } else if (pid == 441) {
    mass << "etaCMass"; width << "etaCWidth"; spin << "etaCSpin";
  } else if (pid == 225) {
    mass << "f2Mass"; width << "f2Width"; spin << "f2Spin";
  } else if (pid == 115) {
    mass << "a2Mass"; width << "a2Width"; spin << "a2Spin";
  } else if (pid == 335) {
    mass << "f2PrimeMass"; width << "f2PrimeWidth"; spin << "f2PrimeSpin";
  } else if (pid == 113) {
    mass << "rho0Mass"; width << "rho0Width";
  } else if (pid == 223) {
    mass << "OmegaMass"; width << "OmegaWidth";
  } else if (pid == 333) {
    mass << "PhiMass"; width << "PhiWidth";
  } else if (pid == 443) {
    mass << "JpsiMass"; width << "JpsiWidth";
  } else if (pid == 100443) {
    mass << "Psi2SMass"; width << "Psi2SWidth";
  } else if (pid == 553) {
    mass << "Upsilon1SMass"; width << "Upsilon1SWidth";
  } else if (pid == 100553) {
    mass << "Upsilon2SMass"; width << "Upsilon2SWidth";
  } else if (pid == 200553) {
    mass << "Upsilon3SMass"; width << "Upsilon3SWidth";
  }

  // Process the parameter strings.
  if (mass.str() != "") {
    mass << " = " << thePP->mass()/Gaudi::Units::GeV;
    m_pars.setParameter(mass.str());
  }
  if (width.str() != "") {
    width << " = " << Gaudi::Units::hbarc/
      (thePP->lifetime()*Gaudi::Units::c_light)/Gaudi::Units::GeV;
    m_pars.setParameter(width.str());
  }
  if (spin.str() != "") {
    spin << " = " << (thePP->pid().jSpin() - 1.0)/2.0;
    m_pars.setParameter(spin.str());
  }
}

//=============================================================================
// Dummy functions.
//=============================================================================

void StarLightProduction::setStable(const LHCb::ParticleProperty */*thePP*/) {}

void StarLightProduction::savePartonEvent(HepMC::GenEvent */*theEvent*/) {}

void StarLightProduction::retrievePartonEvent(HepMC::GenEvent */*theEvent*/) {}

StatusCode StarLightProduction::hadronize(HepMC::GenEvent */*theEvent*/,
					  LHCb::GenCollision */*theCollision*/)
{return StatusCode::SUCCESS;}

void StarLightProduction::printRunningConditions() {}

bool StarLightProduction::isSpecialParticle(const LHCb::ParticleProperty *
					    /*thePP*/) const {return false;}

StatusCode StarLightProduction::setupForcedFragmentation(const int
							 /*thePdgId*/)
{return StatusCode::SUCCESS;}

void StarLightProduction::turnOnFragmentation() {}

void StarLightProduction::turnOffFragmentation() {}

//=============================================================================
// Parse the StarLight settings.
//=============================================================================
StatusCode StarLightProduction::parseSettings(const CommandVector &settings) {
  // Loop over the settings.
  for (unsigned int i = 0; i < settings.size(); ++i) {
    debug() << "Command is: " << settings[i] << endmsg;
    if (!m_pars.setParameter(settings[i])) {
      warning() << "Unknown command: " << settings[i] << endmsg;
      return StatusCode::FAILURE;
    }
  }
  return StatusCode::SUCCESS;
}

// ============================================================================
// The END
// ============================================================================
