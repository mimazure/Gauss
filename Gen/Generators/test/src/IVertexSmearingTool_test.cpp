/*****************************************************************************\
* (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "GaudiAlg/GaudiHistoAlg.h"
#include "Gaudi/Accumulators.h"
#include "Generators/IVertexSmearingTool.h"
#include "Event/HepMCEvent.h"
#include "HepMC/GenEvent.h"
// #include <chrono>

class IVertexSmearingTool_test : public GaudiHistoAlg {
  public: 
    IVertexSmearingTool_test( const std::string& name, ISvcLocator* pSvcLocator);
    StatusCode execute() override;
    StatusCode initialize() override;  
    StatusCode finalize() override;
    virtual ~IVertexSmearingTool_test(); 
  private: 
    IVertexSmearingTool* m_vertexSmearingTool; 
    std::string m_vertexSmearingToolName;
    unsigned m_nPVs;  
    Gaudi::Accumulators::SigmaCounter<double> m_x{this, "x"}; 
    Gaudi::Accumulators::SigmaCounter<double> m_y{this, "y"};
    Gaudi::Accumulators::SigmaCounter<double> m_z{this, "z"};
    Gaudi::Accumulators::SigmaCounter<double> m_t{this, "t"};
    Gaudi::Accumulators::SigmaCounter<double> m_xy{this, "xy"};
    Gaudi::Accumulators::SigmaCounter<double> m_xx{this, "xx"};
    Gaudi::Accumulators::SigmaCounter<double> m_xz{this, "xz"};
    Gaudi::Accumulators::SigmaCounter<double> m_yz{this, "yz"};
    Gaudi::Accumulators::SigmaCounter<double> m_xt{this, "xt"};
    Gaudi::Accumulators::SigmaCounter<double> m_yt{this, "yt"};
    Gaudi::Accumulators::SigmaCounter<double> m_zt{this, "zt"};
    Gaudi::Accumulators::SigmaCounter<double> m_cpuTime{this, "CPUTime"};
};

DECLARE_COMPONENT( IVertexSmearingTool_test )

  IVertexSmearingTool_test::IVertexSmearingTool_test( const std::string& name, ISvcLocator* pSvcLocator) : 
    GaudiHistoAlg( name, pSvcLocator ) {
      declareProperty( "VertexSmearingTool", m_vertexSmearingToolName = "BeamSpotSmearVertex" ) ;
      declareProperty( "nPVs", m_nPVs = 50 ) ;
    }

IVertexSmearingTool_test::~IVertexSmearingTool_test(){}

StatusCode IVertexSmearingTool_test::execute()
{
  StatusCode sc = StatusCode::SUCCESS;
  
  for( unsigned int i = 0 ; i != m_nPVs; ++i )
  {
    LHCb::HepMCEvent theEvent;
    HepMC::GenEvent genEvent; 
    (*theEvent.pGenEvt()) = genEvent;
    auto * pEvt = theEvent.pGenEvt() ;
    pEvt->add_vertex( new HepMC::GenVertex() );
    auto t_start = std::chrono::high_resolution_clock::now();
    sc &= m_vertexSmearingTool -> smearVertex( &theEvent );
    auto t_end = std::chrono::high_resolution_clock::now();
    m_cpuTime += std::chrono::duration<double, std::milli>(t_end - t_start ).count();

    for( auto vertex = pEvt->vertices_begin(); vertex != pEvt->vertices_end(); ++vertex )
    {
      auto p = (*vertex)->position();
      plot( p.x() , "x", ";x [mm];Entries", -0.1, 0.1, 100 );
      plot( p.y() , "y", ";y [mm];Entries", -0.1, 0.1, 100 );
      plot( p.z() , "z", ";z [mm];Entries", -150, 150  , 100 );
      plot( p.t() , "t", ";t [ns];Entries", -0.6, 0.6  , 100 );
      m_x  += p.x();
      m_y  += p.y();
      m_z  += p.z();
      m_t  += p.t();
      m_xx += p.x() * p.x();
      m_xy += p.x() * p.y();
      m_xz += p.x() * p.z();
      m_yz += p.y() * p.z();
      m_xt += p.x() * p.t();
      m_yt += p.y() * p.t();
      m_zt += p.z() * p.t();
    }
  }
  return sc; 
}

StatusCode IVertexSmearingTool_test::initialize() 
{ 
  m_vertexSmearingTool = tool< IVertexSmearingTool >( m_vertexSmearingToolName , this ) ;
  return StatusCode::SUCCESS; 
}

StatusCode IVertexSmearingTool_test::finalize()
{
  if ( 0 != m_vertexSmearingTool ) release( m_vertexSmearingTool ).ignore(/* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */);
  return GaudiHistoAlg::finalize();
}

