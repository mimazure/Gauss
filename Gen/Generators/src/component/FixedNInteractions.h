/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// $Id: FixedNInteractions.h,v 1.4 2009-04-07 16:11:21 gcorti Exp $
#ifndef GENERATORS_FIXEDNINTERACTIONS_H
#define GENERATORS_FIXEDNINTERACTIONS_H 1

// Include files
// from Gaudi
#include "GaudiAlg/GaudiTool.h"

#include "Generators/IPileUpTool.h"

/** @class FixedNInteractions FixedNInteractions.h "FixedNInteractions.h"
 *
 *  Tool to generate fixed number of pile-up interactions. Concrete
 *  implementation of IPileUpTool.
 *
 *  @author Patrick Robbe
 *  @date   2005-08-17
 */
class FixedNInteractions : public extends<GaudiTool,IPileUpTool> {
 public:
  /// Standard constructor
  using extends::extends;
  
  /// Initialize method
  StatusCode initialize( ) override;

  /** Returns a constant number of pile-up interactions.
   *  Implementation of IPileUpTool::numberOfPileUp.
   *  Returns a luminosity equal to 0.
   */
  unsigned int numberOfPileUp( ) override;

  /// Implementation of IPileUpTool::printPileUpCounters
  void printPileUpCounters( ) override;

 private:
  /// Number of interactions to generate (set by job options)
  Gaudi::Property<int> m_nInteractions{this,"NInteractions",1,"NInteractions"} ;
};
#endif // GENERATORS_FIXEDNINTERACTIONS_H
