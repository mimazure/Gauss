/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// $Id: MergedEventsFilter.cpp,v 1.1 2008-05-06 08:27:55 gcorti Exp $
// Include files 

// from Gaudi

// from LHCb
#include "Event/GenHeader.h"
#include "Event/GenCollision.h"

// from Kernel
#include "MCInterfaces/IFullGenEventCutTool.h"

// local
#include "MergedEventsFilter.h"

//-----------------------------------------------------------------------------
// Implementation file for class : MergedEventsFilter
//
// 2008-04-30 : Gloria CORTI
//-----------------------------------------------------------------------------

// Declaration of the Algorithm Factory
DECLARE_COMPONENT( MergedEventsFilter )


//=============================================================================
// Initialization
//=============================================================================
StatusCode MergedEventsFilter::initialize() {
  StatusCode sc = GaudiAlgorithm::initialize(); // must be executed first
  if ( sc.isFailure() ) return sc;  // error printed already by GaudiAlgorithm

  if ( msgLevel(MSG::DEBUG) ) debug() << "==> Initialize" << endmsg;

  // Retrieve full gen event cut tool
  if ( "" != m_fullGenEventCutToolName.value() ) {
    m_fullGenEventCutTool =
      tool< IFullGenEventCutTool >( m_fullGenEventCutToolName.value() , this ) ;
  }
  else {
    info() << "Filter does not have a cut::: MUST be specified!!!" << endmsg;
    return StatusCode::FAILURE;
  }

  return StatusCode::SUCCESS;
}

//=============================================================================
// Main execution
//=============================================================================
StatusCode MergedEventsFilter::execute() {

  if ( msgLevel(MSG::DEBUG) ) debug() << "==> Execute" << endmsg;

  // Retrieve info from the TES
  LHCb::HepMCEvents* theEvents = 
    get<LHCb::HepMCEvents>( m_hepMCEventLocation.value() );

  LHCb::GenCollisions* theCollisions = 
    get<LHCb::GenCollisions>( m_genCollisionLocation.value() );
  

  // Apply generator level cut on full event
  bool goodEvent = true;
  
  //  ++m_nBeforeFullEvent ;
  goodEvent = m_fullGenEventCutTool->studyFullEvent( theEvents, theCollisions );
  // put counters
  setFilterPassed(goodEvent);
  
  return StatusCode::SUCCESS;
}

//=============================================================================
//  Finalize
//=============================================================================
StatusCode MergedEventsFilter::finalize() {

  if ( msgLevel(MSG::DEBUG) ) debug() << "==> Finalize" << endmsg;

  return GaudiAlgorithm::finalize();  // must be called after all other actions
}

//=============================================================================
