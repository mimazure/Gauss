/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// $Id: CollidingBeams.h,v 1.3 2005-12-31 17:32:01 robbep Exp $
#ifndef GENERATORS_COLLIDINGBEAMS_H
#define GENERATORS_COLLIDINGBEAMS_H 1

// Include files
// from Gaudi
#include "GaudiAlg/GaudiTool.h"
#include "GaudiKernel/RndmGenerators.h"

#include "Generators/IBeamTool.h"

// From Event
#include "Event/BeamParameters.h"

// Forward declarations
class IRndmGenSvc ;

/** @class CollidingBeams CollidingBeams.h "CollidingBeams.h"
 *
 *  Tool to compute colliding beams values. Concrete implementation
 *  of a beam tool.
 *
 *  @author Patrick Robbe
 *  @date   2005-08-18
 */
class CollidingBeams : public extends<GaudiTool,IBeamTool> {
 public:
  /// Standard constructor
  using extends::extends;

  /// Initialize method
  StatusCode initialize( ) override;

  /** Implements IBeamTool::getMeanBeams
   */
  void getMeanBeams( Gaudi::XYZVector & pBeam1 ,
                     Gaudi::XYZVector & pBeam2 ) const override;

  /** Implements IBeamTool::getBeams
   *  Compute beam 3-momentum taking into account the horizontal and vertical
   *  beam angles (given by job options). These angles are Gaussian-smeared
   *  with an angular smearing equal to (emittance/beta*)^1/2.
   */
  void getBeams( Gaudi::XYZVector & pBeam1 ,
                 Gaudi::XYZVector & pBeam2 ) override;

 private:
  Gaudi::Property<std::string> m_beamParameters{this,"BeamParameters",LHCb::BeamParametersLocation::Default,"BeamParameters"} ; ///< Location of beam parameters (set by options)

  Rndm::Numbers m_gaussianDist ; ///< Gaussian random number generator
};
#endif // GENERATORS_COLLIDINGBEAMS_H
