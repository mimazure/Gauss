/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// $Id: FixedLuminosityForRareProcess.cpp,v 1.3 2009-04-07 16:11:21 gcorti Exp $
// Include files

// local
#include "FixedLuminosityForRareProcess.h"

// from Gaudi
#include "GaudiKernel/IRndmGenSvc.h"
#include "GaudiKernel/SystemOfUnits.h"

// From Event
#include "Event/GenCountersFSR.h"

// From Generators
#include "Generators/GenCounters.h"
#include "Generators/ICounterLogFile.h"

//-----------------------------------------------------------------------------
// Implementation file for class : FixedLuminosityForRareProcess
//
// 2005-08-17 : Patrick Robbe
//-----------------------------------------------------------------------------

// Declaration of the Tool Factory

DECLARE_COMPONENT( FixedLuminosityForRareProcess )

//=============================================================================
// Initialize method
//=============================================================================
StatusCode FixedLuminosityForRareProcess::initialize( ) {
  StatusCode sc = GaudiTool::initialize( ) ;
  if ( sc.isFailure() ) return sc ;

  // Initialize the number generator
  m_randSvc = svc< IRndmGenSvc >( "RndmGenSvc" , true ) ;

  // XML file for generator statistics
  m_xmlLogTool = tool< ICounterLogFile >( "XmlCounterLogFile" ) ;

  info() << "Poisson distribution with fixed luminosity. " << endmsg ;

  return sc ;
}

//=============================================================================
// Compute the number of pile up to generate according to beam parameters
//=============================================================================
unsigned int FixedLuminosityForRareProcess::numberOfPileUp( ) {
  LHCb::BeamParameters * beam = get< LHCb::BeamParameters >( m_beamParameters.value() ) ;
  if ( 0 == beam ) Exception( "No beam parameters registered" ) ;

  LHCb::GenFSR* genFSR = nullptr;
  if(m_FSRName.value() != ""){
    IDataProviderSvc* fileRecordSvc = svc<IDataProviderSvc>("FileRecordDataSvc", true);
    genFSR = getIfExists<LHCb::GenFSR>(fileRecordSvc, m_FSRName.value(), false);
    if(!genFSR) warning() << "Could not find GenFSR at " << m_FSRName.value() << endmsg;
  }

  unsigned int result = 0 ;
  m_nEvents++ ;
  if(genFSR) genFSR->incrementGenCounter(LHCb::GenCountersFSR::AllEvt,1);

  Rndm::Numbers poissonGenerator( m_randSvc , Rndm::Poisson( beam -> nu() ) ) ;
  result = (unsigned int) ( poissonGenerator() + 1.0 ) ;
  return result ;
}

//=============================================================================
// Print the specific pile up counters
//=============================================================================
void FixedLuminosityForRareProcess::printPileUpCounters( ) {
  using namespace GenCounters ;
  printCounter( m_xmlLogTool , "all events (including empty events)", m_nEvents ) ;
}

//=============================================================================
// Finalize method
//=============================================================================
StatusCode FixedLuminosityForRareProcess::finalize( ) {
  release( m_randSvc ).ignore(/* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */);
  return GaudiTool::finalize( ) ;
}
