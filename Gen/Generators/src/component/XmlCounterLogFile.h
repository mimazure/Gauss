/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef GENERATORS_XMLCOUNTERLOGFILE_H
#define GENERATORS_XMLCOUNTERLOGFILE_H 1

// Include files
// from STL
#include <string>
#include <fstream>

// from Gaudi
#include "GaudiAlg/GaudiTool.h"
#include "Generators/ICounterLogFile.h"            // Interface

/** @class XmlCounterLogFile XmlCounterLogFile.h component/XmlCounterLogFile.h
 *
 *  Tool to write counters in a xml file
 *
 *  @author Patrick Robbe
 *  @date   2013-01-15
 */
class XmlCounterLogFile : public extends<GaudiTool, ICounterLogFile> {
public:
  /// Standard constructor
  using extends::extends;
  /// Initialize method
  StatusCode initialize( ) override;

  /// Finalize method
  StatusCode finalize( ) override;

  /// Add efficiency number
  void addEfficiency( const std::string & name , const unsigned int after ,
                      const unsigned int before , const double fraction ,
                      const double err_fraction ) override;

  void addCounter( const std::string & name , const unsigned int value ) override;

  void addFraction( const std::string & name , const unsigned int number ,
                    const double fraction , const double err_fraction ) override;

  void addCrossSection( const std::string & name ,
                        const unsigned int processId ,
                        const unsigned int number ,
                        const double value ) override;

  void addEventType( const unsigned int evType ) override;

  /// Add the Gauss version
  void addGaussVersion( const std::string & version ) override;

  void addMethod( const std::string & type ) override;

  void addGenerator( const std::string & generator ) override;

protected:

private:
  std::string     m_version{"1.1"} ;
  std::ofstream   m_file ;
  Gaudi::Property<std::string> m_fileName{this,"FileName","GeneratorLog.xml", "Output file name"} ;

};
#endif // GENERATORS_XMLCOUNTERLOGFILE_H
