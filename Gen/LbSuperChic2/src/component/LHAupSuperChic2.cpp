/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// SuperChic 2.
#include "SuperChic2/SuperChic2.h"

// Local.
#include "LHAupSuperChic2.h"

//-----------------------------------------------------------------------------
//  Implementation file for class: LHAupSuperChic2
//
//  2016-08-30 : Philip Ilten
//-----------------------------------------------------------------------------

//=============================================================================
// Default constructor.
//=============================================================================
Pythia8::LHAupSuperChic2::LHAupSuperChic2(Pythia *pythiaIn, string dirIn) : 
  dir(dirIn), pre("SuperChic2:") {
  if (!pythiaIn) return;
  pythia = pythiaIn;
  
  // Primary settings.
  pythia->readString("Beams:frameType = 5");
  pythia->readString("PartonLevel:MPI = off");
  pythia->readString("PartonLevel:ISR = off");
  pythia->readString("PartonLevel:Remnants = off");
  pythia->settings.addParm(pre + "rts", 13000, true, false, 0, 0);
  pythia->settings.addMode(pre + "isurv", 4, true, true, 1, 4);
  pythia->settings.addWord(pre + "intag", "in");
  pythia->settings.addWord(pre + "PDFname", "MMHT2014lo68cl");
  pythia->settings.addMode(pre + "PDFmember", 0, true, false, 0, 0);
  pythia->settings.addMode(pre + "proc", 50, true, true, 1, 67);
  pythia->settings.addWord(pre + "outtag", "out");
  pythia->settings.addFlag(pre + "sfaci", true);

  // Integration settings.
  pythia->settings.addMode(pre + "ncall", 1000, true, false, 0, 0);
  pythia->settings.addMode(pre + "itmx", 10, true, false, 0, 0);
  pythia->settings.addParm(pre + "prec", 0.5, true, true, 0, 100);
  pythia->settings.addMode(pre + "ncall1", 5000, true, false, 0, 0);
  pythia->settings.addMode(pre + "inccall", 5000, true, false, 0, 0);
  pythia->settings.addMode(pre + "itend", 5000, true, false, 0, 0);
  pythia->settings.addMode(pre + "iseed", -1, true, false, -1, 0);
  pythia->settings.addMode(pre + "s2int", 4, true, false, 0, 0);
  
  // Generation settings.
  pythia->settings.addFlag(pre + "genunw", true);
  pythia->settings.addFlag(pre + "readwt", false);
  pythia->settings.addParm(pre + "wtmax", 0, true, false, 0, 0);
  pythia->settings.addParm(pre + "ymin", 0, false, false, 0, 0);
  pythia->settings.addParm(pre + "ymax", 6, false, false, 0, 0);
  pythia->settings.addParm(pre + "mmin", 0, true, false, 0, 0);
  pythia->settings.addParm(pre + "mmax", 13000, true, false, 0, 0);
  pythia->settings.addFlag(pre + "gencuts", false);
  pythia->settings.addFlag(pre + "scorr", true);
  pythia->settings.addFlag(pre + "fwidth", true);

  // Kinematic requirement settings.
  pythia->settings.addParm(pre + "ptamin", 0, true, false, 0, 0);
  pythia->settings.addParm(pre + "ptbmin", 0, true, false, 0, 0);
  pythia->settings.addParm(pre + "ptcmin", 0, true, false, 0, 0);
  pythia->settings.addParm(pre + "ptdmin", 0, true, false, 0, 0);
  pythia->settings.addParm(pre + "ptemin", 0, true, false, 0, 0);
  pythia->settings.addParm(pre + "ptfmin", 0, true, false, 0, 0);
  pythia->settings.addParm(pre + "etaamin", 0, false, false, 0, 0);
  pythia->settings.addParm(pre + "etabmin", 0, false, false, 0, 0);
  pythia->settings.addParm(pre + "etacmin", 0, false, false, 0, 0);
  pythia->settings.addParm(pre + "etadmin", 0, false, false, 0, 0);
  pythia->settings.addParm(pre + "etaemin", 0, false, false, 0, 0);
  pythia->settings.addParm(pre + "etafmin", 0, false, false, 0, 0);
  pythia->settings.addParm(pre + "etaamax", 5, false, false, 0, 0);
  pythia->settings.addParm(pre + "etabmax", 5, false, false, 0, 0);
  pythia->settings.addParm(pre + "etacmax", 5, false, false, 0, 0);
  pythia->settings.addParm(pre + "etadmax", 5, false, false, 0, 0);
  pythia->settings.addParm(pre + "etaemax", 5, false, false, 0, 0);
  pythia->settings.addParm(pre + "etafmax", 5, false, false, 0, 0);
  
  // Jet and decay settings.
  pythia->settings.addParm(pre + "rjet", 0.6, true, false, 0, 0);
  pythia->settings.addWord(pre + "jalg", "antikt");
  pythia->settings.addFlag(pre + "decays", true);
  pythia->settings.addParm(pre + "m2b", 0.133, false, false, 0, 0);
  pythia->settings.addMode(pre + "pdgid1", 211, false, false, 0, 0);
  pythia->settings.addMode(pre + "pdgid2", -211, false, false, 0, 0);

  // Create the run directory structure.
  mkdir(dir.c_str(), 0777);
  mkdir((dir + "/inputs").c_str(), 0777);
  mkdir((dir + "/outputs").c_str(), 0777);
  mkdir((dir + "/evrecs").c_str(), 0777);
}

//=============================================================================
// Initialize the SuperChic2 generator.
//=============================================================================
bool Pythia8::LHAupSuperChic2::setInit() {
  // Get primary settings.
  vars_.rts[0]         = pythia->parm(pre + "rts");
  survin_.isurv[0]     = pythia->mode(pre + "isurv");
  pdfint_.pdfmember[0] = pythia->mode(pre + "PDFmember");
  process_.proc[0]     = pythia->mode(pre + "proc");
  survin_.sfaci[0]     = pythia->flag(pre + "sfaci");
  chrcpy(in_.intag, pythia->word(pre + "intag"), 100);
  chrcpy(pdfint_.pdfname, pythia->word(pre + "PDFname"), 100);
  chrcpy(out_.outtag, pythia->word(pre + "outtag"), 100);
  chrcpy(record_.erec, "lhe", 10);

  // Handle the INIT settings (non-unique common block names).
  initcom_.irts[0]       = pythia->parm(pre + "rts");
  initcom_.iisurv[0]     = pythia->mode(pre + "isurv");
  initcom_.ipdfmember[0] = pythia->mode(pre + "PDFmember");
  chrcpy(initcom_.iintag, pythia->word(pre + "intag"), 100);
  chrcpy(initcom_.ipdfname, pythia->word(pre + "PDFname"), 100);

  // Get integration settings.
  bveg1_.ncall[0] = pythia->mode(pre + "ncall");
  bveg1_.itmx[0]  = pythia->mode(pre + "itmx");
  prec_.prec[0]   = pythia->parm(pre + "prec");
  veg_.ncall1[0]  = pythia->mode(pre + "ncall1");
  veg_.inccall[0] = pythia->mode(pre + "inccall");
  veg_.itend[0]   = pythia->mode(pre + "itend");
  veg_.iseed[0]   = pythia->mode(pre + "iseed");
  nsurv_.s2int[0] = pythia->mode(pre + "s2int");

  // Get generation settings.
  genunw_.genunw[0]  = pythia->flag(pre + "genunw");
  wtmax_.readwt[0]   = pythia->flag(pre + "readwt");
  wtmax_.wtmax[0]    = pythia->parm(pre + "wtmax");
  yrange_.ymin[0]    = pythia->parm(pre + "ymin");
  yrange_.ymax[0]    = pythia->parm(pre + "ymax");
  mrange_.mmin[0]    = pythia->parm(pre + "mmin");
  mrange_.mmax[0]    = pythia->parm(pre + "mmax");
  gencut_.gencuts[0] = pythia->flag(pre + "gencuts");
  corr_.scorr[0]     = pythia->flag(pre + "scorr");
  widths_.fwidth[0]  = pythia->flag(pre + "fwidth");

  // Get kinematic requirement settings.
  cuts_.ptamin[0]   = pythia->parm(pre + "ptamin");
  cuts_.ptamin3[0]  = pythia->parm(pre + "ptamin");
  cuts_.ptamin4[0]  = pythia->parm(pre + "ptamin");
  cuts_.ptamin6[0]  = pythia->parm(pre + "ptamin");
  cuts_.ptbmin[0]   = pythia->parm(pre + "ptbmin");
  cuts_.ptbmin3[0]  = pythia->parm(pre + "ptbmin");
  cuts_.ptbmin4[0]  = pythia->parm(pre + "ptbmin");
  cuts_.ptbmin6[0]  = pythia->parm(pre + "ptbmin");
  cuts_.ptcmin3[0]  = pythia->parm(pre + "ptcmin");
  cuts_.ptcmin4[0]  = pythia->parm(pre + "ptcmin");
  cuts_.ptcmin6[0]  = pythia->parm(pre + "ptcmin");
  cuts_.ptdmin4[0]  = pythia->parm(pre + "ptdmin");
  cuts_.ptdmin6[0]  = pythia->parm(pre + "ptdmin");
  cuts_.ptemin6[0]  = pythia->parm(pre + "ptemin");
  cuts_.ptfmin6[0]  = pythia->parm(pre + "ptfmin");
  cuts_.etaamin[0]  = pythia->parm(pre + "etaamin");
  cuts_.etaamin3[0] = pythia->parm(pre + "etaamin");
  cuts_.etaamin4[0] = pythia->parm(pre + "etaamin");
  cuts_.etaamin6[0] = pythia->parm(pre + "etaamin");
  cuts_.etabmin[0]  = pythia->parm(pre + "etabmin");
  cuts_.etabmin3[0] = pythia->parm(pre + "etabmin");
  cuts_.etabmin4[0] = pythia->parm(pre + "etabmin");
  cuts_.etabmin6[0] = pythia->parm(pre + "etabmin");
  cuts_.etacmin3[0] = pythia->parm(pre + "etacmin");
  cuts_.etacmin4[0] = pythia->parm(pre + "etacmin");
  cuts_.etacmin6[0] = pythia->parm(pre + "etacmin");
  cuts_.etadmin4[0] = pythia->parm(pre + "etadmin");
  cuts_.etadmin6[0] = pythia->parm(pre + "etadmin");
  cuts_.etaemin6[0] = pythia->parm(pre + "etaemin");
  cuts_.etafmin6[0] = pythia->parm(pre + "etafmin");
  cuts_.etaamax[0]  = pythia->parm(pre + "etaamax");
  cuts_.etaamax3[0] = pythia->parm(pre + "etaamax");
  cuts_.etaamax4[0] = pythia->parm(pre + "etaamax");
  cuts_.etaamax6[0] = pythia->parm(pre + "etaamax");
  cuts_.etabmax[0]  = pythia->parm(pre + "etabmax");
  cuts_.etabmax3[0] = pythia->parm(pre + "etabmax");
  cuts_.etabmax4[0] = pythia->parm(pre + "etabmax");
  cuts_.etabmax6[0] = pythia->parm(pre + "etabmax");
  cuts_.etacmax3[0] = pythia->parm(pre + "etacmax");
  cuts_.etacmax4[0] = pythia->parm(pre + "etacmax");
  cuts_.etacmax6[0] = pythia->parm(pre + "etacmax");
  cuts_.etadmax4[0] = pythia->parm(pre + "etadmax");
  cuts_.etadmax6[0] = pythia->parm(pre + "etadmax");
  cuts_.etaemax6[0] = pythia->parm(pre + "etaemax");
  cuts_.etafmax6[0] = pythia->parm(pre + "etafmax");
  
  // Get jet and decay settings.
  jetalg_.rjet[0] = pythia->parm(pre + "rjet");
  decays          = pythia->flag(pre + "decays");
  masses_.m2b[0]  = 0;
  pdg_.pdgid[5]   = pythia->mode(pre + "pdgid1");
  pdg_.pdgid[6]   = pythia->mode(pre + "pdgid2");
  chrcpy(jetalg_.jalg, pythia->word(pre + "jalg"), 10);

  // If a two-body decay, set the combined mass.
  if (pythia->particleData.isParticle(pdg_.pdgid[5]) &&
      pythia->particleData.isParticle(pdg_.pdgid[6]))
    masses_.m2b[0] = pythia->particleData.m0(pdg_.pdgid[5]) +
      pythia->particleData.isParticle(pdg_.pdgid[6]);

  // Set the electroweak parameters and particle masses.
  pythia->couplings.init(pythia->settings, &pythia->rndm);
  ewpars_.gf[0]       = pythia->couplings.GF();
  ewpars_.v[0]        = sqrt(1.0 / (ewpars_.gf[0]*sqrt(2)));
  ewpars_.mt[0]       = pythia->particleData.m0(6);
  ewpars_.mb[0]       = pythia->particleData.m0(5);
  masses_.mc[0]       = pythia->particleData.m0(4);
  masses_.mmu[0]      = pythia->particleData.m0(13);
  masses_.mpsi[0]     = pythia->particleData.m0(443);
  masses_.mpsip[0]    = pythia->particleData.m0(100443);
  masses_.mups[0]     = pythia->particleData.m0(553);
  masses0_.mchic0[0]  = pythia->particleData.m0(10441);
  masses0_.mchib0[0]  = pythia->particleData.m0(10551);;
  pmass_.mp[0]        = pythia->particleData.m0(2212);
  ewpars_.mw[0]       = pythia->particleData.m0(24);
  ewpars_.me[0]       = pythia->particleData.m0(11);
  ewpars_.mtau[0]     = pythia->particleData.m0(15);
  mpi_.mpip[0]        = pythia->particleData.m0(211);
  mkp_.mkp[0]         = pythia->particleData.m0(321);
  ewpars_.alpha[0]    = pythia->couplings.alphaEM(0);

  // Run the SuperChic2 grid initialization if needed.
  getcwd(cwd, sizeof(cwd));
  chdir(dir.c_str());
  if (access(("inputs/hg" + pythia->word(pre + "intag") + ".dat").c_str(), 
	     F_OK) == -1) {
    initcom_.entryp[0] = 1000;
    initcom_.exitp[0]  = 1002;
    sbr_init__();
    initcom_.entryp[0] = 1003;
    initcom_.exitp[0]  = 1001;
    sbr_init__();
  }

  // Initialize SuperChic2.
  sccom_.entryp[0] = 1003;
  sccom_.exitp[0]  = 1004;
  sbr_superchic__();
  sccom_.entryp[0] = 1005;
  sccom_.exitp[0]  = 1008;
  sbr_superchic__();
  chdir(cwd);

  // Set up Pythia.
  setStrategy(3);
  setBeamA(pdg_.pdgid[0], vars_.rts[0]/2.);
  setBeamB(pdg_.pdgid[1], vars_.rts[0]/2.);
  addProcess(process_.proc[0], sccom_.avgi[0], sccom_.sd[0],
	     wmax_.wmax[0]);
  return true;
}

//=============================================================================
// Generate an event with SuperChic2.
//=============================================================================
bool Pythia8::LHAupSuperChic2::setEvent(int) {
  // Generate events if needed.
  int itrs(0);
  while (unweighted_.nev[0] >= unweighted_.evnum[0] && itrs < 100) {
    getcwd(cwd, sizeof(cwd));
    chdir(dir.c_str());
    unweighted_.nev[0]   = 0;
    unweighted_.evnum[0] = 0;
    sccom_.entryp[0] = 1008;
    sccom_.exitp[0]  = 1009;
    sbr_superchic__();
    ++itrs;
    chdir(cwd);
  }

  // Fill the event record.
  setProcess(process_.proc[0], 0., 0., 0., 0.);

  // Create the particles.
  vector<LHAParticle> prts;
  for (int i = 0; i < 500; ++i) {
    // Break if no decays, and decays have been reached.
    if (!decays && i > 0 && hepeup_.mothup[i][0] != 0 && hepeup_.mothup[i][1] 
	== 0 && pdg_.pdgid[hepeup_.mothup[i][0] - 1] != 93) break;

    // Break if no valid particles remain.
    if (pdg_.pdgid[i] == 0) break;

    // Fill the standard LHE block.
    hepeup_.idup[i] = pdg_.pdgid[i];
    hepeup_.pup[i][4] = pythia->particleData.m0(pdg_.pdgid[i]);
    if (abs(hepeup_.istup[0]) != 1)
      hepeup_.vtimup[i] = pythia->particleData.tau0(pdg_.pdgid[i]) * 
	pythia->rndm.exp();
    if (i > 1) {
      hepeup_.pup[i][0] = rec_.evrec[0][i][unweighted_.nev[0]];
      hepeup_.pup[i][1] = rec_.evrec[1][i][unweighted_.nev[0]];
      hepeup_.pup[i][2] = rec_.evrec[2][i][unweighted_.nev[0]];
      hepeup_.pup[i][3] = rec_.evrec[3][i][unweighted_.nev[0]];
      hepeup_.pup[i][4] = sqrtpos
	(pow2(hepeup_.pup[i][3]) - pow2(hepeup_.pup[i][0]) - 
	 pow2(hepeup_.pup[i][1]) - pow2(hepeup_.pup[i][2]));
    }

    // Fix the mother numbering.
    int mothup0(hepeup_.mothup[i][0]), mothup1(hepeup_.mothup[i][1]);
    if (mothup0 > 2) mothup0 += 2;
    if (mothup1 > 2) mothup1 += 2;
    if (hepeup_.istup[i] > 0 && mothup0 == 0 && mothup1 == 0) {
      mothup0 = 1; mothup1 = 2;}

    // Fix the status codes.
    if (hepeup_.istup[i] >= 0) hepeup_.istup[i] = 1;
    if (mothup0 != 0 && mothup1 == 0 && mothup0 - 1 < (int)prts.size())
      prts[mothup0 - 1].statusPart = 2;

    // Store the particle and switch off resonance if needed.
    prts.push_back(LHAParticle
		   (hepeup_.idup[i], hepeup_.istup[i], mothup0, mothup1,
		    hepeup_.icolup[i][0], hepeup_.icolup[i][1], 
		    hepeup_.pup[i][0], hepeup_.pup[i][1], hepeup_.pup[i][2], 
		    hepeup_.pup[i][3], hepeup_.pup[i][4], hepeup_.vtimup[i], 
		    hepeup_.spinup[i], 0));
    pythia->particleData.isResonance(pdg_.pdgid[i], false);
  }

  // Set the event.
  for (int i = 0; i < (int)prts.size(); ++i) addParticle(prts[i]);
  setPdf(hepeup_.idup[0], hepeup_.idup[1], hepeup_.pup[0][3]/eBeamA(),
	 hepeup_.pup[1][3]/eBeamB(), 0., 0., 0., false);
  setXSec(0, sccom_.avgi[0]*1e9);
  setXErr(0, sccom_.sd[0]*1e9);
  ++unweighted_.nev[0];
  return true;
}

//=============================================================================
// Copy a string to a FORTRAN variable.
//=============================================================================
void Pythia8::LHAupSuperChic2::chrcpy(char *f, string c, int n) {
  for (int i = 0; i < n; ++i) f[i] = i >= (int)c.size() ? ' ' : c[i];}

//=============================================================================
// The END.
//=============================================================================
