/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// $Id: VeloGaussMoni.h,v 1.5 2009-03-26 22:02:12 robbep Exp $
#ifndef VELOGAUSSMONI_H
#define VELOGAUSSMONI_H 1

// Include files
// from Gaudi
#include "GaudiAlg/GaudiTupleAlg.h"
#include "Event/MCHit.h"

/** @class VeloGaussMoni VeloGaussMoni.h
 *
 *
 *  @author Tomasz Szumlak & Chris Parkes
 *  @date   2005-12-13
 */

class DeVelo;
class DeVeloRType;
class DeVeloPhiType;

class VeloGaussMoni : public GaudiTupleAlg {
public:
  /// Standard constructor
  VeloGaussMoni( const std::string& name, ISvcLocator* pSvcLocator );

  virtual ~VeloGaussMoni( ); ///< Destructor

  StatusCode initialize() override;    ///< Algorithm initialization
  StatusCode execute   () override;    ///< Algorithm execution
  StatusCode finalize  () override;    ///< Algorithm finalization

protected:

  StatusCode checkTests();
  StatusCode getData();
  StatusCode veloMCHitMonitor();
  StatusCode veloPileUpMCHitMonitor();
  StatusCode basicMonitor();

private:

  std::string m_veloDetLocation;
  DeVelo* m_veloDet;
  LHCb::MCHits* m_veloMCHits;
  LHCb::MCHits* m_veloPileUpMCHits;
  int m_print;
  bool m_printInfo;
  bool m_detailedMonitor;
  bool m_testMCHit;
  bool m_testPileUpMCHit;
  double m_nMCH;
  double m_nMCH2;
  double m_nPUMCH;
  double m_nPUMCH2;
  int m_nEvent;
  /// Location of Velo MCHits
  std::string m_veloMCHitsLocation ;
  /// Location of PuVeto MCHits
  std::string m_puVetoMCHitsLocation ;
};
#endif // VELOGAUSSMONI_H
