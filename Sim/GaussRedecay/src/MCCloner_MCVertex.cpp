/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "MCCloner.h"

LHCb::MCVertex* MCCloner::getStoredMCV(const LHCb::MCVertex* mcv) {
    auto result = m_mcvs.find(mcv);
    if (result == m_mcvs.end()) {
        return nullptr;
    } else {
        return result->second;
    }
}

LHCb::MCVertex* MCCloner::cloneKeyedMCV(const LHCb::MCVertex* mcv) {
    auto clone = getStoredMCV(mcv);
    if (!clone) {
        clone = new LHCb::MCVertex();
        clone->setPosition(mcv->position());
        clone->setTime(mcv->time());
        clone->setType(mcv->type());
        if (m_clone_key) {
            getClonedMCVs()->insert(clone, mcv->key());
        } else {
            getClonedMCVs()->insert(clone);
        }
        m_mcvs.insert(
            std::pair<const LHCb::MCVertex*, LHCb::MCVertex*>(mcv, clone));
    }
    return clone;
}

LHCb::MCVertex* MCCloner::cloneMCV(const LHCb::MCVertex* vertex) {
    if (!vertex) return NULL;
    LHCb::MCVertex* clone = getStoredMCV(vertex);

    const size_t nProd = vertex->products().size();
    const size_t nCloneProd = (clone ? clone->products().size() : 0);

    return (clone && (nProd == nCloneProd) ? clone : this->doCloneMCV(vertex));
}

LHCb::MCVertex* MCCloner::doCloneMCV(const LHCb::MCVertex* vertex) {
    LHCb::MCVertex* clone = cloneKeyedMCV(vertex);

    clone->setMother(cloneMCP(vertex->mother()));

    clone->clearProducts();

    cloneDecayProducts(vertex->products(), clone);

    return clone;
}

LHCb::MCVertices* MCCloner::getClonedMCVs() {
    if (!m_list_mcvs) {
        m_list_mcvs = new LHCb::MCVertices();
    }
    return m_list_mcvs;
}
