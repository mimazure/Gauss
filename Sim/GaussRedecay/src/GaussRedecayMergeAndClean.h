/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef GaussRedecayMergeAndClean_H
#define GaussRedecayMergeAndClean_H 1

// Include files
// from Gaudi
#include "GaudiAlg/GaudiAlgorithm.h"
#include <utility>
#include <set>
#include "Event/MCVertex.h"

class MCCloner;
class IGaussRedecayStr;
class IGaussRedecayCtr;
namespace LHCb {
class MCVertex;
class MCParticle;
}


/** @class GaussRedecayMergeAndClean GaussRedecayMergeAndClean.h
 *
 * Algorithm to merge all the signal stuff back into the main containers
 * and clean the signal tree in the TES.
 *
 *  @author Dominik Muller
 *  @date   2016-4-1
 */
class GaussRedecayMergeAndClean : public GaudiAlgorithm {
  public:
  /// Standard constructor
  GaussRedecayMergeAndClean(const std::string& Name, ISvcLocator* SvcLoc);

  StatusCode initialize() override;  ///< Algorithm initialization
  StatusCode execute() override;     ///< Algorithm execution

  protected:
  /** accessor to GaussRedecay Service
   *  @return pointer to GaussRedecay Service
   */
  private:
  /* Finds the vertex in the signal origin vertex in the full event
   * using the placeholder id. Also removes the placeholder from
   * the vertex and the list of particles.
   */
  LHCb::MCVertex* findVertex(int placeholder);
  /* Finds the MCParticle of the given placeholder in the provided
   * list of particles.
   */
  LHCb::MCParticle* findPlaceholder(const LHCb::MCParticles* parts, int placeholder);
  std::string m_gaussRDSvcName;
  IGaussRedecayStr* m_gaussRDStrSvc;
  IGaussRedecayCtr* m_gaussRDCtrSvc;
  MCCloner* m_temp_cloner = nullptr;

  std::string m_particlesLocation;
  std::string m_verticesLocation;
  std::vector<std::string> m_hitsLocations;
  std::vector<std::string> m_calohitsLocations;
  std::string m_richHitsLocation;
  std::string m_richOpticalPhotonsLocation;
  std::string m_richSegmentsLocation;
  std::string m_richTracksLocation;
  std::string m_signal_tes_prefix;
  std::string m_hepMCEventLocation;
  std::string m_genCollisionLocation;
  std::string m_mcHeader;
  std::set<LHCb::MCParticle*> m_deleted;

  std::pair<LHCb::MCParticles*, LHCb::MCParticles*> m_mcparticles;
  std::pair<LHCb::MCVertices*, LHCb::MCVertices*> m_mcvertices;
  template <typename T>
  std::pair<T*, T*> get_and_print(const std::string&);
  /// Delete a complete tree from event record
  void deleteParticle(LHCb::MCParticle* P, LHCb::MCVertices* m_vertexContainer,
                      LHCb::MCParticles* m_particleContainer);
  void fix_connections(int placeholder, int original_id);
  // Function to get the set of all descendants of an MCParticle.
  // Set will include the MCParticle itself.
  std::set<const LHCb::MCParticle*> all_descendants(const LHCb::MCParticle*);

};

template <typename T>
std::pair<T*, T*> GaussRedecayMergeAndClean::get_and_print(const std::string& loc) {
  auto con = get<T>(loc);
  auto loc_s = m_signal_tes_prefix + loc;
  auto con_s = get<T>(loc_s);
  if (msgLevel(MSG::DEBUG)) {
    debug() << "Got " << con->size() << " from " << loc << endmsg;
    debug() << "Got " << con_s->size() << " from " << loc_s << endmsg;
  }
  return std::pair<T*, T*>(con, con_s);
}
#endif  // GaussRedecayMergeAndClean_H
