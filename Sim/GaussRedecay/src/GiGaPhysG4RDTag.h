/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// ============================================================================
#ifndef    GAUSSREDECAY_GIGAPHYSG4RDTAG_H
#define    GAUSSREDECAY_GIGAPHYSG4RDTAG_H 1
// ============================================================================
// include files
// ============================================================================
// GiGa
// ============================================================================
#include "GiGa/GiGaPhysConstructorBase.h"
#include "G4Decay.hh"
#include "G4UnknownDecay.hh"
// ============================================================================
// forward declarations
template <class TYPE> class GiGaFactory;

class GiGaPhysG4RDTag : public GiGaPhysConstructorBase
{
  /// friend factory for instantiation
  friend class GiGaFactory<GiGaPhysG4RDTag>;

public:

  GiGaPhysG4RDTag
  ( const std::string& type   ,
    const std::string& name   ,
    const IInterface*  parent ) ;


public:
  void ConstructParticle () override;
  void ConstructProcess  () override;

private:

  G4Decay m_decayProcess ;
  G4UnknownDecay m_unknownDecay ;
  int m_g4_reserve = 0;
  int m_initial_placeholder = 424242;

};

#endif
