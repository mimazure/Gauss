/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// $Id: MCDecayCounter.cpp,v 1.6 2007-01-12 15:28:40 ranjard Exp $
// Include files 

// from Gaudi
#include "GaudiKernel/MsgStream.h" 
#include "GaudiKernel/SmartDataPtr.h"

// from Event
#include "Event/MCParticle.h"

// local
#include "MCDecayCounter.h"

//-----------------------------------------------------------------------------
// Implementation file for class : MCDecayCounter
//
// 2004-04-26 : Gloria CORTI
//-----------------------------------------------------------------------------

// Declaration of the Algorithm Factory
DECLARE_COMPONENT( MCDecayCounter )




//=============================================================================
// Initialisation. Check parameters
//=============================================================================
StatusCode MCDecayCounter::initialize() {

  StatusCode sc = GaudiAlgorithm::initialize(); // must be executed first
  if ( sc.isFailure() ) return sc;  // error printed already by GaudiAlgorithm

  debug() << "==> Initialize" << endmsg;

  // Retrieve MCDecayfinder tool to check if signal is what looked for
  m_mcFinder = tool<IMCDecayFinder>( "MCDecayFinder", this );

  return StatusCode::SUCCESS;
}

//=============================================================================
// Main execution
//=============================================================================
StatusCode MCDecayCounter::execute() {

  debug() << "==> Execute" << endmsg;

  // Counter of events processed
  m_nEvents++;
  
  LHCb::MCParticles* kmcparts =
    get<LHCb::MCParticles>( eventSvc(), LHCb::MCParticleLocation::Default );
  
  // Find decay as described in MCDecayFinder
  const LHCb::MCParticle *mcpart = NULL;
  LHCb::MCParticle::ConstVector mcparts(kmcparts->begin(), kmcparts->end());

  while( m_mcFinder->findDecay( mcparts, mcpart ) ) {
    m_nMCFound++;
    if( m_debug.value() ) {
      verbose() << "Signal info from " << LHCb::MCParticleLocation::Default
                << endmsg;
      verbose() << "== MCParticle id = " << mcpart->particleID().pid()
                << " , momentum = " << mcpart->momentum() 
                << endmsg;
    }
  }
  
  return StatusCode::SUCCESS;
}

//=============================================================================
//  Finalize
//=============================================================================
StatusCode MCDecayCounter::finalize() {

  debug() << "==> Finalize" << endmsg;

  std::string decayAnalyzed = "Unknown";
  if( 0 != m_mcFinder ) {
    decayAnalyzed =  m_mcFinder->decay();
  }  

  double multiplicity = double(m_nMCFound)/double(m_nEvents);

  info() << endmsg
         << " Decay analyzed   = " << decayAnalyzed
         << endmsg
         << "   events processed = " << format( "%8d", m_nEvents )
         << endmsg
         << "   events found     = " << format( "%8d", m_nMCFound )
         << endmsg
         << "   fraction/event   = " << format( "%8.2f", multiplicity )
         << endmsg;

  return GaudiAlgorithm::finalize();  // must be called after all other actions
  
}

//=============================================================================
