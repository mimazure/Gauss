/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// $Id: DecayAnalysis.h,v 1.1 2007-03-07 18:51:42 gcorti Exp $
#ifndef DECAYANALYSIS_H
#define DECAYANALYSIS_H 1

// Include files
// from STL
#include <string>

// from Gaudi
#include "GaudiAlg/GaudiHistoAlg.h"

// From HepMC
#include "Event/HepMCEvent.h"

// from AIDA
#include "AIDA/IHistogram1D.h"
#include "AIDA/IAxis.h"

/** @class DecayAnalysis DecayAnalysis.h
 *
 *  Analysis algorithms for the generator sequences with many reference
 *  histograms for generators comparisons
 *
 *  @author M. Kreps (based on GeneratorAnalysis) 
 *  @date   2020-05-04
 */
class DecayAnalysis : public GaudiHistoAlg {
public:
  /// Standard constructor
  using GaudiHistoAlg::GaudiHistoAlg;

  
  StatusCode initialize() override;    ///< Algorithm initialization
  StatusCode execute   () override;    ///< Algorithm execution
  StatusCode finalize  () override;    ///< Algorithm finalization

protected:
  void bookHistos(const bool neutral); ///< Book histograms
  void normHistos();                   ///< Normalize histograms

private:
  Gaudi::Property<std::string>    m_dataPath{this,"Input", LHCb::HepMCEventLocation::Default ,"location of input data"};            ///< location of input data

  int            m_counter{0};
  int            m_counterStable{0};
  int            m_counterCharged{0};
  int            m_nEvents{0};

  Gaudi::Property<std::string>    m_generatorName{this,"ApplyTo", "" ,"Generator name to apply to"};

  // 'Normal' Histograms
  AIDA::IHistogram1D* m_hNPart;
  AIDA::IHistogram1D* m_hNStable;
  AIDA::IHistogram1D* m_hNSCharg;
  AIDA::IHistogram1D* m_hPartP;
  AIDA::IHistogram1D* m_hPartPDG;
  AIDA::IHistogram1D* m_hProtoP;
  AIDA::IHistogram1D* m_hProtoPDG;
  AIDA::IHistogram1D* m_hProtoLTime;
  AIDA::IHistogram1D* m_hStableEta;
  AIDA::IHistogram1D* m_hStablePt;

  // Basic Histograms for all Charged stable particles
  AIDA::IHistogram1D* m_hAllChargedStableMult     ;
  AIDA::IHistogram1D* m_hAllChargedStablePRap     ;
  AIDA::IHistogram1D* m_hAllChargedStableEnergy   ;
  AIDA::IHistogram1D* m_hAllChargedStableEnergyMax;
  AIDA::IHistogram1D* m_hAllChargedStableP        ;
  AIDA::IHistogram1D* m_hAllChargedStablePMax     ;
  AIDA::IHistogram1D* m_hAllChargedStablePt       ;
  AIDA::IHistogram1D* m_hAllChargedStablePtMax    ;
  AIDA::IHistogram1D* m_hAllChargedStablePID      ;

  // Histograms for neutral stable particles in acceptance
  AIDA::IHistogram1D* m_hNeutralStableMult     ;
  AIDA::IHistogram1D* m_hNeutralStablePRap     ;
  AIDA::IHistogram1D* m_hNeutralStableEnergy   ;
  AIDA::IHistogram1D* m_hNeutralStableEnergyMax;
  AIDA::IHistogram1D* m_hNeutralStableP        ;
  AIDA::IHistogram1D* m_hNeutralStablePMax     ;
  AIDA::IHistogram1D* m_hNeutralStablePt       ;
  AIDA::IHistogram1D* m_hNeutralStablePtMax    ;
  AIDA::IHistogram1D* m_hNeutralStablePID      ;

  // Histograms for all charged stable particles
  AIDA::IHistogram1D* m_hAllParticlesMult     ;
  AIDA::IHistogram1D* m_hAllParticlesPRap     ;
  AIDA::IHistogram1D* m_hAllParticlesEnergy   ;
  AIDA::IHistogram1D* m_hAllParticlesP        ;
  AIDA::IHistogram1D* m_hAllParticlesPt       ;
  AIDA::IHistogram1D* m_hAllParticlesPID      ;
  int m_nAllParticles{0}; //< Counter for all charged stable particles

  // Pointers to histograms
  std::vector<AIDA::IHistogram1D*> m_pHisto;

  int m_nBHistos;
  // Location where to find HepMC event
  std::string m_inputData ;

  Gaudi::Property<bool> m_neutralHistos{this,"NeutralParticleHistos", false ,"Flag for producing Neutral Histograms"} ; //< Flag for producing Neutral Histograms

  int m_nParticles, m_nPartCount;

  double fraction(const int a, const int b){
    return ( double(a) / double(b) );
  };


  double err_fraction ( const int a, const int b){
    return sqrt(double(a)*(1.-(double(a)/double(b))))/(double(b));
  };

};
#endif // DECAYANALYSIS_H
