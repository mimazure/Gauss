/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef GaussTools_MonitorTiming_H
#define GaussTools_MonitorTiming_H 1
// Geant4
#include "G4ProcessManager.hh"
#include "G4Timer.hh"
#include "G4VProcess.hh"
// GiGa
#include "G4SteppingManager.hh"
#include "GiGa/GiGaStepActionBase.h"
// Gaudi Kernel
#include "GaudiKernel/IHistogramSvc.h"
#include "GaudiKernel/NTuple.h"
// GaussTools
#include "GaussTools/GaussTrackInformation.h"
// STL
#include <fstream>

// forward declarations
template <class TYPE>
class GiGaFactory;

// Class for volumes timing counters
class DetData {
public:
  std::map<int, double> particlesTimes;
  double                totalTime    = 0.;
  double                fractionTime = 0.;
};

/** @class MonitorTiming MonitorTiming.h
 *
 *  @author  James McCarthy
 *  @date    12/2013
 */

class MonitorTiming : virtual public GiGaStepActionBase {
  // friend factory for instantiation
  friend class GiGaFactory<MonitorTiming>;

  /** standard constructor
   *  @see GiGaStepActionBase
   *  @see GiGaBase
   *  @see AlgTool
   *  @param type type of the object (?)
   *  @param name name of the object
   *  @param parent  pointer to parent object
   */

public:
  MonitorTiming( const std::string& type, const std::string& name, const IInterface* parent );
  ~MonitorTiming();

  StatusCode initialize() override;
  StatusCode finalize() override;

  void UserSteppingAction( const G4Step* ) override;

private:
  void FillSubdetTiming( int const particleId, double const stepTime, std::string const detName );
  void WriteSubdetTiming( std::ofstream& out, std::string const detName );
  std::vector<std::pair<double, std::string>> sortMapByValue( std::map<std::string, double> const& data );
  std::vector<std::pair<double, int>>         sortMapByValue( std::map<int, double> const& data );

  G4SteppingManager* steppingManager{nullptr};

  // Timers
  G4Timer *totalTimer{nullptr};
  G4Timer *stepTimer{nullptr};

  // Output file
  Gaudi::Property<std::string> m_TimerFileName{this,"TimerFileName","","Output file"};

  // Summary category name for all known volumes
  std::string const detAll = "All";

  // Total cumulated time in all volumes
  G4double totalCumTime;

  std::string volumeName;

  // Timing data of all known volumes
  std::map<std::string, DetData> detsData;

  // Grouping of detectors for group summaries
  Gaudi::Property<std::map<std::string, std::vector<std::string>>> detsGroups{this,"DetsGroups",{},"Grouping of detectors for group summaries"};

  // Volumes variables
  std::map<std::string, double> allVolsTimes;
  std::map<std::string, double> allSubvolsTimes;
  std::map<std::string, double> allProcTimes;
  Gaudi::Property<std::vector<std::string>>      knownVolumes{this,"Volumes",{},"Volumes"};
  std::vector<std::string>      otherVolumes;
};

#endif // GaussTools_MonitorTiming_H
