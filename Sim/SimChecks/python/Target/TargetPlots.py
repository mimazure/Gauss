###############################################################################
# (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
#####################################################################################
# Option file to run hadronic cross section checks with multiple targets.           #
# For moreusage informations: https://twiki.cern.ch/twiki/bin/view/LHCb/TargetStudy #
#                                                                                   #
# @author : L. Pescatore, K. Zarebski                                               #
# @date   : Last modified 2018-07-25                                                #
#####################################################################################

import ROOT
ROOT.gROOT.SetBatch(True)
from Target.TargetSummary import Plotter, doMultiHistos
from array import array
from copy import *
import os
import logging
logging.basicConfig()

vardef = {"TOTAL": "xsec", "INEL": "inel_xsec", "EL": "el_xsec",
        "MULTI_NCH": "multiNCh", "MULTI_NCH_NOGAMMA": "multiNCh_nogamma",
        "PERC_NCH": "percNCh", "PERC_MINUS": "percMinus", "PERC_PLUS": "percPlus",
        "MULTI_GAMMA": "multi_gamma", "MULTI": "multi"}


plots_title_plotterDict = { 'vardef' : { 'TOTAL'             : 'Total_CrossSection'                ,
				  'INEL'              : 'InElastic_CrossSection'            ,
				  'EL'                : 'Elastic_CrossSection'              ,
				  'MULTI_NCH'         : 'Multiplicity_NeutralCharge'        ,
				  'MULTI_NCH_NOGAMMA' : 'Multiplicity_NeutralCharge_NoGamma',
				  'PERC_NCH'          : 'Percent_NeutralCharged'            ,
				  'PERC_MINUS'        : 'Percent_NegativelyCharged'         ,
				  'PERC_PLUS'         : 'Percent_PositivelyCharged'         ,
				  'MULTI_GAMMA'       : 'Multiplicity_Gamma'                ,
				  'MULTI'             : 'Multiplicity'                      ,
				  'RATIO_TOTAL'       : 'Total_CrossSection_Ratio'          ,
				  'ASYM_TOTAL'        : 'Total_CrossSection_Asymmetry'      ,
				  'RATIO_INEL'        : 'InElastic_CrossSection_Ratio'      ,
				  'ASYM_INEL'         : 'InElastic_CrossSection_Asymmetry'  ,
				  'RATIO_EL'          : 'Elastic_CrossSection_Ratio'        ,
				  'ASYM_EL'           : 'Elastic_CrossSection_Asymmetry'  } }

colors = [1, 2, 4, 6, 8, 9, 38, 12, 18, 41, 5, 3, 20, 21, 22, 23, 24, 25, 27, 28, 29, 30, 31, 32, 33, 34, 35]

mat_names = {'Al' : 'Aluminium', 'Be' : 'Beryllium', 'Si' : 'Silicon'}

part_latex = {'Piminus' : '#pi^{-}', 'Piplus' : '#pi^{+}', 'p' : 'p^{+}', 'pbar' : 'p^{-}',
              'Kplus' : 'K^{+}', 'Kminus' : 'K^{-}'}



class ROOTException(Exception):
     def __init__(self, *args, **kwargs):
         message = '{} Failed.\nINFO: {}'.format(*args)
         Exception.__init__(self, message)

def Plot(dataTree, xvar, finalPlot, outputPath, models=[], pguns=[], materials=[], E0=-1, Dx=-1, plotData=False, makePDFs=False, debug=False):

    ROOT.gROOT.SetBatch(True)

    _logger = logging.getLogger('TARGETPLOTS')

    if debug:
        _logger.setLevel('DEBUG')

    plotterDict = Plotter()

    leg = ROOT.TLegend(0.10, 0.1, 0.9, 0.9)
    leg.SetTextSize(0.05)

    fPlot = finalPlot.replace("RATIO_", "").replace("ASYM_", "")
    var = vardef[fPlot]

    if any(i in finalPlot for i in ['RATIO', 'ASYM']):

        mystr = " in " + str(Dx) + " mm"
        if(xvar == "thickness"):
            mystr = " for " + str(E0) + " GeV"

        titleMultigr = finalPlot
        if(finalPlot == "RATIO_TOTAL"):
            titleMultigr = "Ratio of Total (inel + el) Cross Sections" + mystr
        if(finalPlot == "ASYM_TOTAL"):
            titleMultigr = "Asymmetry of Total (inel + el) Cross Sections" + mystr
        if(finalPlot == "RATIO_INEL"):
            titleMultigr = "Ratio of Inelastic Cross Sections" + mystr
        if(finalPlot == "ASYM_INEL"):
            titleMultigr = "Asymmetry of Inelastic  Cross Sections" + mystr

        pdgenergies = [1, 5, 10, 50, 100]
        pdgRatios_p = [1.67, 1.42, 1.31, 1.14, 1.10]
        pdgRatios_pi = [1.22, 1.13, 1.10, 1.05, 1.03]
        pdgRatios_K = [1.61, 1.32, 1.23, 1.10, 1.07]
        os.system("mkdir -p {}/data_tables".format(outputPath))
        if(xvar == "energy"):
            ratiotxt = open(os.path.join(outputPath, "data_tables/%s_in%imm.txt") % (finalPlot, Dx), "w")
        else:
            ratiotxt = open(os.path.join(outputPath, "data_tables/%s_for%iGeV.txt") % (finalPlot, E0), "w")
        ratiotxt.write("\\begin{tabular}\n")

        grs = []

        for pg in range(0, len(pguns) - 1, 2):

            ratiotxt.write("\\multicolumn{2}{c}{ratio " + str(plotterDict._all_pguns[pguns[pg + 1]].GetLatex("$")) + "/" + str(plotterDict._all_pguns[pguns[pg]].GetLatex("$")) + "} \\\\ \\hline \n")
            nm = 0
            for m in models:
                _logger.debug("Creating {}: {} vs {} Graphs for:\n\tPgun: {}\n\tModel: {}\n\tMaterial: {}\n\tEnergy={}GeV\tThickness={}mm".format(finalPlot, xvar, var, pguns[pg], m, materials, E0, Dx))

                ratiotxt.write("\\multicolumn{2}{c}{ " + m + "} \\\\ \\hline \n")

                varexp = "h_" + str(pg) + m
                select_template = "model == {mod} && material == {mat} && pGun == {part}"
                select = select_template.format(mod=ord(m[0]), mat=ord(materials[0][0]), part=plotterDict._all_pguns[pguns[pg]]._pdgID)

                if(xvar == "energy"):
                    select += " && thickness == " + str(Dx)
                elif(xvar == "thickness"):
                    select += " && energy == " + str(E0)

                dataTree.SetEntryList(0)
                dataTree.Draw(">>" + varexp, select, "entrylist")
                entry_list = ROOT.gDirectory.Get(varexp)
                entries = entry_list.GetN()
                dataTree.SetEntryList(entry_list)

                _logger.debug("Retrieving Entry List of Size "+str(entries))
                
                values = {'xerr' : array('d', [0.] * entries), 'R' : array('d', []), 'Rerr' : array('d', [])}

                has_error = any(i in finalPlot for i in ['MULTI', 'TOTAL', 'INEL', 'EL'])

                try:
                    assert entries != 0
                except AssertionError:
                    _logger.error("No entries selected! Selection used: {}\nGot value: entries={}\nAborting...".format(entries, select))
                    raise AssertionError 

                if(has_error):
                    try:
                        values['y1'] = array('d', [dataTree.GetEntry(entry_list.GetEntry(i)) and getattr(dataTree, var) for i in xrange(entries)])
                        values['x'] = array('d', [dataTree.GetEntry(entry_list.GetEntry(i)) and getattr(dataTree, xvar) for i in xrange(entries)])
                        values['y1err'] = array('d', [dataTree.GetEntry(entry_list.GetEntry(i)) and getattr(dataTree, var+'_err') for i in xrange(entries)])
                        
                        gr = ROOT.TGraphErrors(entries, values['x'],
                                                        values['y1'],
                                                        values['xerr'],
                                                        values['y1err'])
                    except:
                        _logger.error("Could not generate Graph: '{}+/-{} vs {}+/-{}'".format(xvar, '0', var, var+'_err' ))
                        raise ROOTException('ROOT.TGraphErrors', 'x={}\nxerr={}\ny1={}\ny1err={}'.format(values['x'], values['xerr'], values['y1'], values['y1err']))
                else:
                    try:
                        values['y1'] = array('d', [dataTree.GetEntry(entry_list.GetEntry(i)) and getattr(dataTree, var) for i in xrange(entries)])
                        values['x'] = array('d', [dataTree.GetEntry(entry_list.GetEntry(i)) and getattr(dataTree, xvar) for i in xrange(entries)])
                        gr = ROOT.TGraphErrors(entries, values['x'], values['y1'])
                    except:
                        _logger.error("Could not generate Graph: '{} vs {}'".format(xvar, var))
                        raise ROOTException('ROOT.TGraphErrors', 'x={}\ny1={}'.format(values['x'], values['y1']))


                varexp = "h_" + str(pg + 1) + m
                select = select_template.format(mod=ord(m[0]), mat=ord(materials[0][0]), part=plotterDict._all_pguns[pguns[pg + 1]]._pdgID)

                if(xvar == "energy"):
                    select += " && thickness == " + str(Dx)
                elif(xvar == "thickness"):
                    select += " && energy == " + str(E0)


                dataTree.SetEntryList(0)
                dataTree.Draw(">>" + varexp, select, "entrylist")
                entry_list = ROOT.gDirectory.Get(varexp)
                entries = entry_list.GetN()
                
                _logger.debug("Retrieving Entry List of Size "+str(entries))
                
                dataTree.SetEntryList(entry_list)


                if(has_error):
                    try:
                        values['y2'] = array('d', [dataTree.GetEntry(entry_list.GetEntry(i)) and getattr(dataTree, var) for i in xrange(entries)])
                        values['x'] = array('d', [dataTree.GetEntry(entry_list.GetEntry(i)) and getattr(dataTree, xvar) for i in xrange(entries)])
                        values['y2err'] = array('d', [dataTree.GetEntry(entry_list.GetEntry(i)) and getattr(dataTree, var+'_err') for i in xrange(entries)])
                        gr = ROOT.TGraphErrors(entries, values['x'], values['y2'], values['xerr'], values['y2err'])
                    except:
                        _logger.error("Could not generate Graph: '{}+/-{} vs {}+/-{}'".format(xvar, '0', var, var+'_err' ))
                        raise ROOTException('ROOT.TGraphErrors', 'x={}\nxerr={}\ny2={}\ny2err={}'.format(values['x'], values['xerr'], values['y1'], values['y1err']))
                else:
                    try:
                        values['y2'] = array('d', [dataTree.GetEntry(entry_list.GetEntry(i)) and getattr(dataTree, var) for i in xrange(entries)])
                        values['x'] = array('d', [dataTree.GetEntry(entry_list.GetEntry(i)) and getattr(dataTree, xvar) for i in xrange(entries)])
                        gr = ROOT.TGraphErrors(entries, values['x'], values['y2'])
                    except:
                        _logger.error("Could not generate Graph: 'x={} vs y2={}'".format(xvar, var))
                        raise ROOTException('ROOT.TGraphErrors', '{}\n{}'.format(values['x'], values['y1']))


                try:
                    assert len(values['x']) == len(values['y1']) and len(values['x']) == len(values['y2']) and len(values['x']) == entries
                except AssertionError:
                    _logger.error("Array sizes for x, y1, y2 are incompatible")
                    raise AssertionError
                 

                for ee in range(0, entries):

                    ratiotxt.write(str(values['x'][ee]))
                    if 'RATIO' in finalPlot:
                        try:
                            values['R'].append(values['y2'][ee] / values['y1'][ee])
                        except ZeroDivisionError:
                            _logger.error("Attempted to Divide by a Zero\nCalculation y2/y1:"+
                            "\n\ty2={}\n\ty1={}\nSelection: {}".format(values['y2'],values['y1'], select)+
                            "\nThese values are obtained using GetV1(), GetV2() etc.")
                            raise ZeroDivisionError
                    else:
                        values['R'].append(100 * ROOT.TMath.Abs(values['y1'][ee] - values['y2'][ee]) / 2.)

                    if has_error:
                        try:
                            totErr2 = ROOT.TMath.Power(values['y1err'][ee] / values['y1'][ee], 2) + ROOT.TMath.Power(values['y2err'][ee] / values['y2'][ee], 2)
                        except ZeroDivisionError:
                            _logger.error("Attempted to Divide by a Zero\nCalculation y1err/y1+y2err/y2:"+
                            "\n\ty1err={}\n\ty1={}\n\ty2err={}\n\ty2={}.\n\tSelection: {}".format(values['y1err'],
                             values['y1'], values['y2err'], values['y2'], select)+
                            "\nThese values are obtained using GetV1(), GetV2() etc.")
                            raise ZeroDivisionError
       
                        values['Rerr'].append(values['R'][ee] * ROOT.TMath.Sqrt(totErr2))
                        ratiotxt.write(' & $ {0:4.2} \\pm {1:4.2} $ \\\\ \n'.format(values['R'][ee], values['Rerr'][ee]))
                    else:
                        ratiotxt.write(' & $ {0:4.2} $ \\\\ \n'.format(values['R'][ee]))


                if has_error:
                    gr = ROOT.TGraphErrors(entries, values['x'], values['R'], values['xerr'], values['Rerr'])
                else:
                    gr = ROOT.TGraphErrors(entries, values['x'], values['R'])

                gr.SetMarkerColor(colors[pg / 2])
                gr.SetMarkerStyle(20 + nm)

                label = plotterDict._all_pguns[pguns[pg + 1]].GetLatex("LEG") + " / " + plotterDict._all_pguns[pguns[pg]].GetLatex("LEG")
                if(len(models) > 1):
                    label += " (" + m + ")"
                leg.AddEntry(gr, label, "P")

                if plotData and any(i in finalPlot for i in ['TOTAL', 'INEL', 'RATIO']):

                    grPDG = 0
                    if(plotterDict._all_pguns[pguns[pg]].GetName() == "p" and plotterDict._all_pguns[pguns[pg + 1]].GetName() == "pbar"):
                        grPDG = ROOT.TGraphErrors(5, array('d', pdgenergies), array('d', pdgRatios_p))
                    elif(plotterDict._all_pguns[pguns[pg]].GetName() == "Piplus" and plotterDict._all_pguns[pguns[pg + 1]].GetName() == "Piminus"):
                        grPDG = ROOT.TGraphErrors(5, array('d', pdgenergies), array('d', pdgRatios_pi))
                    elif(plotterDict._all_pguns[pguns[pg]].GetName() == "Kplus" and plotterDict._all_pguns[pguns[pg + 1]].GetName() == "Kminus"):
                        grPDG = ROOT.TGraphErrors(5, array('d', pdgenergies), array('d', pdgRatios_K))

                    if grPDG:
                        if(len(pguns) > 2):
                            grPDG.SetMarkerColor(colors[pg / 2])
                        else:
                            grPDG.SetMarkerColor(4)
                        grPDG.SetMarkerStyle(28)
                        grPDG.SetMarkerSize(1.2)
                        if nm == len(models) - 1:
                            grs.append(grPDG)
                            leg.AddEntry(grPDG, plotterDict._all_pguns[pguns[pg + 1]].GetLatex("LEG") + " / " + plotterDict._all_pguns[pguns[pg]].GetLatex("LEG") + " PDG", "P")

                nm += 1
                Material = mat=ord(materials[0][0])
                plot_var = '{}mm'.format(Dx) if xvar == "energy" else '{}GeV'.format(E0)
                plot_var_label = "Thickness" if xvar == "energy" else "Energy"
                gr.SetName("{}_{}-{}_Model-{}-PGun-{}".format(plots_title_plotterDict['vardef'][finalPlot], plot_var_label, plot_var, m, pguns[pg]))
                gr.SetTitle(titleMultigr+' {} {}'.format('of' if xvar == 'energy' else '{} in'.format(pguns[pg]), mat_names[materials[0]]))
                grs.append(gr)

        ratiotxt.write("\\hline\n\\end{tabular}")

        c = ROOT.TCanvas()
        leg_pad = ROOT.TPad("leg_pad", "", 0.73, 0, 1., 1.)
        gr_pad = ROOT.TPad("gr_pad", "", 0.03, 0, 0.8, 1.)
        gr_pad.cd()

        gr_pad.SetLogx()
        gr_pad.SetGrid()

        os.system("mkdir -p {}/ROOTGraphs".format(outputPath))
        output_rootFile = os.path.join(outputPath, "ROOTGraphs/{}".format(finalPlot + mystr.replace(" ", "_") + ".root"))
        out_file = ROOT.TFile.Open(output_rootFile, "recreate")


        ratio_title_y = '#sigma^{{{group}}}_{{antipart}}/#sigma^{{{group}}}_{{part}}'

        for gg in grs:

            gg.GetYaxis().SetTitleOffset(1.5)
            if 'RATIO' in finalPlot:
                if 'INEL' in finalPlot:
                    gg.GetYaxis().SetTitle(ratio_title_y.format(group='inel'))
                elif 'TOTAL' in finalPlot:
                    gg.GetYaxis().SetTitle(ratio_title_y.format(group='total'))
                elif 'EL' in finalPlot:
                    gg.GetYaxis().SetTitle(ratio_title_y.format(group='el'))
                else:
                    pass
                gg.GetYaxis().SetRangeUser(0, 3.)
            else:
                gg.GetYaxis().SetTitle("Asym (%)")

            if(xvar == "energy"):
                gg.GetXaxis().SetTitle("|p| (GeV)")
                gg.GetXaxis().SetRangeUser(0.5, 300)
            elif(xvar == "thickness"):
                gg.GetXaxis().SetTitle("#Delta x (mm)")

            if(gg == grs[0]):
                gg.Draw("APL")
            else:
                gg.Draw("PL SAME")
            gg.Write()

        leg_pad.cd()
        leg.Draw()

        c.cd()
        gr_pad.Draw()
        leg_pad.Draw()
        if makePDFs:
            c.Print(os.path.join(outputPath, finalPlot + mystr.replace(" ", "_") + ".pdf"))
        c.Clear()

    else:

        c = ROOT.TCanvas()
        leg_pad = ROOT.TPad("leg_pad", "", 0.73, 0, 1., 1.)
        gr_pad = ROOT.TPad("gr_pad", "", 0.03, 0, 0.8, 1.)
        gr_pad.cd()

        mystr = " in " + str(Dx) + " mm"
        if(xvar == "thickness"):
            mystr = " for " + str(E0) + " GeV"

        nameMultigr = "mgr" + mystr
        titleMultigr = finalPlot
        if(finalPlot == "TOTAL"):
            titleMultigr = "Total (inel + el) Probability of Interaction" + mystr
        elif(finalPlot == "INEL"):
            titleMultigr = "Inelastic Probability of Interaction" + mystr
        elif(finalPlot == "EL"):
            titleMultigr = "Elastic Probability of Interaction" + mystr
        elif(finalPlot == "PERC_PLUS"):
            titleMultigr = "Percentage of Positive Particles" + mystr
        elif(finalPlot == "PERC_MINUS"):
            titleMultigr = "Percentage of Negative Particles" + mystr
        elif(finalPlot == "MULTI"):
            titleMultigr = "Multiplicity of Secondaries" + mystr
        elif(finalPlot == "MULTI_NCH"):
            titleMultigr = "Multiplicity (neutral)" + mystr
        elif(finalPlot == "MULTI_NCH_NOGAMMA"):
            titleMultigr = "Multiplicity (neutral minus gamma)" + mystr
        elif(finalPlot == "PERC_NCH"):
            titleMultigr = "Percentage of Neutral Particles" + mystr
        elif(finalPlot == "MULTI_NOGAMMA"):
            titleMultigr = "Multiplicity Excluding Gammas" + mystr

        grs = []

        PintOverSigmaFactor = Dx / (1000. * plotterDict._all_materials[materials[0]].GetSigmaDxOverPintFactor() * 1000.)

        #COMPAS Inelastic Xsec data in Al
        COMPAS_p_x = [1.52, 5., 9., 20., 30., 60.]
        COMPAS_p_sigmaErr = [10., 4., 4., 5., 5., 7.]
        COMPAS_p_sigma = [445., 445., 465., 446., 445., 455.]
        COMPAS_p_y = [x * PintOverSigmaFactor for x in COMPAS_p_sigma]
        COMPAS_p_yErr = [x * PintOverSigmaFactor for x in COMPAS_p_sigmaErr]
        COMPAS_pbar_x = [1.45, 6.65, 13.3, 25., 30., 60.]
        COMPAS_pbar_sigma = [617., 558., 536., 480., 457., 439.]
        COMPAS_pbar_sigmaErr = [17., 10., 10., 9., 11., 13.]
        COMPAS_pbar_y = [x * PintOverSigmaFactor for x in COMPAS_pbar_sigma]
        COMPAS_pbar_yErr = [x * PintOverSigmaFactor for x in COMPAS_pbar_sigmaErr]
        #COMPAS Inelastic Xsec data in Be

        COMPAS_inBe_p_x = [3., 5., 9., 30., 50., 60.]
        COMPAS_inBe_p_sigmaErr = [4., 3., 3., 3., 3., 2.]
        COMPAS_inBe_p_sigma = [236., 207., 210., 210., 210., 216.]
        COMPAS_inBe_p_y = [x * PintOverSigmaFactor for x in COMPAS_inBe_p_sigma]
        COMPAS_inBe_p_yErr = [x * PintOverSigmaFactor for x in COMPAS_inBe_p_sigmaErr]
        COMPAS_inBe_pbar_x = [6.65, 13.3, 20., 30., 40.]
        COMPAS_inBe_pbar_sigma = [296., 275., 240., 235., 226., 190.]
        COMPAS_inBe_pbar_sigmaErr = [6., 4., 10., 6., 7.]
        COMPAS_inBe_pbar_y = [x * PintOverSigmaFactor for x in COMPAS_inBe_pbar_sigma]
        COMPAS_inBe_pbar_yErr = [x * PintOverSigmaFactor for x in COMPAS_inBe_pbar_sigmaErr]
        #COMPAS Total Xsec data in Al

        COMPASTot_p_x = [1.52, 1.8, 19.3, 20.]
        COMPASTot_p_sigmaErr = [22., 27., 10., 10.]
        COMPASTot_p_sigma = [687., 694., 687., 687.]
        COMPASTot_p_y = [x * PintOverSigmaFactor for x in COMPASTot_p_sigma]
        COMPASTot_p_yErr = [x * PintOverSigmaFactor for x in COMPASTot_p_sigmaErr]
        COMPASTot_pbar_x = [1.45, 1.8]
        COMPASTot_pbar_sigma = [1034., 1066.]
        COMPASTot_pbar_sigmaErr = [40., 40.]
        COMPASTot_pbar_y = [x * PintOverSigmaFactor for x in COMPASTot_pbar_sigma]
        COMPASTot_pbar_yErr = [x * PintOverSigmaFactor for x in COMPASTot_pbar_sigmaErr]

        COMPAS_p_gr = ROOT.TGraphErrors(6, array('d', COMPAS_p_x), array('d', COMPAS_p_y), array('d', [0.] * 6), array('d', COMPAS_p_yErr))
        COMPAS_pbar_gr = ROOT.TGraphErrors(6, array('d', COMPAS_pbar_x), array('d', COMPAS_pbar_y), array('d', [0.] * 6), array('d', COMPAS_pbar_yErr))
        COMPAS_p_gr.SetMarkerStyle(29)
        COMPAS_p_gr.SetMarkerSize(1.1)
        COMPAS_pbar_gr.SetMarkerStyle(30)
        COMPAS_pbar_gr.SetMarkerSize(1.2)

        COMPASTot_p_gr = ROOT.TGraphErrors(4, array('d', COMPASTot_p_x), array('d', COMPASTot_p_y), array('d', [0.] * 4), array('d', COMPASTot_p_yErr))
        COMPASTot_pbar_gr = ROOT.TGraphErrors(2, array('d', COMPASTot_pbar_x), array('d', COMPASTot_pbar_y), array('d', [0.] * 2), array('d', COMPASTot_pbar_yErr))
        COMPASTot_p_gr.SetMarkerStyle(29)
        COMPASTot_p_gr.SetMarkerSize(1.1)
        COMPASTot_pbar_gr.SetMarkerStyle(30)
        COMPASTot_pbar_gr.SetMarkerSize(1.2)

        COMPAS_inBe_p_gr = ROOT.TGraphErrors(6, array('d', COMPAS_inBe_p_x), array('d', COMPAS_inBe_p_y), array('d', [0.] * 6), array('d', COMPAS_inBe_p_yErr))
        COMPAS_inBe_pbar_gr = ROOT.TGraphErrors(5, array('d', COMPAS_inBe_pbar_x), array('d', COMPAS_inBe_pbar_y), array('d', [0.] * 5), array('d', COMPAS_inBe_pbar_yErr))
        COMPAS_inBe_p_gr.SetMarkerStyle(29)
        COMPAS_inBe_p_gr.SetMarkerSize(1.1)
        COMPAS_inBe_pbar_gr.SetMarkerStyle(30)
        COMPAS_inBe_pbar_gr.SetMarkerSize(1.2)

        #Plotting Gauss values
        n0 = 0
        nh = 0
        for n0, model in enumerate(models):
            for material in materials:

                n2 = 0

                for pg in pguns:

                    _logger.debug("Creating {}: {} vs {} Graphs for:\n\tModel: {}\n\tMaterial: {}".format(finalPlot, xvar, var, model, material))
                    gr = None
                    varexp = "h_" + str(nh)
                    nh += 1

                    select = "model == " + str(ord(model[0])) + " && material == " + str(ord(material[0])) + " && pGun == " + str(plotterDict._all_pguns[pg].GetPDG())
                    if(xvar == "energy"):
                        select += " && thickness == " + str(Dx)
                    elif(xvar == "thickness"):
                        select += " && energy == " + str(E0)


                    dataTree.SetEntryList(0)
                    dataTree.Draw(">>" + varexp, select, "entrylist")
                    entry_list = ROOT.gDirectory.Get(varexp)
                    entries = entry_list.GetN()
                    dataTree.SetEntryList(entry_list)
                
                    _logger.debug("Retrieving Entry List of Size "+str(entries))

                    dataTree.SetEstimate(entries)

                    values = {'xerr' : array('d', [0.] * entries)}

                    if(finalPlot == "MULTI" or finalPlot == "TOTAL" or finalPlot == "INEL" or finalPlot == "EL"):
                         try:
                             dataTree.Draw(xvar + ":" + var + ":" + var + "_err", "", "colz")
 
                             values['y'] = array('d', [dataTree.GetEntry(entry_list.GetEntry(i)) and getattr(dataTree, var) for i in xrange(entries)])
                             values['x'] = array('d', [dataTree.GetEntry(entry_list.GetEntry(i)) and getattr(dataTree, xvar) for i in xrange(entries)])
                             values['yerr'] = array('d', [dataTree.GetEntry(entry_list.GetEntry(i)) and getattr(dataTree, var+'_err') for i in xrange(entries)])
               
                             gr = ROOT.TGraphErrors(entries, values['x'], values['y'], values['xerr'], values['yerr'])
                             
                         except:
                             _logger.error("Could not generate Graph: '{}+/-{} vs {}+/-{}'".format(xvar, '0', var, var+'_err' ))
                             raise ROOTException('TGraphErrors','')
                    else:
                        try:
                             values['y'] = array('d', [dataTree.GetEntry(entry_list.GetEntry(i)) and getattr(dataTree, var) for i in xrange(entries)])
                             values['x'] = array('d', [dataTree.GetEntry(entry_list.GetEntry(i)) and getattr(dataTree, xvar) for i in xrange(entries)])
                             gr = ROOT.TGraphErrors(entries, values['x'], values['y'])
                        except:
                             _logger.error("Could not generate Graph: '{} vs {}'".format(xvar, var))
                             raise ROOTException('TGraphErrors','')


                    if(nh % 2 == 0):
                        gr.SetMarkerColor(colors[int(nh / 2. - 1)])
                        gr.SetMarkerStyle(int(24 + nh / 2. - 1))
                    else:
                        gr.SetMarkerColor(colors[int(nh / 2.)])
                        gr.SetMarkerStyle(int(20 + nh / 2.))

                    gr.SetMarkerSize(1.1)

                    n2 += 1

                    label = plotterDict._all_pguns[pg].GetLatex("LEG") + " in " + material
                    if(len(models) > 1):
                        label += " (" + model + ")"
                    leg.AddEntry(gr, label, "P")

                    plot_var_label = "Thickness" if xvar == "energy" else "Energy"
                    plot_var = '{}GeV'.format(E0) if xvar == "thickness" else '{}mm'.format(Dx)
                    gr.SetName("{}_{}-{}_Mat-{}_Mod-{}_PGun-{}".format(plots_title_plotterDict['vardef'][finalPlot], plot_var_label, plot_var, material, model, pg))
                    gr.SetTitle(titleMultigr+' {} {}'.format('of' if xvar == 'energy' else '{} in'.format(part_latex[pg]), mat_names[material]))
                    grs.append(gr)

                    if plotData and n0 == len(models) - 1 and materials[0] == "Al":
                        if 'TOTAL' in finalPlot:
                            if plotterDict._all_pguns[pg].GetName() == "p":
                                COMPASTot_p_gr.SetMarkerColor(4)  # colors[int(nh/2.-1)])
                                grs.append(COMPASTot_p_gr)
                                leg.AddEntry(COMPASTot_p_gr, "COMPAS p total in Al", "P")
                            elif plotterDict._all_pguns[pg].GetName() == "pbar":
                                COMPASTot_pbar_gr.SetMarkerColor(4)  # colors[int(nh/2.-1)])
                                grs.append(COMPASTot_pbar_gr)
                                leg.AddEntry(COMPASTot_pbar_gr, "COMPAS #bar{p} total in Al", "P")
                        elif 'INEL' in finalPlot:
                            if plotterDict._all_pguns[pg].GetName() == "p":
                                COMPAS_p_gr.SetMarkerColor(4)  # colors[int(nh/2.-1)])
                                grs.append(COMPAS_p_gr)
                                leg.AddEntry(COMPAS_p_gr, "COMPAS p inel in Al", "P")
                            elif plotterDict._all_pguns[pg].GetName() == "pbar":
                                COMPAS_pbar_gr.SetMarkerColor(4)  # colors[int(nh/2.-1)])
                                grs.append(COMPAS_pbar_gr)
                                leg.AddEntry(COMPAS_pbar_gr, "COMPAS #bar{p} inel in Al", "P")
                    elif plotData and n0 == len(models) - 1 and materials[0] == "Be" and 'INEL' in finalPlot:
                        if plotterDict._all_pguns[pg].GetName() == "p":
                            COMPAS_inBe_p_gr.SetMarkerColor(4)  # colors[int(nh/2.-1)])
                            grs.append(COMPAS_inBe_p_gr)
                            leg.AddEntry(COMPAS_inBe_p_gr, "COMPAS p inel in Be", "P")
                        elif plotterDict._all_pguns[pg].GetName() == "pbar":
                            COMPAS_inBe_pbar_gr.SetMarkerColor(4)  # colors[int(nh/2.-1)])
                            grs.append(COMPAS_inBe_pbar_gr)
                            leg.AddEntry(COMPAS_inBe_pbar_gr, "COMPAS #bar{p} inel in Be", "P")
            
            n0 += 1

        ROOT.gStyle.SetOptStat(0)
        gr_pad.SetGrid()
        if(xvar == "energy"):
            gr_pad.SetLogx()
        gr_pad.cd()
        os.system("mkdir -p {}/ROOTGraphs".format(outputPath))
        output_rootFile = os.path.join(outputPath, "ROOTGraphs/{}".format(finalPlot + mystr.replace(" ", "_") + ".root"))
        out_file = ROOT.TFile.Open(output_rootFile, "recreate")
        ngg = 0
        for gg in grs:

            if(xvar == "energy"):
                gg.GetXaxis().SetTitle("|p| (GeV)")
            elif (xvar == "thickness"):
                gg.GetXaxis().SetTitle("#Delta x (mm)")
                gg.GetYaxis().SetTitleOffset(1.2)

            if(ngg == 0):
                gg.Draw("APL")
                ngg += 1
            else:
                gg.Draw("PL SAME")

            gr_pad.Update()

            if(finalPlot == "TOTAL"):
                gg.GetYaxis().SetTitle("P^{tot}_{int} = N^{inel+el}/N^{gen}")
                if(Dx == 1):
                    gg.GetYaxis().SetRangeUser(0.001, 0.01)
                else:
                    gg.GetYaxis().SetRangeUser(0.001, 0.08)

            elif(finalPlot == "INEL"):
                if(Dx == 1):
                    gg.GetYaxis().SetRangeUser(0.001, 0.01)
                else:
                    gg.GetYaxis().SetRangeUser(0.001, 0.08)
                gg.GetYaxis().SetTitle("P^{inel}_{int} = N^{inel}/N^{gen}")
            elif(finalPlot == "EL"):
                gg.GetYaxis().SetTitle("P^{el}_{int} = N^{el}/N^{gen}")
                if(Dx == 1):
                    gg.GetYaxis().SetRangeUser(0.00001, 0.0025)
                else:
                    gg.GetYaxis().SetRangeUser(0.00001, 0.025)
            elif 'MULTI' in finalPlot:
                gg.GetYaxis().SetTitle("< Multi >")
                gg.GetYaxis().SetRangeUser(0., 40.)
            elif 'PERC' in finalPlot:
                gg.GetYaxis().SetTitle("%")
                gg.GetYaxis().SetRangeUser(0., 100.)

            gg.GetYaxis().SetTitleOffset(1.5)
            gg.Write()

            gr_pad.Update()
        leg_pad.cd()
        leg.Draw()

        printname = os.path.join(outputPath, "{}{}.pdf".format(finalPlot, mystr.replace(" ", "_")))
        c.cd()
        gr_pad.Draw()
        leg_pad.Draw()
        if makePDFs:
            c.Print(printname)
        c.Clear()




if __name__ == "__main__":
    ###### Possible types of plots are :
    ##          INEL:                                   inelastic cross section
    ##          EL:                                     elastic cross section
    ##          TOTAL:                                  total cross section
    ##          MULTI:                                  total multiplicity
    ##          MULTI_PLUS(MINUS / NCH / NCH_NOGAMMA):  multiplicity of positive (negative, neutral, neutral but no gammas) secondary particles produced
    ##          PERC_PLUS(MINUS / NCH / NCH_NOGAMMA):   percentage of positive (negative, neutral, neutral but no gammas) secondary particles produced
    ##
    ##        For each of the plots above you can have them is form of a ratio of particles (the consecutive ones in the "pguns" array, see below)
    ##      or as asymmetries adding RATIO or ASYM to the plot type. e.g. RATIO_TOTAL or RATIO_MULTI_NCH or ASYM_INEL, etc

    plots = ["RATIO_TOTAL", "INEL", "TOTAL", "EL", "MULTI", "MULTI_NCH", "MULTI_NCH_NOGAMMA", "PERC_PLUS", "PERC_MINUS"]

    models = ["FTFP_BERT"]  # any you generated, by default "QGSP_BERT","FTFP_BERT"
    thicks = [1]  # 1,5,10
    materials = ["Al", "Be", "Si"]  # ## Al,Si,Be
    energies = [1, 5, 10, 100]  # any you generated, by default 1,5,10,100
    pguns = ["p", "pbar", "Kplus", "Kminus", "Piplus", "Piminus"]  # "p","pbar","Kplus","Kminus","Piplus","Piminus"
    #pguns = ["pbar","p"]
    path = "/afs/cern.ch/work/p/pluca/TargetOutput"

    file_in = ROOT.TFile.Open(path + "/TargetsPlots.root")
    dataTree = file_in.Get("summaryTree")

    os.system("mkdir -p " + path + "/Kaons")
    os.system("mkdir -p " + path + "/Protons")
    os.system("mkdir -p " + path + "/Pions")

    for p in plots:
        Plot(dataTree, "energy", p, path, models, pguns, materials, 2, 1, True)
        Plot(dataTree, "energy", p, path + "/Protons", models, ["p", "pbar"], materials, 2, 1, True)
        Plot(dataTree, "energy", p, path + "/Kaons", models, ["Kplus", "Kminus"], materials, 2, 1, True)
        Plot(dataTree, "energy", p, path + "/Pions", models, ["Piplus", "Piminus"], materials, 2, 1, True)
