###############################################################################
# (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from Gaudi.Configuration import importOptions
from Gaudi.Configuration import DEBUG
importOptions("$APPCONFIGOPTS/Gauss/DataType-2018.py")
importOptions("$GAUSSOPTS/DBTags-2018.py")

from Configurables import LHCbApp
LHCbApp().EvtMax = 1

from Configurables import Gauss
Gauss().DetectorGeo = {"Detectors": ['Spd', 'Prs', 'Ecal', 'Hcal']}
Gauss().DetectorSim = {"Detectors": ['Spd', 'Prs', 'Ecal', 'Hcal']}
Gauss().DetectorMoni = {"Detectors": ['Spd', 'Prs', 'Ecal', 'Hcal']}
Gauss().BeamPipe = "BeamPipeOff"

from Configurables import GaussGeo
GaussGeo().WorldMaterial = '/dd/Materials/Vacuum'

# Particle Gun On
from Gaudi.Configuration import importOptions
importOptions("$LBPGUNSROOT/options/PGuns.py")
from Configurables import ParticleGun
pgun = ParticleGun("ParticleGun")
from Configurables import FlatPtRapidity
pgun.ParticleGunTool = "FlatPtRapidity"
pgun.addTool(FlatPtRapidity, name="FlatPtRapidity")
from GaudiKernel.SystemOfUnits import GeV
pgun.FlatPtRapidity.PtMin = .5 * GeV
pgun.FlatPtRapidity.PtMax = .5 * GeV
pgun.FlatPtRapidity.RapidityMin = 2.5  # ensure in ECAL
pgun.FlatPtRapidity.RapidityMax = 4.8  # ensure in ECAL
pgun.FlatPtRapidity.PdgCodes = [22, 2112]
from Configurables import FlatNParticles
pgun.NumberOfParticlesTool = "FlatNParticles"
pgun.addTool(FlatNParticles, name="FlatNParticles")
pgun.FlatNParticles.MinNParticles = 10
pgun.FlatNParticles.MaxNParticles = 10

from GaudiKernel.SystemOfUnits import m, mm, degree
# Run2 zmax options
zMax = 12280. * mm
collector_z_size = .001 * mm
collector_z_pos = zMax - collector_z_size / 2.
collector_angle = .207 * degree

collector_shape = {
    "Type": "Cuboid",
    "zPos": collector_z_pos,
    "xSize": 10. * m,
    "ySize": 10. * m,
    "zSize": collector_z_size,
    "xAngle": -collector_angle,
    "OutputLevel": DEBUG,
}

collector_sensitive = {
    "Type": "MCCollectorSensDet",
    "RequireEDep": False,
    "OnlyForward": True,
    "OnlyAtBoundary": True,
    "OutputLevel": DEBUG,
}

collector_hit = {
    'Type': 'GetMCCollectorHitsAlg',
    "OutputLevel": DEBUG,
}

collector_moni = {
    'CheckParent': True,
    'CollectorZ': collector_z_pos,
    'CollectorXAngle': collector_angle,
    "OutputLevel": DEBUG,
}

from Configurables import ParallelGeometry, Gauss, ExternalDetectorEmbedder
Gauss().ParallelGeometry = True

# here each different type of collector will placed
# in a separate parallel world

collector_types = ["", "Double", "Triple", "Quadrupole"]

for index, (collector_type, calo) in enumerate(
        zip(collector_types,
            Gauss().DetectorSim["Detectors"])):
    collector_moni["CaloHits" + str(index + 1)] = "MC/" + calo + "/Hits"
    collector_moni["CaloLocation" + str(
        index + 1)] = "/dd/Structure/LHCb/DownstreamRegion/" + calo
    external_name = collector_type + "CollectorEmbedder"
    external = ExternalDetectorEmbedder(external_name)
    external.Shapes[collector_type + 'CaloCollector'] = dict(collector_shape)
    external.Sensitive[collector_type +
                       'CaloCollector'] = dict(collector_sensitive)
    external.Hit[collector_type + 'CaloCollector'] = dict(collector_hit)
    external.Moni[collector_type + 'CaloCollector'] = {
        'Type': collector_type + 'CaloCollector',
        **dict(collector_moni)
    }
    pg = ParallelGeometry()
    pg_name = collector_type + "CollectorParallelWorld"
    pg.ParallelWorlds[pg_name] = {
        'ExternalDetectorEmbedder': external_name,
        "OutputLevel": DEBUG,
    }
    pg.ParallelPhysics[pg_name] = {
        'LayeredMass': True,
        "OutputLevel": DEBUG,
    }

from Configurables import NTupleSvc
NTupleSvc().Output = [
    "FILE1 DATAFILE='CaloCollector.root' TYP='ROOT' OPT='NEW'"
]
