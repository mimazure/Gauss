/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// $Id: GetCaloHitsAlg.h,v 1.2 2009-03-26 21:38:44 robbep Exp $
#ifndef GAUSSCALO_GETCALOHITSALG_H
#define GAUSSCALO_GETCALOHITSALG_H 1

// Include files
// from Gaudi
#include "GaudiAlg/GaudiAlgorithm.h"

// forward declarations
class IGiGaSvc ;
class IGiGaKineCnvSvc ;

/** @class GetCaloHitsAlg GetCaloHitsAlg.h
 *  Conversion from G4 hits to MC CaloHits
 *
 *  @author Patrick Robbe
 *  @date   2005-11-14
 */
class GetCaloHitsAlg : public GaudiAlgorithm {
public:
  /// Standard constructor
  using GaudiAlgorithm::GaudiAlgorithm;

  StatusCode initialize() override;    ///< Algorithm initialization
  StatusCode execute   () override;    ///< Algorithm execution
  StatusCode finalize  () override;    ///< Algorithm finalization

protected:

private:
/// Name of the GiGa service
  Gaudi::Property<std::string> m_gigaSvcName{this,"GiGaService","GiGa","Name of the GiGa service"} ;
  /// Name of the GiGaCnv service
  Gaudi::Property<std::string> m_kineSvcName{this,"KineCnvService",IGiGaCnvSvcLocation::Kine,"Name of the GiGaCnv service"} ;
  /// Name of the output location of MCHits
  Gaudi::Property<std::string> m_hitsLocation{this,"MCHitsLocation","","Name of the output location of MCHits"} ;
  /// Name of the G4 hits collection
  Gaudi::Property<std::string> m_colName{this,"CollectionName","","Name of the G4 hits collection"} ;
  /// Name of the MCParticles location
  Gaudi::Property<std::string> m_mcParticles{this,"MCParticles",LHCb::MCParticleLocation::Default,"Name of the MCParticles location"} ;

  /// Pointer to the GiGa service
  IGiGaSvc * m_gigaSvc{nullptr} ;
  /// Pointer to the GiGaKine service
  IGiGaKineCnvSvc * m_gigaKineCnvSvc{nullptr} ;
};
#endif // GAUSSCALO_GETCALOHITSALG_H
