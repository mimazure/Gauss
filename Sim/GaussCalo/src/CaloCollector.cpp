/*****************************************************************************\
* (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

// Gaudi
#include "GaudiAlg/Consumer.h"
// LHCb
#include "CaloDet/DeCalorimeter.h"
#include "DetDesc/GenericConditionAccessorHolder.h"
#include "Event/MCCaloHit.h"
// Gauss
#include "GaussTools/ZMaxPlane.h"
#include "MCCollector/MCCollectorTuple.h"

namespace MCCollector {
  using BaseClass_t = LHCb::DetDesc::usesBaseAndConditions<MCCollector::TupleAlg, DeCalorimeter>;

  template <class... CalorimeterHitsAndLocs>
  using Consumer =
      Gaudi::Functional::Consumer<void( const LHCb::MCHeader&, const LHCb::MCHits&, const CalorimeterHitsAndLocs&... ),
                                  BaseClass_t>;

  template <class... CalorimeterHitsAndLocs>
  class CaloCollector : public Consumer<CalorimeterHitsAndLocs...> {

  public:
    using KeyValue                            = class Consumer<CalorimeterHitsAndLocs...>::KeyValue;
    using Tuple                               = class Consumer<CalorimeterHitsAndLocs...>::Tuple;
    constexpr static std::size_t otherCalos_n = sizeof...( CalorimeterHitsAndLocs );

    CaloCollector( const std::string& name, ISvcLocator* pSvcLocator )
        : Consumer<CalorimeterHitsAndLocs...>(
              name, pSvcLocator,
              std::tuple_cat(
                  std::make_tuple( KeyValue{"MCHeader", LHCb::MCHeaderLocation::Default},
                                   KeyValue{"CollectorHits", ""} ),
                  std::invoke( []() {
                    std::array<KeyValue, otherCalos_n> calo_props{};
                    for ( size_t i = 0; i < otherCalos_n / 2; i++ ) {
                      calo_props[i * 2] = KeyValue{"CaloHits" + std::to_string( i + 1 ), LHCb::MCCaloHitLocation::Ecal};
                      calo_props[i * 2 + 1] =
                          KeyValue{"CaloLocation" + std::to_string( i + 1 ), DeCalorimeterLocation::Ecal};
                    }
                    return std::tuple_cat( calo_props );
                  } ) ) ){};

    void operator()( const LHCb::MCHeader&, const LHCb::MCHits&, const CalorimeterHitsAndLocs&... ) const override;

    StatusCode initialize() override;

  private:
    template <class CaloHits, class DeCalo, class... OtherCalorimeterHitsAndLocs>
    void fillCaloTuple( int caloID, const LHCb::MCHeader&, const CaloHits&, const DeCalo&,
                        const OtherCalorimeterHitsAndLocs&... ) const;

    template <class CaloHits, class DeCalo>
    void fillCaloTuple( int caloID, const LHCb::MCHeader&, const CaloHits&, const DeCalo& ) const;

    ZMaxPlane m_collectorPlane;

    // set this on whenever there is a possibility that a particle stored in MCCaloHits is a stored daughter of a
    // particle registered in the collector;
    Gaudi::Property<bool>   m_checkParent{this, "CheckParent", false};
    Gaudi::Property<double> m_collZ{this, "CollectorZ", 0.};
    Gaudi::Property<double> m_collAngle{this, "CollectorXAngle", 0.};
    Gaudi::Property<double> m_collYShift{this, "CollectorYShift", 0.};

    mutable Gaudi::Accumulators::Counter<> m_caloHitsCounter{this, "CaloHitsCounter"};
  };

} // namespace MCCollector

template <typename... CalorimeterHitsAndLocs>
StatusCode MCCollector::CaloCollector<CalorimeterHitsAndLocs...>::initialize() {
  return Consumer<CalorimeterHitsAndLocs...>::initialize().andThen( [&] {
    if ( m_checkParent.value() ) {
      m_collectorPlane.prepare( m_collZ.value(), m_collAngle.value(), m_collYShift.value() );
    }
  } );
}

template <class... CalorimeterHitsAndLocs>
void MCCollector::CaloCollector<CalorimeterHitsAndLocs...>::
     operator()( const LHCb::MCHeader& evt, const LHCb::MCHits& collHits, const CalorimeterHitsAndLocs&... calos ) const {
  this->fillCollectorTuple( evt, collHits );
  fillCaloTuple( 1, evt, calos... );
}

template <class... CalorimeterHitsAndLocs>
template <class CaloHits, class DeCalo, class... OtherCalorimeterHitsAndLocs>
void MCCollector::CaloCollector<CalorimeterHitsAndLocs...>::fillCaloTuple(
    int caloID, const LHCb::MCHeader& evt, const CaloHits& caloHits, const DeCalo& calo,
    const OtherCalorimeterHitsAndLocs&... otherCalos ) const {
  fillCaloTuple( caloID, evt, caloHits, calo );
  fillCaloTuple( caloID + 1, evt, otherCalos... );
}

template <class... CalorimeterHitsAndLocs>
template <class CaloHits, class DeCalo>
void MCCollector::CaloCollector<CalorimeterHitsAndLocs...>::fillCaloTuple( int caloID, const LHCb::MCHeader& evt,
                                                                           const CaloHits& caloHits,
                                                                           const DeCalo&   calo ) const {

  Tuple caloT = this->nTuple( "Hits_" + std::to_string( caloID ) );

  for ( const auto& caloHit : caloHits ) {
    caloT->column( "Event_ID", evt.evtNumber() ).ignore();
    auto particle = caloHit->particle();
    if ( m_checkParent.value() ) {
      auto vrxPos = particle->originVertex()->position();
      while ( m_collectorPlane.Distance( vrxPos.y(), vrxPos.z() ) > 0. ) {
        if ( auto mother = particle->mother(); mother ) {
          particle = mother;
          vrxPos   = particle->originVertex()->position();
        } else {
          this->warning() << "No information about particle's mother!" << endmsg;
          break;
        }
      }
    }
    caloT->column( "Particle_Index", particle->index() ).ignore();
    caloT->column( "Active_Energy", caloHit->activeE() ).ignore();
    auto cell_id = caloHit->cellID();
    caloT->column( "Cell_ID", cell_id.index() ).ignore();
    caloT->column( "Cell_X", calo.cellX( cell_id ) ).ignore();
    caloT->column( "Cell_Y", calo.cellY( cell_id ) ).ignore();
    caloT->column( "Cell_Z", calo.cellZ( cell_id ) ).ignore();
    caloT->column( "Cell_Size", calo.cellSize( cell_id ) ).ignore();
    caloT->column( "Time", caloHit->time() ).ignore();

    auto sc = caloT->write();
    if ( sc.isFailure() ) { throw GaudiException( "NTuple not wrtitten!", "CaloCollector", StatusCode::FAILURE ); }

    ++m_caloHitsCounter;
  }
}

using SingleCaloCollector = MCCollector::CaloCollector<LHCb::MCCaloHits, DeCalorimeter>;
DECLARE_COMPONENT_WITH_ID( SingleCaloCollector, "CaloCollector" )

using DoubleCaloCollector =
    MCCollector::CaloCollector<LHCb::MCCaloHits, DeCalorimeter, LHCb::MCCaloHits, DeCalorimeter>;
DECLARE_COMPONENT_WITH_ID( DoubleCaloCollector, "DoubleCaloCollector" )

using TripleCaloCollector = MCCollector::CaloCollector<LHCb::MCCaloHits, DeCalorimeter, LHCb::MCCaloHits, DeCalorimeter,
                                                       LHCb::MCCaloHits, DeCalorimeter>;
DECLARE_COMPONENT_WITH_ID( TripleCaloCollector, "TripleCaloCollector" )

using QuadrupoleCaloCollector =
    MCCollector::CaloCollector<LHCb::MCCaloHits, DeCalorimeter, LHCb::MCCaloHits, DeCalorimeter, LHCb::MCCaloHits,
                               DeCalorimeter, LHCb::MCCaloHits, DeCalorimeter>;
DECLARE_COMPONENT_WITH_ID( QuadrupoleCaloCollector, "QuadrupoleCaloCollector" )
