###############################################################################
# (c) Copyright 2020 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
################################################################################
# Package: GaussFastSim
################################################################################
gaudi_subdir(GaussFastSim v0r1)

gaudi_depends_on_subdirs(Det/CaloDet
                         Det/CaloDetXmlCnv
                         Event/MCEvent
                         Kernel/LHCbKernel
                         Sim/GaussCalo)

find_package(AIDA)
find_package(ROOT)
find_package(Boost)
find_package(CLHEP)

include_directories(SYSTEM ${Boost_INCLUDE_DIRS} ${CLHEP_INCLUDE_DIRS} ${ROOT_INCLUDE_DIRS} ${Geant4_INCLUDE_DIRS})

gaudi_add_module(GaussFastSim
                 src/*.cpp
                 INCLUDE_DIRS AIDA Boost CLHEP  ROOT 
                 LINK_LIBRARIES AIDA ROOT Boost CLHEP CaloDetLib MCEvent LHCbKernel GaussToolsLib GaussFastCaloPointlibLib GaussCaloLib
)
