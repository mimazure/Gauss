/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef IGDML_READER_H
#define IGDML_READER_H 1

#include "GaudiKernel/IAlgTool.h"

/** @class IGDMLReader IGDMLReader.h
 *  Interface class for importing GDML files 
 *
 */

// Declaration of the interface id 
static const InterfaceID IID_IGDMLReader("IGDMLReader", 1, 0);

class G4VPhysicalVolume;

class IGDMLReader : virtual public IAlgTool {

public:
  /// Static access to interface id
  static const InterfaceID& interfaceID() {return IID_IGDMLReader;}

  virtual StatusCode import(G4VPhysicalVolume* world) = 0;

};
#endif 
