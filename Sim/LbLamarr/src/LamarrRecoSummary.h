/*****************************************************************************\
* (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once
// Include files 
// from Gaudi
#include "GaudiAlg/GaudiAlgorithm.h"
#include "Event/Particle.h"
#include "Event/RecSummary.h"
#include "GaudiKernel/RndmGenerators.h"//for MC integration

#include <memory>

// from ROOT
#include <TH1D.h>

/** @class LamarrRecoSummary LamarrRecoSummary.h
 *
 *  @author Lucio Anderlini
 *  @date   2019-02-20  File created
 */
class LamarrRecoSummary : public GaudiAlgorithm 
{
  public: 
    using GaudiAlgorithm::GaudiAlgorithm;

    virtual StatusCode initialize() override;    ///< Algorithm initialization
    virtual StatusCode execute   () override;    ///< Algorithm execution

  private:

////////////////////////////////////////////////////////////////////////////////
// Configuration of the TES location                                          //
////////////////////////////////////////////////////////////////////////////////
    Gaudi::Property<std::string> m_summaryLoc {this,
      "RecSummaryLocation", LHCb::RecSummaryLocation::Default,
      "Output TES location for the simulated RecSummary"}; 

    Gaudi::Property<std::string> m_protoParticlesLocation {this,
      "MCFastProtoParticles", LHCb::ProtoParticleLocation::Charged,
      "Location from which to read the ProtoParticles. "}; 

////////////////////////////////////////////////////////////////////////////////
// Configuration of the input distributions                                   //
////////////////////////////////////////////////////////////////////////////////
    Gaudi::Property<std::string> m_nTracks_histFile {this, 
      "nTracksHistogramFile", "",
      "Histogram defining the distribution for nTracks format: path to TFile"
    };
    Gaudi::Property<std::string> m_nTracks_histName {this, 
      "nTracksHistogramName", "",
      "Histogram defining the distribution for nTracks format: name of TObject"
    };


////////////////////////////////////////////////////////////////////////////////
// Multiplicity multiplier                                                    //
////////////////////////////////////////////////////////////////////////////////
    Gaudi::Property<double> m_mult_mulp {this, 
      "MultiplicityMultiplier", 1.,
      "Multiplier of the event multiplicity"
    };


  private: // methods 
    std::unique_ptr <TH1D> m_hist_nTracks; 
};



