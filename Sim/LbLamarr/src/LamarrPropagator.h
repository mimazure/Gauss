/*****************************************************************************\
* (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

// Include files 
// from Gaudi
#include "GaudiAlg/GaudiAlgorithm.h"
#include "GaudiKernel/AnyDataHandle.h"

#include "TMath.h"
#include "Math/VectorUtil.h"

#include "TLorentzVector.h"
#include "TVector3.h"
#include "TH3D.h"
#include "TH1D.h"
#include <map>
#include <string>
#include <fstream>
#include "Event/State.h"
#include "THnSparse.h"
#include "TH3F.h"

// LHCb
#include "Event/MCHeader.h"
// LoKi Relations
#include "Relations/RelationWeighted1D.h"
// ============================================================================
// LinkerEvent
// ============================================================================
#include "Linker/LinkedTo.h"
#include "Linker/LinkedFrom.h"

//Local 
#include "HistSampler.h"  
#include "LamarrMLFun.h"


// DEBUG ONLY
#include "TNtuple.h"
#include "TFile.h"


/** @class LamarrPropagator LamarrPropagator.h
 *
 *  FastSimulation Lamarr Propagator and Reco
 *
 *  @author Benedetto Gianluca Siddi
 *  @date   2019
 */
class LamarrPropagator : public GaudiAlgorithm {
public: 
  LamarrPropagator( const std::string& name, ISvcLocator* pSvcLocator );  
  StatusCode initialize() override;    ///< Algorithm initialization
  StatusCode execute   () override;    ///< Algorithm execution
  StatusCode finalize  () override;    ///< Algorithm finalization

  /* virtual StatusCode fill( const LHCb::Particle* */
  /*                          , const LHCb::Particle* */
  /*                          , const std::string& */
  /*                          , Tuples::Tuple& ); */
  HepMC::GenVertex* primaryVertex(const HepMC::GenEvent* genEvent) const;
  typedef LHCb::RelationWeighted1D<LHCb::ProtoParticle,LHCb::MCParticle,double> TablePP2MC;

protected:
  
private:
  double computeZMagnetCenter(double qOverP);

  std::unique_ptr<LHCb::State> MCPropagationToState(
      const std::unique_ptr < LHCb::MCParticle >& mcPart, 
      double zProp,
      LHCb::State::Location location);

  bool apply_acceptance(const std::unique_ptr<LHCb::MCParticle>& particle);
  LHCb::Event::Enum::Track::Type apply_efficiency (const std::unique_ptr<LHCb::MCParticle>& particle);
  void apply_resolution (
    std::unique_ptr <LHCb::State>& state,
    std::unique_ptr <LHCb::Track>& track,
    const std::unique_ptr <LHCb::MCParticle>& particle
  );
  void apply_covariance (
    std::unique_ptr <LHCb::State>& state,
    std::unique_ptr <LHCb::Track>& track,
    const std::unique_ptr <LHCb::MCParticle>& particle
  );

  void CovarianceAtZ(std::unique_ptr<LHCb::State> &state);
  void ResolutionAtZ(std::unique_ptr<LHCb::State> &state);
  //void AddTrackInfo(std::unique_ptr<LHCb::Track> &track);
  const HepMC::GenParticle* hasOscillated
  (const HepMC::GenParticle* P) const;
  bool keep(const HepMC::GenParticle* particle) const;
  bool isChargedAndStable ( const LHCb::ParticleID &partId ) const;
  void PropagateAndConvert(
      const std::unique_ptr<LHCb::MCParticle>& particle
    );
  StatusCode processParticle ( 
      HepMC::GenParticle* genPart, 
      std::unique_ptr<LHCb::MCVertex>& originVtx 
    ); 
  
  std::unique_ptr<LHCb::MCVertex> getOrCreateMCVertex 
    (const HepMC::GenVertex *prodVertex);
  
  // const HepMC::GenParticle* hasOscillated
  // (std::unique_ptr<HepMC::GenParticle> &P) const;
  

  Long64_t m_key_gen = 0; //sequential key to give to candidates

  LHCb::HepMCEvents* generationEvents{nullptr};
  LHCb::MCParticles *m_genParticleContainer{nullptr};
  LHCb::MCVertices  *m_genVerticesContainer{nullptr};
  LHCb::MCParticles *m_particleContainer{nullptr};
  LHCb::MCVertices  *m_verticesContainer{nullptr};
  LHCb::Tracks  *m_trackContainer{nullptr};
  LHCb::ProtoParticles* m_chargedProtoContainer{nullptr};
  LHCb::ProtoParticles* m_neutralProtoContainer{nullptr};
  LHCb::RichPIDs* m_richPIDContainer{nullptr};
  LHCb::MuonPIDs* m_muonPIDContainer{nullptr};
  LHCb::MCHeader* mcHeader{nullptr};
  
  Gaudi::Property<std::string> m_generationLocation{this,
      "HepMCEventLocation",
      LHCb::HepMCEventLocation::Default,
      "Location to read the HepMC event."};///< Location in TES of input HepMC events.
  Gaudi::Property<std::string> m_particles{this,
      "MCFastParticleLocation",
      "/Event/MCFast/MCParticles",
      "Location to write smeared MCParticles."}; ///< Location in TES of output smeared MCParticles.
  Gaudi::Property<std::string> m_vertices{this,
      "MCFastVerticesLocation",
      "/Event/MCFast/MCVertices",
      "Location to write smeared MCVertices."}; ///< Location in TES of output smeared MCVertices.
  Gaudi::Property<std::string> m_generatedParticles{this,
      "MCParticleLocation",
      LHCb::MCParticleLocation::Default,
      "Location to write generated MCParticles."}; //< Location in TES of output generated MCParticles.
  Gaudi::Property<std::string> m_generatedVertices{this,
      "MCVerticesLocation",
      LHCb::MCVertexLocation::Default,
      "Location to write generated MCVertices."};  //< Location in TES of output generated MCVertices.
  Gaudi::Property<std::string> m_magnetPolarity{this,
      "MagnetPolarity",
      "up",
      "Magnet Polarity"};
  Gaudi::Property<double> m_ptKick{this,
      "PtKick",
      1200,
      "Pt Kick [MeV/c]"};
  Gaudi::Property<std::string> m_formulaCenterMagnet{this,
      "FormulaZCenterMagnet",
      "",
      "Formula for the z center of magnet parameterization"};
  Gaudi::Property<double> m_valueCenterMagnet{this,
      "ValueZCenterMagnet",
      5200,
      "Value of the z center of magnet parameterization"};  
  Gaudi::Property<std::string> m_tracks{this,
      "MCFastTrack",
      LHCb::TrackLocation::Default,
      "Location to write Tracks."};
  
  Gaudi::Property<std::string> m_richPIDs{this,
      "MCFastRichPID",
      LHCb::RichPIDLocation::Default,
      "Location to write RichPIDs."};
  
  Gaudi::Property<std::string> m_muonPIDs{this,
      "MCFastMuonPID",
      LHCb::MuonPIDLocation::Default,
      "Location to write MuonPIDs."};
  

  Gaudi::Property<std::string> m_ResolutionHisto{this,
      "ResolutionHisto",
      "$LBLAMARRROOT/DataPackages/ResSparse_2012.root",
      "Histogram for Resolution Smearing"};

  Gaudi::Property<std::string> m_EfficiencyHisto{this,
      "EfficiencyHisto",
      "$LBLAMARRROOT/DataPackages/ResSparse_2012.root",
      "Histogram for Efficiency Smearing"};
  Gaudi::Property<std::string> m_chargedProtoParticles{ this, 
      "ChargedProtoParticles",
      LHCb::ProtoParticleLocation::Charged, 
      "Location to write charged ProtoParticles." };//< Location in TES of output ProtoParticles
  Gaudi::Property<std::string> m_neutralProtoParticles{ this, 
      "NeutralProtoParticles",
      LHCb::ProtoParticleLocation::Neutrals, 
      "Location to write charged ProtoParticles." };//< Location in TES of output ProtoParticles
  Gaudi::Property<std::string> m_mcHeader{ this, 
      "MCHeader",
      LHCb::MCHeaderLocation::Default, 
      "MCHeader" };
  
  Gaudi::Property < std::string > m_cppRelationsLocation {this,
    "RelationsChargedProtoLocation", "/Event/Relations/" + LHCb::ProtoParticleLocation::Charged,
    "Output TES location for the MC Relations of the Charged ProtoParticles"} ; 

  Gaudi::Property<bool> m_useEfficiency{ this, 
      "UseEfficiency",
      true, 
      "Reject a fraction of particles modeling acceptance and inefficiencies" };

  Gaudi::Property<bool> m_useResolution{ this, 
      "UseResolution",
      true, 
      "Smear the reconstructed quantities of the track states" };
  
  Gaudi::Property<bool> m_useCovariance{ this, 
      "UseCovariance",
      true, 
      "Compute the covariance matrix for the track states" };
  
  Gaudi::Property<std::vector<std::string>> m_chargedStates{this,
      "ChargedParticlesStates",
      {"EndVelo", "ClosestToBeam"},
      "States of the tracks"};
  Gaudi::Property<std::vector<std::string>> m_neutralStates{this,
      "NeutralParticlesStates",
      {"EndRich2"},
      "States of the tracks"};

  Gaudi::Property<float> m_trackCovarianceScale {this,
    "TrackCovarianceScale", 1.0, "Rescale of the track covariance"};
  
  ////////////////////////////////////////////////////////////////////////////////
  Gaudi::Property<std::string> m_track_likelihood_file{ this,"TrackLikelihoodFileName",
    "","File name to load the Track Likelihood vs. qOverP histogram"};
  Gaudi::Property<std::string> m_track_likelihood_hist{ this,"TrackLikelihoodHistPath",
    "likelihood","TObject name to load the Track Likelihood vs. qOverP histogram"};

  Gaudi::Property<std::string> m_track_ghostprob_file{ this,"TrackGhostProbFileName",
    "","File name to load the Track GhostProb vs. qOverP histogram"};
  Gaudi::Property<std::string> m_track_ghostprob_hist{ this,"TrackGhostProbHistPath",
    "ghostProb","TObject name to load the Track GhostProb vs. qOverP histogram"};

  Gaudi::Property<std::string> m_track_chi2ndof_file{ this,"TrackChi2NdofFileName",
    "","File name to load the Track Chi2/dof vs. qOverP histogram"};
  Gaudi::Property<std::string> m_track_chi2ndof_hist{ this,"TrackChi2NdofHistPath",
    "chi2PerDoF","TObject name to load the Track Chi2/dof vs. qOverP histogram"};

  Gaudi::Property<std::string> m_track_ndof_file{ this,"TrackNdofFileName",
    "","File name to load the histogram for the number of degrees of freedom of the Track vs. qOverP"};
  Gaudi::Property<std::string> m_track_ndof_hist{ this,"TrackNdofHistPath",
    "nDoF","TObject name to load histogram for the number of degrees of freedom of the Track vs. qOverP"};

  Gaudi::Property<std::string> m_covmat_file{ this,"CovMatFile",
    "","File name to load the histogram for the number of degrees of freedom of the Track vs. qOverP"};
  Gaudi::Property<std::string> m_covmat_hist{ this,"CovMatHistPrefix",
    "cov_","TObject name to load histogram for covariance matrix. Name is completed appending <state>"};

  Gaudi::Property<std::string> m_posres_file { this, "PositionResolutionFile",
    "", "File name to load the histogram for the resolutions of the XYZ coords"};
  Gaudi::Property<std::string> m_posresX_hist{ this,"XResolutionHistPrefix",
    "delta_x_","TObject name to load histogram for X resolution. Name is completed appending <state>"};
  Gaudi::Property<std::string> m_posresY_hist{ this,"YResolutionHistPrefix",
    "delta_y_","TObject name to load histogram for Y resolution. Name is completed appending <state>"};
  Gaudi::Property<std::string> m_posresZ_hist{ this,"ZResolutionHistPrefix",
    "delta_z_","TObject name to load histogram for Z resolution. Name is completed appending <state>"};

  Gaudi::Property<std::string> m_acceptance_model_lib{ this, "AcceptanceModelLib",
    "", "Shared Object File containing the acceptance model"
    };
  Gaudi::Property<std::string> m_trkEfficiency_model_lib{ this, "EfficiencyModelLib",
    "", "Shared Object File containing the tracking model"
    };
  Gaudi::Property<std::string> m_resolution_model_lib{ this, "ResolutionModelLib",
    "", "Shared Object File containing the resolution model"
    };
  Gaudi::Property<std::string> m_covariance_model_lib{ this, "CovarianceModelLib",
    "", "Shared Object File containing the covariance model"
    };

  Gaudi::Property<std::string> m_acceptance_model_entrypoint{ this, "AcceptanceModelEntryPoint",
    "acceptance", "Entry point to execute the acceptance model in its shared object"
    };
  Gaudi::Property<std::string> m_trkEfficiency_model_entrypoint{ this, "EfficiencyModelEntryPoint",
    "trkEfficiency", "Entry point to execute the tracking efficiency model"
    };

  Gaudi::Property<std::string> m_resolution_long_entrypoint{ this, "ResolutionLongEntryPoint",
    "reslong_pipe", "Resolution Long Entry Point"
    };
  Gaudi::Property<std::string> m_resolution_upstream_entrypoint{ this, "ResolutionUpstreamEntryPoint",
    "resupstream_pipe", "Resolution Long Entry Point"
    };
  Gaudi::Property<std::string> m_resolution_downstream_entrypoint{ this, "ResolutionDownstreamEntryPoint",
    "resdownstream_pipe", "Resolution Long Entry Point"
    };
  //Gaudi::Property<std::string> m_resolution_preprocessing_suffix{ this, "ResolutionPreprocessingSuffix",
  //  "_tX", "Suffix to identify pre-processing entry point (resolution)"
  //};
  //Gaudi::Property<std::string> m_resolution_postprocessing_suffix{ this, "ResolutionPostprocessingSuffix",
  //  "_tY_inverse", "Suffix to identify post-processing entry point (resolution)"
  //};


  Gaudi::Property<std::string> m_covariance_long_entrypoint{ this, "CovarianceLongEntryPoint",
    "covlong_pipe", "Covariance Long Entry Point"
    };
  Gaudi::Property<std::string> m_covariance_upstream_entrypoint{ this, "CovarianceUpstreamEntryPoint",
    "covupstream_pipe", "Covariance Long Entry Point"
    };
  Gaudi::Property<std::string> m_covariance_downstream_entrypoint{ this, "CovarianceDownstreamEntryPoint",
    "covdownstream_pipe", "Covariance Long Entry Point"
    };


  Gaudi::Property<int> m_resolution_random_nodes { this, "ResolutionRandomNodes",
    128, "Number of random nodes in the Resoluton GAN"
  };
  Gaudi::Property<int> m_covariance_random_nodes { this, "CovarianceRandomNodes",
    128, "Number of random nodes in the Covariance GAN"
  };


  std::unique_ptr<HistSampler> m_track_likelihood, m_track_ghostprob, m_track_chi2ndof, m_track_ndof; 
  TablePP2MC *pp2mcTable;
  std::map < LHCb::State::Location, std::map < uint , std::unique_ptr<HistSampler> > > m_covsampler; 
  std::map < LHCb::State::Location, std::array<std::unique_ptr<HistSampler>, 3 > > m_posres;
  ////////////////////////////////////////////////////////////////////////////////
  
  Rndm::Numbers m_gaussDist ;//Gaussian random number generator
  Rndm::Numbers m_flatDist ; //< Flat random number generator
  bool existCovarianceLUT = false;
  bool existTrackLUT = false;
  std::vector<std::vector<double>> lutCovarianceMatrix;
  std::vector<std::vector<double>> lutTrackMatrix;
  std::map<std::string,std::ifstream*> map_CovFiles;
  std::map<std::string,std::ifstream*> map_ExtraInfoFiles;

  std::map<LHCb::State::Location, std::vector<std::vector<double>>> covMtx;
  std::map<LHCb::State::Location, std::unique_ptr<TH3F>> m_map_res[6]; 

  std::map<LHCb::State::Location, std::unique_ptr<TH3D>> map_effHisto_num;
  std::map<LHCb::State::Location, std::unique_ptr<TH3D>> map_effHisto_den;

  std::map<LHCb::State::Location, std::unique_ptr<TH1D>> map_effHisto_x;
  std::map<LHCb::State::Location, std::unique_ptr<TH1D>> map_effHisto_y;
  std::map<LHCb::State::Location, std::unique_ptr<TH1D>> map_effHisto_z;


    std::map<std::string,LHCb::State::Location> map_Locations = 
    {
      {"LocationUnknown",LHCb::State::LocationUnknown},
      {"ClosestToBeam",LHCb::State::ClosestToBeam},
      {"EndVelo",LHCb::State::EndVelo},
      {"BegRich1",LHCb::State::BegRich1},
      {"EndRich1",LHCb::State::EndRich1},
      {"EndT",LHCb::State::AtT},
      {"BegRich2",LHCb::State::BegRich2},
      {"EndRich2",LHCb::State::EndRich2},
    };

  std::map<LHCb::State::Location, double> map_ZStates = 
    {
      {LHCb::State::ClosestToBeam,0.},
      {LHCb::State::EndVelo,770.},
      {LHCb::State::BegRich1, 990.},
      {LHCb::State::EndRich1, 2165.},
      {LHCb::State::AtT, 9410.},
      {LHCb::State::BegRich2, 9450.},
      {LHCb::State::EndRich2, 11900.},      
    };

  
  Lamarr::mlfun m_acceptance_mlfun, m_efficiency_mlfun;
  std::map <LHCb::Track::Types, Lamarr::ganfun> m_resol;
  std::map <LHCb::Track::Types, Lamarr::ganfun> m_covar;

  ////////////////////////////////////////////////////////////////////////////////
  // Debug stuff
  // TFile *m_output_file;
  // TNtuple *m_output_tree;
  // TNtuple *m_output_tree_Xprep;
  // TNtuple *m_output_tree_Yprep;
};
