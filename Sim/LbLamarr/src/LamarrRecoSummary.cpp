/*****************************************************************************\
* (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// Local 
#include "LamarrRecoSummary.h"

//MCParticle 
#include "Event/ProtoParticle.h"

//Gaudi
#include "Kernel/IParticlePropertySvc.h"
#include "Kernel/ParticleProperty.h"

// STL
#include <vector>
#include <algorithm>
#include <string>

// ROOT 
#include <TFile.h>

DECLARE_COMPONENT ( LamarrRecoSummary )


//================================================================================
// Initialize 
//================================================================================
StatusCode LamarrRecoSummary::initialize()
{
  if (m_nTracks_histFile != "")
  {
    auto f = std::unique_ptr <TFile> ( 
          TFile::Open  (static_cast<std::string>(m_nTracks_histFile).c_str()) 
        );
    TH1D *ptr; 
    f->GetObject (static_cast<std::string>(m_nTracks_histName).c_str(), ptr);
    m_hist_nTracks = std::unique_ptr<TH1D>(ptr); 
    m_hist_nTracks->SetDirectory ( 0 ); 
    f->Close(); 
  }

  return StatusCode::SUCCESS;
}

//================================================================================
// Execute 
//================================================================================
StatusCode LamarrRecoSummary::execute   ()
{
  auto *summary = new LHCb::RecSummary(); 
  put ( summary, m_summaryLoc ); 

  if (m_hist_nTracks)
    summary->addInfo ( LHCb::RecSummary::nTracks, m_mult_mulp * m_hist_nTracks->GetRandom() ); 
  else 
  {
    LHCb::ProtoParticles *protos = getIfExists<LHCb::ProtoParticles> (
                                                  m_protoParticlesLocation 
                                                ); 
    summary->addInfo ( LHCb::RecSummary::nTracks, protos ? protos->size() : 0 );  
  }

  return StatusCode::SUCCESS;
}

