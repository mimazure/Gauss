/*****************************************************************************\
* (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// Include files 

 // from Gaudi
#include "GaudiKernel/IRndmGenSvc.h"
#include "GaudiKernel/IRndmEngine.h"
#include "GaudiKernel/RndmGenerators.h"
#include "GaudiKernel/IDataManagerSvc.h"

// Event.
#include "Event/MCParticle.h"
#include "Event/Particle.h"
// from LHCb
#include "Kernel/IParticlePropertySvc.h"
#include "Kernel/ParticleProperty.h"

// local
#include "LamarrHist.h"
#include "TLorentzVector.h"

//-----------------------------------------------------------------------------
// Implementation file for class : LamarrHist
//
// 2015-10-20 : Benedetto Gianluca Siddi
//-----------------------------------------------------------------------------

// Declaration of the Algorithm Factory
DECLARE_COMPONENT( LamarrHist )


//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
LamarrHist::LamarrHist( const std::string& name,
                          ISvcLocator* pSvcLocator)
  : GaudiHistoAlg ( name , pSvcLocator )
{
}

//=============================================================================
// Initialization
//=============================================================================
StatusCode LamarrHist::initialize() {
  StatusCode sc = GaudiHistoAlg::initialize(); // must be executed first
  if ( sc.isFailure() ) return sc;  // error printed already by GaudiAlgorithm

  if ( msgLevel(MSG::DEBUG) ) debug() << "==> Initialize" << endmsg;
  
  m_theta_XY_Gen = book2D("theta_XY_Gen",-500E-3,500E-3,100,-450E-3,450E-3,100);
  m_theta_XY_Rec = book2D("theta_XY_Rec",-500E-3,500E-3,100,-450E-3,450E-3,100);
  m_ThetaP_Gen = book2D("ThetaP_Gen","P versus Theta Gen",0,10000,100,0,TMath::Pi(),100);
  m_ThetaP_Rec = book2D("ThetaP_Rec","P versus Theta Rec",0,10000,100,0,TMath::Pi(),100);

  m_XYGen = book2D("XYGen","X vs Y Gen",-5000,5000,1000,-5000,5000,1000);
  m_XYRec = book2D("XYRec","X vs Y Rec",-5000,5000,1000,-5000,5000,1000);
  m_RGen = book1D("RGen","R Gen",0,5000,100);
  m_RRec = book1D("RRec","R Rec",0,5000,100);
  m_EtaPhiGen = book2D("EtaPhiGen", "Eta vs Phi Gen", -5,5,100, -10,10,100);
  m_EtaPhiRec = book2D("EtaPhiRec", "Eta vs Phi Rec", -5,5,100, -10,10,100);
  m_EtaGen = book1D("EtaGen", "Eta Gen", -15, 15, 100);
  m_EtaRec = book1D("EtaRec", "Eta Rec", -10, 10, 100);
  m_ZGen = book1D("ZGen", "Z_Gen", -3000, 13000, 100);
  m_ZRec = book1D("ZRec", "Z_Rec", -3000, 13000, 100);
  m_P = book2D("P","Momentum",0,10000,100,0,10000,100);
  m_Pt = book2D("Pt","Tranverse Momentum",0,10000,100,0,10000,100);
  m_Vertex_X = book2D("Vertex_X","Vertex_X",-10000,10000,100,-10000,10000,100);
  m_Vertex_Y = book2D("Vertex_Y","Vertex_Y",-10000,10000,100,-10000,10000,100);
  m_Vertex_Z = book2D("Vertex_Z","Vertex_Z",-10000,10000,100,-10000,10000,100);
  m_PID = book2D("PID","PID",0, 3000, 3000, 0, 3000, 3000);
  m_PX = book2D("PX","PX",-10000,10000,100,-10000,10000,100);
  m_PY = book2D("PY","PY",-10000,10000,100,-10000,10000,100);
  m_PZ = book2D("PZ","PZ",0,10000,100,0,10000,100);
  m_E = book2D("E","Energy",0,10000,100,0,10000,100);
  m_M = book2D("M","Mass",0,10000,100,0,10000,100);
  m_ThetaGen = book1D("ThetaGen", "Theta Gen", -TMath::Pi(), TMath::Pi(), 100);
  m_ThetaRec = book1D("ThetaRec", "Theta Rec", -TMath::Pi(), TMath::Pi(), 100);
  m_PhiGen = book1D("PhiGen", "Phi Gen", -5, 5, 100);
  m_PhiRec = book1D("PhiRec", "Phi Rec", -5, 5, 100);
  m_PhiXGen = book2D("PhiXGen", "Phi vs X Gen", -5, 5, 100, -5000, 5000, 1000);
  m_PhiXRec = book2D("PhiXRec", "Phi vs X Rec", -5, 5, 100, -5000, 5000, 1000);

  m_XYPhiGen = book3D("XYPhiGen","X Y Phi Gen",-5000., 5000, 100, -5000., 5000., 100, -3.241,3.142,20);
  m_XYPhiRec = book3D("XYPhiRec","X Y Phi Rec",-5000., 5000, 100, -5000., 5000., 100, -3.241,3.142,20);

  m_TxTyPGen = book3D("TxTyPGen","Tx Ty P Gen",-1, 1, 100, -0.3, 0.3, 100, 0, 100, 500);
  m_TxTyPRec = book3D("TxTyPRec","Tx Ty P Rec",-1, 1, 100, -0.3, 0.3, 100, 0, 100, 500);

  return StatusCode::SUCCESS;
}

//=============================================================================
// Main execution
//=============================================================================
StatusCode LamarrHist::execute() {

  if ( msgLevel(MSG::DEBUG) ) debug() << "==> Execute" << endmsg;

  //Get TES container
  LHCb::MCParticles *m_genParticleContainer =
    get<LHCb::MCParticles>(m_generatedParticles);

  LHCb::MCParticles *m_particleContainer = 
    get<LHCb::MCParticles>(m_particles);

  GenPions.clear();
  RecPions.clear();
  Pions.clear();
  
  LHCb::MCParticles::const_iterator i;
  for( i=m_genParticleContainer->begin(); i != m_genParticleContainer->end(); i++ ) 
  {
    GenPions.insert(std::pair<Long64_t,LHCb::MCParticle*>((*i)->key(),(*i)));    
    LHCb::MCParticle *P = (*i);
    const LHCb::MCVertex &Vtx = P->originVertex();
    Double_t x = Vtx.position().X();
    Double_t y = Vtx.position().Y();
    Double_t z = Vtx.position().Z();
    // Double_t x = P->momentum().X();
    // Double_t y = P->momentum().Y();
    // Double_t z = P->momentum().Z();
    Double_t R = TMath::Power(x,2) + TMath::Power(y,2);
    R = TMath::Sqrt(R);
    Double_t eta = P->pseudoRapidity();
    Double_t phi = P->momentum().Phi();
    Double_t theta = P->momentum().Theta();
    Double_t px = P->momentum().Px();
    Double_t py = P->momentum().Py();
    Double_t pz = P->momentum().Pz();
    Double_t theta_x = px/pz;
    Double_t theta_y = py/pz;
    Double_t p = P->momentum().P();
    
    fill(m_ThetaP_Gen,p,theta,1);
    
    fill(m_theta_XY_Gen,theta_x,theta_y,1);
    fill(m_XYGen,x,y,1);
    fill(m_EtaPhiGen,phi,eta,1);
    fill(m_EtaGen,eta,1);
    fill(m_ZGen,z,1);
    fill(m_RGen,R,1);  
    fill(m_ThetaGen,theta,1);
    fill(m_PhiGen,phi,1);
    fill(m_PhiXGen,phi,x,1);
    fill(m_XYPhiGen,x,y,phi,1);
    fill(m_TxTyPGen,theta_x,theta_y,p/1000.,1);
    
    
  }
  // info()<<"GenSize: "<<GenPions.size()<<endmsg;

  for( i=m_particleContainer->begin(); i != m_particleContainer->end(); i++ ) 
  { 
    
    RecPions.insert(std::pair<Long64_t,LHCb::MCParticle*>((*i)->key(),(*i)));
    LHCb::MCParticle *P = (*i);
    const LHCb::MCVertex *Vtx = P->endVertices()[0];
    
    Double_t x = Vtx->position().X();
    Double_t y = Vtx->position().Y();
    Double_t z = Vtx->position().Z();
    // Double_t x = P->momentum().X();
    // Double_t y = P->momentum().Y();
    // Double_t z = P->momentum().Z(); 
    Double_t eta = P->pseudoRapidity();
    Double_t phi = P->momentum().Phi();
    Double_t theta = P->momentum().Theta();
    Double_t R = TMath::Power(x,2) + TMath::Power(y,2);
    Double_t px = P->momentum().Px();
    Double_t py = P->momentum().Py();
    Double_t pz = P->momentum().Pz();
    Double_t theta_x = px/pz;
    Double_t theta_y = py/pz;
    Double_t p = P->momentum().P();
    
    fill(m_ThetaP_Rec,p,theta,1);
    
    fill(m_theta_XY_Rec,theta_x,theta_y,1);
    
    R = TMath::Sqrt(R);
    fill(m_XYRec,x,y,1);
    fill(m_EtaPhiRec,phi,eta,1);
    fill(m_EtaRec,eta,1);
    fill(m_ZRec,z,1);
    fill(m_RRec,R,1);
    fill(m_ThetaRec,theta,1);
    fill(m_PhiRec,phi,1);
    fill(m_PhiXRec,phi,x,1);
    fill(m_XYPhiRec,x,y,phi,1);
    fill(m_TxTyPRec,theta_x,theta_y,p/1000.,1);
    
  }
  // info()<<"RecSize: "<<RecPions.size()<<endmsg;  
  std::map<Long64_t, LHCb::MCParticle*>::iterator it;

  for(it=GenPions.begin(); it!=GenPions.end(); ++it)
  {
    if(RecPions.find(it->first) != RecPions.end()) 
      Pions.insert(std::make_pair(it->first,std::make_pair(it->second,RecPions[it->first])));
  }
  // info()<<"PionsSize: "<<Pions.size()<<endmsg;
  
  std::pair<LHCb::MCParticle*,LHCb::MCParticle*> particles;

  std::map<Long64_t, std::pair<LHCb::MCParticle*,LHCb::MCParticle*>>::iterator itp;

  LHCb::MCParticle *P1;
  LHCb::MCParticle *P2;
  Double_t Px1, Py1, Pz1, Pt1, x1, y1, z1, Pid1, E1, M1;
  Double_t Px2, Py2, Pz2, Pt2, x2, y2, z2, Pid2, E2, M2;
  for(itp=Pions.begin(); itp!=Pions.end(); ++itp)
  {
    particles = itp->second;
    P1 = (LHCb::MCParticle*)particles.first;
    P2 = (LHCb::MCParticle*)particles.second;
    const LHCb::ParticleID &ID1 = P1->particleID();
    const LHCb::ParticleID &ID2 = P2->particleID();
    const LHCb::MCVertex &Vtx1 = P1->originVertex();
    const LHCb::MCVertex *Vtx2 = P2->endVertices()[0];
    
    x1 = Vtx1.position().X();
    y1 = Vtx1.position().Y();
    z1 = Vtx1.position().Z();
    Pid1 = ID1.pid();
    Px1 = P1->momentum().Px();
    Py1 = P1->momentum().Py();
    Pz1 = P1->momentum().Pz();
    Pt1 = P1->momentum().Pt();
    E1 = P1->momentum().E();
    M1 = P1->momentum().M();

    x2 = Vtx2->position().X();
    y2 = Vtx2->position().Y();
    z2 = Vtx2->position().Z();
    Pid2 = ID2.pid();
    Px2 = P2->momentum().Px();
    Py2 = P2->momentum().Py();
    Pz2 = P2->momentum().Pz();
    Pt2 = P2->momentum().Pt();
    E2 = P2->momentum().E();
    M2 = P2->momentum().M();

    //// Uncomment these definitions to add define monitoring vars 
    //// to be added to the nTuple
    // Double_t theta_x1 = Px1/Pz1;
    // Double_t theta_y1 = Py1/Pz1;
    // Double_t p1 = P1->momentum().P();
    // Double_t theta_x2 = Px2/Pz2;
    // Double_t theta_y2 = Py2/Pz2;
    // Double_t p2 = P2->momentum().P();

    fill(m_P,P1->p(),P2->p(),1);
    fill(m_PID,TMath::Abs(Pid1),TMath::Abs(Pid2),1);
    fill(m_PX,Px1,Px2,1);
    fill(m_PY,Py1,Py2,1);
    fill(m_PZ,Pz1,Pz2,1);
    fill(m_E,E1,E2,1);
    fill(m_M,M1,M2,1);
    fill(m_Vertex_X,x1,x2,1);
    fill(m_Vertex_Y,y1,y2,1);
    fill(m_Vertex_Z,z1,z2,1);  
    fill(m_Pt,Pt1,Pt2,1);
    
  }
  return StatusCode::SUCCESS;
}

