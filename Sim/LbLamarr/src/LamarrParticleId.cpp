/*****************************************************************************\
* (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// Local 
#include "LamarrParticleId.h"

//MCParticle 
#include "Event/MCParticle.h"

//Gaudi
#include "Kernel/IParticlePropertySvc.h"
#include "Kernel/ParticleProperty.h"

// STL
#include <vector>
#include <cstdlib>

DECLARE_COMPONENT ( LamarrParticleId )

//================================================================================
// Initialize 
//================================================================================
StatusCode LamarrParticleId::initialize()
{

  if (initRandomGenerators().isFailure())
    return StatusCode::FAILURE;

  LHCb::IParticlePropertySvc*  ppSvc = 
        this -> template svc<LHCb::IParticlePropertySvc> 
          ( "LHCb::ParticlePropertySvc" , true) ;

  // input validation 
  for (auto& [partName, ganDir] : m_isMuonEntryPoint)
    if (!ppSvc->find(partName))
    {
      error() << "Particle " << partName 
              << " defined for isMuon efficiency model not valid." 
              << endmsg;
      return StatusCode::FAILURE; 
    }

  for (auto& [partName, ganDir] : m_pipeEntryPoint)
    if (!ppSvc->find(partName))
    {
      error() << "Particle " << partName 
              << " defined for CompiledModel not valid." 
              << endmsg;
      return StatusCode::FAILURE; 
    }

  ///// CompiledModel //////
  for (auto& [partName, entrypoint] : m_pipeEntryPoint)
    m_compiledModel [ppSvc->find(partName)->pdgID().pid()] = Lamarr::load_mlfun<Lamarr::ganfun> (m_compiledModelPath, entrypoint);

  for (auto& [partName, entrypoint] : m_isMuonEntryPoint)
    m_isMuonEff [ppSvc->find(partName)->pdgID().pid()] = Lamarr::load_mlfun<Lamarr::mlfun> (m_compiledModelPath, entrypoint);

  return StatusCode::SUCCESS;
}

//================================================================================
// Execute 
//================================================================================
StatusCode LamarrParticleId::execute   ()
{
  StatusCode status (StatusCode::SUCCESS); 

  // Evaluate the compiled model
  if (m_compiledModelPath != "") 
    status &= evalCompiledModel ( ); 

  return status;
}

//================================================================================
// evalCompiledModel 
//================================================================================
StatusCode LamarrParticleId::evalCompiledModel ( )
{
  auto inputTes = get<LHCb::MCParticles>(m_input_particles); 
  auto richPid = get<LHCb::RichPIDs> (m_rich_pid); 
  auto muonPid = get<LHCb::MuonPIDs> (m_muon_pid); 
  auto protos  = get<LHCb::ProtoParticles> (m_chargedProtoParticles); 

  // Creates a vector for nTracks 
  auto *summary = getIfExists<LHCb::RecSummary> (m_recsummary_location); 
  size_t nTrk = 0;
  if (summary) nTrk = summary->info ( LHCb::RecSummary::nTracks, 0. );

  std::vector<float> random (m_randomNodes);

  for (auto& _p : *inputTes)
  {
    const auto key = _p -> key(); 
    const float pid = _p->particleID().pid();
    float input[] = {
      static_cast<float>(_p->p()),
      static_cast<float>(_p->pseudoRapidity()),
      static_cast<float>(nTrk),
      static_cast<float>(_p->particleID().threeCharge()/3.0),
      static_cast<float>(0.)  //isMuon
    };





    for (int i = 0; i < m_randomNodes; ++i)
      random [i] = m_gaussDist();

    float isMuonEff = 0.;
    if (m_isMuonEff.find(pid) != m_isMuonEff.end()) 
      m_isMuonEff[pid] (&isMuonEff, input);
    
    const bool isMuon = (m_flatDist() < isMuonEff);
    input[4] = static_cast<float>(isMuon);
    
    float output[15];
    if (m_compiledModel.find(pid) != m_compiledModel.end())
      m_compiledModel[pid] (output, input, random.data());
    else
      for (short i = 0; i < 15; ++i) 
        output[i] = -9999;


    // Decoding
    float RichDLLe = output[0];
    float RichDLLmu = output[1];
    float RichDLLK = output[2];
    float RichDLLp = output[3];

    float MuonMuLL = output[4];
    float MuonBkgLL = output[5];
  
    float PIDe = output[6];
    float PIDK = output[7];
    float PIDp = output[8];
    float ProbNNe = output[9];
    float ProbNNpi = output[10];
    float ProbNNk = output[11];
    float ProbNNp = output[12];

    float PIDmu = output[13];
    float ProbNNmu = output[14];

    // Update objects in TES
    if (richPid->object(key))
    {
      LHCb::RichPID* rpid = richPid->object ( key ); 

      rpid->setBestParticleID (Rich::Pion);
      rpid->setParticleDeltaLL(Rich::Pion, 0.); 

      rpid->setParticleDeltaLL(Rich::Electron,        RichDLLe); 
      rpid->setParticleDeltaLL(Rich::Kaon,            RichDLLK); 
      rpid->setParticleDeltaLL(Rich::Muon,            RichDLLmu); 
      rpid->setParticleDeltaLL(Rich::Proton,          RichDLLp); 
      rpid->setParticleDeltaLL(Rich::BelowThreshold,  -9999.); 
      rpid->setParticleDeltaLL(Rich::Deuteron,        -9999.); 
    }

    if (muonPid->object(key))
    {
      muonPid->object (key)->setIsMuonLoose (isMuon);
      muonPid->object (key)->setIsMuon      (isMuon);

      if (isMuon) 
      {
        muonPid->object (key)->setMuonLLMu ( MuonMuLL );
        muonPid->object (key)->setMuonLLBg ( MuonBkgLL );
      }
    }

    if (protos->object(key))
    {
      auto proto = protos->object(key);

      proto->addInfo ( LHCb::ProtoParticle::additionalInfo::RichDLLe,  RichDLLe ); 
      proto->addInfo ( LHCb::ProtoParticle::additionalInfo::RichDLLmu, RichDLLmu ); 
      proto->addInfo ( LHCb::ProtoParticle::additionalInfo::RichDLLpi, 0 ); 
      proto->addInfo ( LHCb::ProtoParticle::additionalInfo::RichDLLk,  RichDLLK ); 
      proto->addInfo ( LHCb::ProtoParticle::additionalInfo::RichDLLp,  RichDLLp ); 

      if (isMuon)
      {
        proto->addInfo ( LHCb::ProtoParticle::additionalInfo::MuonMuLL,  MuonMuLL ); 
        proto->addInfo ( LHCb::ProtoParticle::additionalInfo::MuonBkgLL, MuonBkgLL ); 
      }

      proto->removeCombinedInfo();
      proto->addInfo ( LHCb::ProtoParticle::additionalInfo::CombDLLe,  PIDe ); 
      proto->addInfo ( LHCb::ProtoParticle::additionalInfo::CombDLLpi, 0 ); 
      proto->addInfo ( LHCb::ProtoParticle::additionalInfo::CombDLLk,  PIDK ); 
      proto->addInfo ( LHCb::ProtoParticle::additionalInfo::CombDLLp,  PIDp ); 

      if (isMuon)
        proto->addInfo ( LHCb::ProtoParticle::additionalInfo::CombDLLmu, PIDmu ); 

      proto->addInfo ( LHCb::ProtoParticle::additionalInfo::ProbNNe,  ProbNNe ); 
      proto->addInfo ( LHCb::ProtoParticle::additionalInfo::ProbNNpi, ProbNNpi ); 
      proto->addInfo ( LHCb::ProtoParticle::additionalInfo::ProbNNk,  ProbNNk ); 
      proto->addInfo ( LHCb::ProtoParticle::additionalInfo::ProbNNp,  ProbNNp ); 

      if (isMuon)
        proto->addInfo ( LHCb::ProtoParticle::additionalInfo::ProbNNmu,  ProbNNmu ); 
    }
  }
  return StatusCode::SUCCESS; 
}

StatusCode LamarrParticleId::initRandomGenerators ()
{
  StatusCode sc = StatusCode::SUCCESS;
  
  IRndmGenSvc * randSvc = svc< IRndmGenSvc >( "RndmGenSvc" , true ) ;
  sc &= m_gaussDist.initialize( randSvc , Rndm::Gauss( 0. , 1. ) ) ;
  if(sc.isFailure())
    error()<<"Cannot initialize flat random number generator"<<endmsg;

  sc &= m_flatDist.initialize( randSvc , Rndm::Flat( 0. , 1. ) ) ;
  if(sc.isFailure())
    error()<<"Cannot initialize flat random number generator"<<endmsg;

  sc &= release(randSvc);
  if (sc.isFailure())
    error()<<"Cannot release RandomService"<<endmsg;
  
  return sc;
}