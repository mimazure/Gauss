/*****************************************************************************\
* (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once
// Include files 
// from Gaudi
#include "GaudiAlg/GaudiAlgorithm.h"
#include "Event/Particle.h"
#include "GaudiKernel/RndmGenerators.h"//for MC integration

#include "Event/RecSummary.h" 

//#include "GaudiTensorFlow/Predictor.h" 
//namespace gtf=GaudiTensorFlow;

#include <memory>

#include <unordered_map>

// Local 
#include "LamarrMLFun.h"
  
/** @class LamarrParticleId LamarrParticleId.h
 *
 *  @author Lucio Anderlini
 *  @date   2018-12-10  File created
 */
class LamarrParticleId : public GaudiAlgorithm 
{
  public: 
    /// Standard constructor
    using GaudiAlgorithm::GaudiAlgorithm;

    virtual StatusCode initialize() override;  ///< Algorithm initialization
    virtual StatusCode execute   () override;  ///< Algorithm execution

    typedef float  t_CMFloat; 
    typedef double t_RichFloat;
    typedef float  t_MuonFloat; 

  private:

////////////////////////////////////////////////////////////////////////////////
// Input configuration                                                        //
////////////////////////////////////////////////////////////////////////////////
    Gaudi::Property<std::string> m_input_particles {this, 
      "InputLocation", "/Event/MC/Particles",
      "Input TES location of particles"};

    Gaudi::Property<std::string> m_chargedProtoParticles{ this, 
      "ChargedProtoParticles",
      LHCb::ProtoParticleLocation::Charged, 
      "Location to write charged ProtoParticles." };//< Location in TES of output ProtoParticles

    Gaudi::Property<std::string> m_recsummary_location {this, 
      "RecSummaryLocation", LHCb::RecSummaryLocation::Default,
      "Input TES location of RecSummary"};

    Gaudi::Property<std::string> m_rich_pid {this, 
      "RichPIDsLocation", LHCb::RichPIDLocation::Default,
      "TES locations of the RichPID objects to update"};

    Gaudi::Property<std::string> m_muon_pid {this, 
      "MuonPIDsLocation", LHCb::MuonPIDLocation::Default,
      "TES locations of the MuonPID objects to update"};



////////////////////////////////////////////////////////////////////////////////
// CompiledModel configuration                                                //
////////////////////////////////////////////////////////////////////////////////
    Gaudi::Property<std::string> m_compiledModelPath {this, 
      "CompiledModel", "",
      "Shared Object defining the compiled model"};

    Gaudi::Property< std::map<std::string, std::string> > m_isMuonEntryPoint{this,
      "IsMuonEntryPoint", std::map<std::string, std::string>(),
      "Name of the entry point for the isMuon efficiency computation as pairs particle:string"}; 

    Gaudi::Property< std::map<std::string, std::string> > m_pipeEntryPoint{this,
      "PipelineEntryPoint", std::map<std::string, std::string>(),
      "Name of the entry point for the PID var computation as pairs particle:string"}; 

    Gaudi::Property <int> m_randomNodes{this,
      "NumberOfRandomNodes", 512,
      "Number of random nodes to pass to the compiled model"};


  private: // methods 
    std::vector <int> getKeysOfParticles(int mcId);
    StatusCode evalCompiledModel ();
    StatusCode initRandomGenerators();

    std::map <int, Lamarr::mlfun> m_isMuonEff;
    std::map <int, Lamarr::ganfun> m_compiledModel;

    Rndm::Numbers m_gaussDist;//Gaussian random number generator
    Rndm::Numbers m_flatDist; //< Flat random number generator

};


