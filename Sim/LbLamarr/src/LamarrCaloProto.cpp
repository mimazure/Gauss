/*****************************************************************************\
* (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// Include files 
// C++
#include <math.h> 
// from Gaudi

#include "GaudiAlg/GaudiAlgorithm.h"

#include "GaudiKernel/IRndmGenSvc.h"
#include "GaudiKernel/IRndmEngine.h"

#include "GaudiKernel/IDataManagerSvc.h"
// Event.
#include "Event/HepMCEvent.h"
#include "Event/MCParticle.h"
#include "Event/Particle.h"
#include "Event/RecVertex.h"

// from LHCb
#include "Kernel/IParticlePropertySvc.h"
#include "Kernel/ParticleProperty.h"
//local
#include "LamarrCaloProto.h"

#include "GaudiKernel/KeyedTraits.h"

//replace with friend class, inheritance
#include "Event/StateParameters.h"
#include "Event/State.h"


//-----------------------------------------------------------------------------
// Implementation file for class : LamarrCaloProto
//
// 2019-06-27 : Adam Davis
//-----------------------------------------------------------------------------

// Declaration of the Algorithm Factory
DECLARE_COMPONENT( LamarrCaloProto )


//=============================================================================
// Initialization
//=============================================================================
StatusCode LamarrCaloProto::initialize() {
  StatusCode sc = GaudiAlgorithm::initialize(); // must be executed first
  if ( sc.isFailure() ) return sc;  // error printed already by GaudiAlgorithm
  
  if ( msgLevel(MSG::DEBUG) ) debug() << "==> Initialize" << endmsg;
  //random number generator
  IRndmGenSvc * randSvc = svc< IRndmGenSvc >( "RndmGenSvc" , true ) ;
  sc = m_flatDist.initialize( randSvc , Rndm::Flat( 0. , 1. ) ) ;
  if(sc.isFailure()){
    error()<<"Cannot initialize flat random number generator"<<endmsg;
    return sc;    
  }
  sc = m_gaussDist.initialize( randSvc , Rndm::Gauss( 0. , 1. ) ) ;
  if(sc.isFailure()){
    error()<<"Cannot initialize flat random number generator"<<endmsg;
    return sc;    
  }
  
  //check configuration of cells.
  if(m_calo_x_borders.value().size()!= m_calo_x_bins.value().size()+1){
    error()<<"Calorimeter geometry configuration error! X borders is not one more than bins size!"<<endmsg;
    return StatusCode::FAILURE;  
  }

  if(m_calo_y_borders.value().size()!= m_calo_y_bins.value().size()+1){
    error()<<"Calorimeter geometry configuration error! Y borders is not one more than bins size!"<<endmsg;
    return StatusCode::FAILURE;  
  }
  // deprecate map for the spill regions. Instead, set everything to be from the calo region we are considering




  //store the information in a more readable fashion, index by the calo area
  std::map<std::string,double> tmp_map;  
  //todo, make these configurable 
  //std::vector<std::string> regions = {"Inner","Middle","Outer"};  
  for(uint i=0; i<m_RegionNames.value().size();++i){ 
    tmp_map["Xlow"] = m_calo_x_borders.value().at(i);
    tmp_map["Ylow"] = m_calo_y_borders.value().at(i);
    tmp_map["Xhigh"] = m_calo_x_borders.value().at(i+1);
    tmp_map["Yhigh"] = m_calo_y_borders.value().at(i+1);
    tmp_map["CellSizeX"] = m_calo_x_bins.value().at(i);
    tmp_map["CellSizeY"] = m_calo_y_bins.value().at(i);

    m_calo_boundary_info[LHCb::Detector::Calo::CellCode::caloArea(LHCb::Detector::Calo::CellCode::Index::EcalCalo, 
                                                m_RegionNames.value().at(i))] = tmp_map;
  }
  if(msgLevel(MSG::DEBUG)){
    debug()<<"now set calo information on boundaries to "<<m_calo_boundary_info<<endmsg;  }
  
  
  //debug()<<"cell size for region 1 = "<<size_reg1x <<" x "<<size_reg1y<<endmsg;
  release(randSvc).ignore();
  return StatusCode::SUCCESS;
}

//=============================================================================
// Main execution
//=============================================================================
StatusCode LamarrCaloProto::execute() {

  if ( msgLevel(MSG::DEBUG) ) debug() << "==> Execute" << endmsg;
  
  
  //Get TES container
  LHCb::ProtoParticles* neutralProtoContainer=getOrCreate<LHCb::ProtoParticles,
                                                          LHCb::ProtoParticles>(m_neutralLocation.value());
  
  LHCb::CaloHypos* caloHypoContainer = getOrCreate<LHCb::CaloHypos,LHCb::CaloHypos>( m_photonsLocation.value() );

  LHCb::CaloDigits* caloDigits = getOrCreate<LHCb::CaloDigits,LHCb::CaloDigits>( m_caloDigitsLocation.value() );
  
  LHCb::MCParticles *m_particleContainer =get<LHCb::MCParticles>(m_particles.value());
  if ( msgLevel(MSG::DEBUG) ) debug()<<"Looking at MC Particles size "<<m_particleContainer->numberOfObjects()<<endmsg;

  LHCb::CaloClusters* caloClusters = getOrCreate<LHCb::CaloClusters,LHCb::CaloClusters>( m_caloClusterLocation.value() );
  if ( msgLevel(MSG::DEBUG) ) debug()<<"Looking at calo clusters size "<<caloClusters->numberOfObjects()<<endmsg;
  

  LamarrSimEcalClusters ecal_tower_pos_and_energy = find_ecal_clusters(m_particleContainer);
  //probably best to think about an indivudal class for this... or at least a struct
  //spill over for ecal
  //find all cells which have the same index, row, col with different seeds.
  if ( msgLevel(MSG::DEBUG) ) debug()<<"now looking for overlaps in "<<ecal_tower_pos_and_energy.size()<<" clusters"<<endmsg;
  //get cluster overlap.
  if(ecal_tower_pos_and_energy.size()>1){CombineOverlappingCluster(ecal_tower_pos_and_energy);}

  //make reconstructed clusters
  //this gets a bit messy. First, make all the CaloCellIDs, associate them to the digits.
  //then put the digits in the TES. Due to the CaloClusterEntry relying on SmartRefs, loop over
  //the digits in the TES and match them to our own list.
  //from there, make the CaloCluster, and then put it in the TES.
  //The Protoparticle is put in first, reliant on the hypo, which is reliant on the digit.
  //remoivng the smart ref reliance on the ClusterEntry would reduce the complexity.
  std::vector<LHCb::CaloDigit*> digit_cont;
  
  for(auto ecalCluster: ecal_tower_pos_and_energy){
    //std::vector<LHCb::CaloDigit* > digit_cont;    
    auto proto = std::make_unique<LHCb::ProtoParticle>();
    //build digits
    
    for(auto ecalCell : ecalCluster.Towers()){      
      LHCb::Detector::Calo::CellID cID(LHCb::Detector::Calo::CellCode::Index::EcalCalo,ecalCell.Region(),ecalCell.Row(),ecalCell.Col());
      //region,row,column
      LHCb::CaloDigit* cDig = new LHCb::CaloDigit(cID,ecalCell.E());
      //check if the digit is already inserted into the TES
      bool manual_check = false;
      for(auto thing : digit_cont){
        if(cID.hash() == thing->cellID().hash()){
          manual_check = true;
          if ( msgLevel(MSG::DEBUG) ) debug()<<"digit is already in TES!"<<endmsg;
          
        }
      }
      
      if(manual_check){//FOUND
        if ( msgLevel(MSG::DEBUG) ) debug()<<"already have digit and cell here"<<endmsg;
        continue;        
      }
      if ( msgLevel(MSG::DEBUG) ){
        debug()<<"inserting calo digit"<<cDig<<endmsg;
        debug()<<"hash of calo cellID is"<<cDig->cellID().hash()<<", row "<<cDig->cellID().row()<<
        ", col"<<cDig->cellID().col()<<", area = "<<cDig->cellID().area()<<endmsg;
      }
      
      caloDigits->insert(cDig);
      //if ( msgLevel(MSG::DEBUG) ) debug()<<"inserted calo digit"<<endmsg;
           
      digit_cont.push_back(cDig);
      //m_cellIDs.push_back(cID);
      //m_digits.push_back(cDig);
    }
        
    //make covariance
      
    double reco_energy = 0;
    double barycenter_x = 0;
    double barycenter_y = 0;
    double cov_xx=0;
    double cov_yy=0;
    double cov_ee=0;
    double cov_ex=0;
    double cov_ey=0;
    double cov_xy=0;
    double mean_x=0;
    double mean_y=0;    

    for(auto clustered_cell :ecalCluster.Towers()){
      //int thisBorderIndex = clustered_cell.Region();
      int thisrow = clustered_cell.Row();
      int thiscol =clustered_cell.Col();
      float thisEfrac =clustered_cell.E();
      reco_energy+=thisEfrac;      
      auto min_x = (thiscol -m_cellCenterX.value())*clustered_cell.GetCellXsize();
      auto max_x = (thiscol+1 -m_cellCenterX.value())*clustered_cell.GetCellXsize();
      auto min_y = (thisrow-m_cellCenterY.value())*clustered_cell.GetCellYsize();
      auto max_y = (thisrow+1 - m_cellCenterY.value())*clustered_cell.GetCellYsize();
      auto xclus = (max_x-min_x)/2.+min_x;
      auto yclus = (max_y-min_y)/2.+min_y;
      mean_x+=xclus;
      mean_y+=yclus;
      
      barycenter_x+=thisEfrac*xclus;
      barycenter_y+=thisEfrac*yclus;
    }
    
    mean_x/=ecal_tower_pos_and_energy.size();
    mean_y/=ecal_tower_pos_and_energy.size();

    barycenter_x/=reco_energy;
    barycenter_y/=reco_energy;
    if(m_SmearCalorimeterPositionAndEnergy){
      int region2use = 0;
      if(LHCb::Detector::Calo::CellCode::CaloArea::Inner==ecalCluster.SeedRegion()) region2use=1;
      else if(LHCb::Detector::Calo::CellCode::CaloArea::Middle==ecalCluster.SeedRegion()) region2use = 2;
      else if(LHCb::Detector::Calo::CellCode::CaloArea::Outer==ecalCluster.SeedRegion()) region2use=3;
      
      auto smeared_pos_and_e = get_Cali_PositionAndEnergy(barycenter_x,barycenter_y,reco_energy,region2use);
      if (msgLevel(MSG::DEBUG) ){debug()<<"Compare reconstructed position and energy with/without smearing"<<endmsg;
        debug()<<"No Smearing: (x,y,E) = ("<<barycenter_x<<","<< barycenter_y<<","<< reco_energy<<")"<<endmsg;
      
        debug()<<"W/ Smearing: (x,y,E) = ("
               <<std::get<0>(smeared_pos_and_e)<<","<< std::get<1>(smeared_pos_and_e)<<","<< std::get<2>(smeared_pos_and_e)
               <<")"<<endmsg;
      }
      
      barycenter_x = std::get<0>(smeared_pos_and_e);
      barycenter_y = std::get<1>(smeared_pos_and_e);
      reco_energy = std::get<2>(smeared_pos_and_e);
      
    }
    
      
    
    //covariance matrix
    if ( msgLevel(MSG::DEBUG) ) debug()<<"Calculating covariance"<<endmsg;
    for(auto cell_i: ecalCluster.Towers()){
      //int thisBorderIndexi = cell_i.Region();
      int thisrowi = cell_i.Row();
      int thiscoli = cell_i.Col();      
      float thisEfraci = cell_i.E();      
      
      auto min_xi = (thiscoli -m_cellCenterX.value())*cell_i.GetCellXsize();
      auto max_xi = (thiscoli+1 -m_cellCenterX.value())*cell_i.GetCellXsize();
      auto min_yi = (thisrowi-m_cellCenterY.value())*cell_i.GetCellYsize();
      auto max_yi = (thisrowi+1 - m_cellCenterY.value())*cell_i.GetCellYsize();
      auto xclusi = (max_xi-min_xi)/2.+min_xi;
      auto yclusi = (max_yi-min_yi)/2.+min_yi;
        
      cov_xx+=(xclusi-mean_x)*(xclusi-mean_x);
      cov_yy+=(yclusi-mean_y)*(yclusi-mean_y);
      cov_ee+=(thisEfraci - reco_energy/ecalCluster.Towers().size())*(thisEfraci - reco_energy/ecalCluster.Towers().size());
      cov_ex+=(thisEfraci - reco_energy/ecalCluster.Towers().size())*(xclusi-mean_x);
      cov_ey+=(thisEfraci - reco_energy/ecalCluster.Towers().size())*(yclusi-mean_y);
      cov_xy+=(xclusi-mean_x)*(yclusi-mean_y);
    }
    
    cov_xx/=ecal_tower_pos_and_energy.size();
    cov_yy/=ecal_tower_pos_and_energy.size();
    cov_ee/=ecal_tower_pos_and_energy.size();
    cov_ex/=ecal_tower_pos_and_energy.size();
    cov_ey/=ecal_tower_pos_and_energy.size();
    cov_xy/=ecal_tower_pos_and_energy.size();
    
    auto cPos = std::make_unique<LHCb::CaloPosition>();
    

    if ( msgLevel(MSG::DEBUG) ) debug()<<"setting barycentre"<<endmsg;
    
    cPos->setParameters(Gaudi::Vector3(barycenter_x,barycenter_y,reco_energy));
    cPos->setZ(m_zEcal);
    
    ///get the neighboring cells
    //bool multiRegion = false;
    
    
    //make covariance matrix based on the neighboring cells
    if ( msgLevel(MSG::DEBUG) ) debug()<<"setting covariance matrix"<<endmsg;
    
    Gaudi::SymMatrix3x3 thisCov;
    thisCov(LHCb::CaloPosition::X,LHCb::CaloPosition::X)=cov_xx;
    thisCov(LHCb::CaloPosition::Y,LHCb::CaloPosition::X)=cov_xy;
    thisCov(LHCb::CaloPosition::E,LHCb::CaloPosition::X)=cov_ex;
    thisCov(LHCb::CaloPosition::Y,LHCb::CaloPosition::Y)=cov_yy;
    thisCov(LHCb::CaloPosition::E,LHCb::CaloPosition::Y)=cov_ey;
    thisCov(LHCb::CaloPosition::E,LHCb::CaloPosition::E)=cov_ee;
    
    cPos->setCovariance(thisCov);
    //m_cpos.push_back(cPos);
    //insert cluster
    LHCb::CaloCluster* cluster = new LHCb::CaloCluster();

    LHCb::CaloDigitStatus::Mask isSeedStatus = LHCb::CaloDigitStatus::Mask::SeedCell;
    LHCb::CaloDigitStatus::Mask owned  = LHCb::CaloDigitStatus::Mask::OwnedCell ;
    for(SmartRef<LHCb::CaloDigit> toFind : digit_cont){
      //      if ( msgLevel(MSG::DEBUG) ) debug()<<"test of smart ref loop"<<endmsg;
      
      if(LHCb::Detector::Calo::CellCode::caloArea(LHCb::Detector::Calo::CellCode::caloNum("Ecal"),toFind->cellID().area())==ecalCluster.SeedRegion()
         && (int)toFind->cellID().row()==ecalCluster.SeedRow() 
         && (int)toFind->cellID().col()==ecalCluster.SeedCol()){

        if ( msgLevel(MSG::DEBUG) ) debug()<<"got seed!"<<endmsg;
        cluster->entries().push_back( LHCb::CaloClusterEntry( toFind , isSeedStatus, 
                                                              ecalCluster.SeedEnergy()/ecalCluster.ClusterEnergy() ) );
        cluster->setSeed( toFind->cellID() );
        
      }
      //else, find out if the digit itself is in the cluster
      else{
        for(auto contrCell : ecalCluster.Towers()){
          if((int)toFind->cellID().row()==contrCell.Row() &&
             (int)toFind->cellID().col()==contrCell.Col() &&
             LHCb::Detector::Calo::CellCode::caloArea(LHCb::Detector::Calo::CellCode::caloNum("Ecal"), toFind->cellID().area())==contrCell.Region()){
            if ( msgLevel(MSG::DEBUG) ) debug()<<"found contributing cell!"<<endmsg;
            cluster->entries().push_back(LHCb::CaloClusterEntry( toFind, owned,
                                                                 contrCell.E()/ecalCluster.ClusterEnergy()
                                                                 ));
          }//endif
        }//end loop on other cells
      }//end else
    }//end loop on smartrefs
    
    //cluster->setSeed(seedID );
    cluster->setPosition(*cPos.get());
    LHCb::CaloHypo* hypo = new LHCb::CaloHypo(); //from digi
    hypo->setPosition(std::make_unique<LHCb::CaloPosition>(*cPos.get()));

    caloClusters->insert(cluster,ecalCluster.MCKey());
    
    
    
    if ( msgLevel(MSG::DEBUG) ) debug()<<"adding digits"<<endmsg;
    for(auto dig : digit_cont){      
      hypo->addToDigits(dig); 
    }    
    hypo->setHypothesis(LHCb::CaloHypo::Hypothesis::Photon);
    hypo->addToClusters(cluster);
    if ( msgLevel(MSG::DEBUG) ) debug()<<"inserting calo hypo"<<hypo<<endmsg;
    caloHypoContainer->insert(hypo,ecalCluster.MCKey());
    //else  hypo->setHypothesis(LHCb::CaloHypo::Hypothesis::NeutralHadron);//just for now.
    
    proto->addToCalo(hypo);
    auto pflag= LHCb::ProtoParticle::IsPhoton;
    //auto hflag = CaloDataType::isPhoton;  
    //const auto data =  m_estimator->data(&hypo, hflag , +1 );
    proto->addInfo(pflag,+1);

    //insert into TES
    if ( msgLevel(MSG::DEBUG) ) debug()<<"trying to print proto"<<endmsg;
    
    if ( msgLevel(MSG::DEBUG) ) debug()<<"proto = "<<*proto<<endmsg;
    //if ( msgLevel(MSG::DEBUG) ) debug()<<"part->key() = "<<part->key()<<endmsg;
    if ( msgLevel(MSG::DEBUG) ) debug()<<"part->key() from cluster = "<< ecalCluster.MCKey()<<endmsg;
    if ( msgLevel(MSG::DEBUG) ) debug()<<"inserting proto"<<endmsg;
    
    neutralProtoContainer->insert(proto.release(),ecalCluster.MCKey());
        
  }//end loop on ecal clusters
  

  
    

  return StatusCode::SUCCESS;
}



//=============================================================================
//  Finalize
//=============================================================================
StatusCode LamarrCaloProto::finalize() {

  if ( msgLevel(MSG::DEBUG) ) debug() << "==> Finalize" << endmsg;
  

  return GaudiAlgorithm::finalize();  // must be called after all other actions
}


std::vector<std::tuple<float,float> > LamarrCaloProto::PointsOfIntersection(float xhit, float yhit,
                                                      float xmin, float xmax,
                                                      float ymin, float ymax, float rM){
  //compute all points of intersection of circle of radius rM with center (xhit,yhit)
  //and box (xmin,ymin), (xmax,ymax)
  std::vector<std::tuple<float,float>> ret;  
  //first, x==xmin points
  if(rM >=fabs(xhit-xmin)){//make sure sqrt is defined
    float thing = sqrt(rM*rM - (xhit-xmin)*(xhit-xmin));    
    float yI1= yhit + thing;
    float yI2 = yhit- thing;
    if (yI1<=ymax && yI1>=ymin){ret.push_back(std::make_tuple(xmin,yI1));}
    if (yI2<=ymax && yI2>=ymin){ret.push_back(std::make_tuple(xmin,yI2));}    
  }
  //x==xmax
  if(rM>=fabs(xmax-xhit)){
    float thing = sqrt(rM*rM - (xmax-xhit)*(xmax-xhit));    
    float yI1= yhit + thing;
    float yI2 = yhit- thing;
    if (yI1<=ymax && yI1>=ymin){ret.push_back(std::make_tuple(xmax,yI1));}
    if (yI2<=ymax && yI2>=ymin){ret.push_back(std::make_tuple(xmax,yI2));}    
  }
  //y==ymin
  if(rM >=fabs(yhit-ymin)){
    float thing = sqrt(rM*rM - (yhit-ymin)*(yhit-ymin));    
    float xI1= xhit + thing;
    float xI2 = xhit- thing;
    if (xI1<=xmax && xI1>=xmin){ret.push_back(std::make_tuple(xI1,ymin));}
    if (xI2<=xmax && xI2>=xmin){ret.push_back(std::make_tuple(xI2,ymin));}    
  }
  //y==ymax
  if(rM>=fabs(ymax-yhit)){
    float thing = sqrt(rM*rM - (ymax-yhit)*(ymax-yhit));    
    float xI1= xhit + thing;
    float xI2 = xhit- thing;
    if (xI1<=xmax && xI1>=xmin){ret.push_back(std::make_tuple(xI1,ymax));}
    if (xI2<=xmax && xI2>=xmin){ret.push_back(std::make_tuple(xI2,ymax));}    
  }
  return ret;
  
}

float LamarrCaloProto::EnergyFraction(float x1, float x2, float y1, float y2, float num_RM, int num_integration_points){
  //numerical integration of region bounded by cell (x1,y1),(x2,y2) and n moliere radii
  //do they intersect?
  //circle is x^2 + y^2 = R^2
  //find points where circle crosses.
  //todo, update to ellipse. for angles
  std::vector<std::tuple< float, float> > points2check  = PointsOfIntersection(0,0,x1,x2,y1,y2,num_RM*m_moliere_radius.value());
  if(points2check.size()==0 
     && x1*x1+y1*y1>=num_RM*m_moliere_radius.value()*num_RM*m_moliere_radius.value() 
     && x2*x2+y2*y2>=num_RM*m_moliere_radius.value()*num_RM*m_moliere_radius.value()){//no points of intersection
    //either there are no points that intersect the circle,
    //or the cell is entirely in the circle
    //if ( msgLevel(MSG::DEBUG) ) debug()<<"no points. continuing"<<endmsg;
  return 0.;}
  
  //for(auto pt : points2check){
    //if ( msgLevel(MSG::DEBUG) ) debug()<<"got point ("<<std::get<0>(pt)<<", "<<std::get<1>(pt)<<")"<<endmsg;
  //}
  int this_int = 0;
  for(int i=0; i<num_integration_points; ++i){    
    //use MC integration to get the area inside this b
    //first throw random point into the circle
    auto xpos = m_flatDist() * (x2-x1) + x1 ;//m_Rand.Uniform(x1,x2);
    auto ypos = m_flatDist() * (y2-y1) + y1;//m_Rand.Uniform(y1,y2);
    //now if the point is inside the region we want, then iterate
    if(xpos*xpos+ypos*ypos<(num_RM*m_moliere_radius.value())*(num_RM*m_moliere_radius.value())){//in the circle.
      this_int+=1;
    }
  }
  //if ( msgLevel(MSG::DEBUG) ) debug()<<"integral = "<<this_int
  //<<", fraction = "<<(double)this_int/num_integration_points.<<endmsg;
  //energy fraction is fraction of total circle in this square,
  //so the area of the circle in the square is the MC integral * area of square
  //fraction of circle is area of intesection / pi r^2
  //if ( msgLevel(MSG::DEBUG) ) debug()<<"area of square = "<<(x2-x1)<<"*"<<(y2-y1)<<endmsg;
  //if ( msgLevel(MSG::DEBUG) ) debug()<<"area of circle = "
  //<<3.14159*num_RM*num_RM*m_moliere_radius.value()*m_moliere_radius.value()<<endmsg;
  
  double ret = (double)this_int/
    ((double)num_integration_points) *(x2-x1)*(y2-y1)/(3.14159*num_RM*m_moliere_radius.value()*num_RM*m_moliere_radius.value());
  
  return ret;
}

  
///to be replaced by friend class/inheritance

std::unique_ptr<LHCb::State> LamarrCaloProto::MCPropagationToState(LHCb::MCParticle *mcPart,
                                                                   double zProp,LHCb::State::Location location)
{
  double x = mcPart->originVertex()->position().X();
  double y = mcPart->originVertex()->position().Y();
  double z = mcPart->originVertex()->position().Z();
  // double t = mcPart->originVertex()->time();
  
  double px = mcPart->momentum().Px();
  double py = mcPart->momentum().Py();
  double pz = mcPart->momentum().Pz();
  double p = mcPart->momentum().P();
  double e = mcPart->momentum().E();
  
  double tx = px/pz;
  double ty = py/pz;
  double q = mcPart->particleID().threeCharge()/3.0;
  double qOverP = q/p;
  double c_light = 2.99792458E11;  
  double gammam = e / (c_light*c_light);

  double zCenter = computeZMagnetCenter(qOverP);
  double ptKick = 1.0e-9 < fabs(q) ? m_ptKick.value() : 0.;

  if(((q > 0) && (m_magnetPolarity.value() == "up")) || ((q < 0) && (m_magnetPolarity.value() == "down"))) ptKick = -ptKick;

  debug()<<"MCPROPAGATIONTOSTATE <== Propagating particle from z = "
         <<z<<" to zProp: "<<zProp
         <<"\t zCenter "<<zCenter<<" relative to qOverP of "<<qOverP<<endmsg;

  auto state = std::make_unique<LHCb::State>();    
  state->setState(x,y,z,tx,ty,qOverP);
  state->setLocation(location);
  
  if((zProp >= z && zProp<zCenter) || (zProp >= z && q==0))
  {
   
    // double tz = gammam / (pz) * (-z + zProp);
    double xk = x + tx* (zProp - z);
    double yk = y + ty * (zProp - z);
    state->setState(xk,yk,zProp,tx,ty,qOverP);
    return state;
    
  }

  if(zProp >= z && zProp>=zCenter && q!=0)
  {    
    double tz = gammam / (pz) * (-z + zCenter);
    double xk = x + tx * (zCenter - z);
    double yk = y + ty * (zCenter - z);
    px += ptKick;    
    double px2 = px*px;
    double py2 = py*py;
    double pz2 = TMath::Power(p,2) - py2 - px2;
    if(pz2>0){pz = TMath::Sqrt(pz2);}
    //Add pz=0 in case pz2<0?
    tx = px / pz;    
    ty = py / pz;
    tz += gammam / (pz) * (zProp - zCenter);
    xk += tx * (zProp - zCenter);    
    yk += ty * (zProp - zCenter);
    state->setState(xk,yk,zProp,tx,ty,qOverP);
    return state;  
  }
  
  debug()<<"MCPROPAGATIONTOSTATE: Particle not propagated, return state at z = "<<z<<endmsg;
  if(z>StateParameters::ZEndVelo) state->setLocation(LHCb::State::Location::LocationUnknown);
  
  return state;  
}

double LamarrCaloProto::computeZMagnetCenter(double qOverP)
{
  if(m_formulaCenterMagnet.value()=="") return m_valueCenterMagnet.value();
  auto zMagnetFormula = std::make_unique<TFormula>("zMagnetFormula",m_formulaCenterMagnet.value().c_str(),1);
  
  double zCenter = zMagnetFormula->Eval(qOverP);
  
  return zCenter;  
}


std::tuple<double,double,double>LamarrCaloProto::get_Cali_PositionAndEnergy(double x, double y, double E, int region){
  double x_cali = 0;
  double y_cali = 0;
  double E_cali = 0;
  double cell_size = m_calo_x_bins.value().at(region-1);
  double x_Rec_Cell = (x/cell_size-floor(x/cell_size))*cell_size-cell_size/2;
  double y_Rec_Cell = (y/cell_size-floor(y/cell_size))*cell_size-cell_size/2;
  switch (region){
  case 1:
    x_cali = m_inner_position_f*m_inner_position_b*asinh( 0.5*x_Rec_Cell/cell_size * cosh(0.5*cell_size/m_inner_position_b));
    y_cali = m_inner_position_f*m_inner_position_b*asinh( 0.5*y_Rec_Cell/cell_size * cosh(0.5*cell_size/m_inner_position_b));
    E_cali = E/(1-m_inner_energy_cali);
    break;
  case 2:
    x_cali = m_middle_position_f*m_middle_position_b*asinh( 0.5*x_Rec_Cell/cell_size * cosh(0.5*cell_size/m_middle_position_b));
    y_cali = m_middle_position_f*m_middle_position_b*asinh( 0.5*y_Rec_Cell/cell_size * cosh(0.5*cell_size/m_middle_position_b));
    E_cali = E/(1-m_middle_energy_cali);
    break;
    
  case 3:
    x_cali = m_outer_position_f*m_outer_position_b*asinh( 0.5*x_Rec_Cell/cell_size * cosh(0.5*cell_size/m_outer_position_b));
    y_cali = m_outer_position_f*m_outer_position_b*asinh( 0.5*y_Rec_Cell/cell_size * cosh(0.5*cell_size/m_outer_position_b));
    E_cali = E/(1-m_outer_energy_cali);
    break;
    
  default:
    error()<<"Unknown calorimeter region!"<<endmsg;
    return std::make_tuple(0.,0.,0.);
  };
  x_cali = floor(x/cell_size)*cell_size+cell_size*0.5 + x_cali;
  y_cali = floor(y/cell_size)*cell_size+cell_size*0.5 + y_cali;
  return std::make_tuple(x_cali,y_cali,E_cali);
}


LamarrSimEcalClusters LamarrCaloProto::find_ecal_clusters(LHCb::MCParticles* parts){
  LamarrSimEcalClusters ecal_tower_pos_and_energy;
  for(auto part : *parts){
    const LHCb::ParticleID &partID = part->particleID();
    if ( msgLevel(MSG::DEBUG) )debug()<<"processing PID "<<partID.pid()<<endmsg;
    //Charged particles
    
    if(!(partID.abspid()==22 || partID.abspid()==111 
         || partID.abspid()==2112 || partID.abspid()==11)){
      //only photons, pi0s or neutrons, or electrons
      return ecal_tower_pos_and_energy;
    }
    
    if( msgLevel(MSG::DEBUG) ){ debug()<<"checking mc propagation = "<<endmsg;}
    //AD. 28-5-18:
    //need maximum 3 loops here to account for possible pileup effects
    //first loop, form all the cell pairs and the relative energies
    //then loop to find ovelap for pileup
    //then form all the LHCb specific things
    //setup
    Double_t px = part->momentum().Px();
    Double_t py = part->momentum().Py();
    Double_t pz = part->momentum().Pz();

    double orig_energy = sqrt(px*px+py*py+pz*pz);  
    //first, find the particle position at the calo
    //for construction of the LHCb::CaloDigit
    //propagate the MC particle to the calorimeter face.
    auto v = MCPropagationToState(part,m_zEcal.value(),LHCb::State::BegECal);
    double x =  v->position().x();
    double y = v->position().y();
    double z = v->position().z();
    LamarrSimEcalCluster ecal_mc_cluster = Get_Ecal_MC_Cluster(x,y,z);
    int region = ecal_mc_cluster.SeedRegion();
    //check. if the propagation was outside of the calo area or propagated incorrectly
    if(LHCb::Detector::Calo::CellCode::CaloArea::UndefinedArea == region){continue;}
    ecal_mc_cluster.SetMCKey(part->key());
    double maxYsize = ecal_mc_cluster.MaxYsize();
    double maxXsize = ecal_mc_cluster.MaxXsize();
    //int numCellsX = ecal_mc_cluster.NCellsX();
    //int numCellsY = ecal_mc_cluster.NCellsY();  
    
    
    
      
    int row = ecal_mc_cluster.SeedRow();
    int col = ecal_mc_cluster.SeedCol();
    
    // //generate shower in n moliere radii
    float thisminX = (col -m_cellCenterX.value())*maxXsize;
    float thismaxX = (col+1 -m_cellCenterX.value())*maxXsize;
    float thisminY = (row-m_cellCenterY.value())*maxYsize;
    float thismaxY = (row+1 - m_cellCenterY.value())*maxYsize;
    if ( msgLevel(MSG::DEBUG) ) debug()<<"checking intersection of hit {"<<x<<","<<y<<"} with box {"
                                       <<thisminX<<","<<thisminY<<"}--{"<<thismaxX<<","<<thismaxY<<"}"<<endmsg;
    if ( msgLevel(MSG::DEBUG) ) debug()<<"ROOT: TGraph* seed = new TGraph(); seed->SetPoint(0,"
                                       <<x<<","<<y
                                       <<");seed->SetMarkerColor(kRed);seed->SetMarkerStyle(kCircle);"<<endmsg;
    if ( msgLevel(MSG::DEBUG) ) debug()<<"ROOT: TBox* box = new TBox("<<thisminX<<","<<thisminY<<","
                                       <<thismaxX<<","<<thismaxY<<"); box->SetLineColor(kGreen);box->SetFillStyle(0);"<<endmsg;
      
    //loop over 3 moliere radii to see if we spill into another region
    int spillRegion = -1;
    //note, LHCb::Detector::Calo::CellCode::CaloArea is 0 for outer, then 1 for middle, 2, inner, as defined in
    // LHCbKernel/Kernel/LHCb::Detector::Calo::CellCode.h
    // use this fact to look inside and outside.
    int num_moliere_radii = m_CaloClusterLoopSize;

    bool multiRegion = false;
    bool spills_inside_current_region = false;//if it spills and doesn't spill inside, it spills outside      
    //in casting this grid, we can tell if the grid falls into an inner or outer region,
    //defining the behaviour of combining or not combining cells
    
    for(int i=-1*num_moliere_radii; i<=num_moliere_radii; i++){
      for(int j=-1*num_moliere_radii; j<=num_moliere_radii; j++){
        if ( msgLevel(MSG::DEBUG) ) debug()<<"now on moliere radius point"<<"\t {" <<x + i*m_moliere_radius.value()
                                           <<","<<y + j*m_moliere_radius.value()<<"},"<<endmsg;
        //does this point land in another region?
        //try the same criteria with box
        
        if((fabs(x+i*m_moliere_radius.value())<m_calo_boundary_info[region]["Xlow"]
            && fabs(y+j*m_moliere_radius.value())<m_calo_boundary_info[region]["Ylow"])&&!multiRegion){
          //spills into region inside
            
          if ( msgLevel(MSG::DEBUG) ) debug()<<"spills into inner region!"<<endmsg;

          multiRegion = true;
          spills_inside_current_region = true;
          if (region != LHCb::Detector::Calo::CellCode::CaloArea::Inner){
            spillRegion = region+1;
          }else
          {
            spillRegion = LHCb::Detector::Calo::CellCode::CaloArea::UndefinedArea;
          }
          
            
        }
        else if((fabs(x+i*m_moliere_radius.value())>m_calo_boundary_info[region]["Xhigh"]
                 || fabs(y+j*m_moliere_radius.value())>m_calo_boundary_info[region]["Yhigh"])
                &&!multiRegion){//splills into outer region
          if ( msgLevel(MSG::DEBUG) ) debug()<<"spills into outer region!"<<endmsg;
          if(region != LHCb::Detector::Calo::CellCode::CaloArea::Outer){
            spillRegion=region-1;
          }else
          {
            spillRegion = LHCb::Detector::Calo::CellCode::CaloArea::UndefinedArea;
          }
          
          multiRegion = true;
        }
      }
    }

    //int count = 0;
    //iterate around bin.
    for(int i=-1*m_CaloClusterLoopSize.value(); i<=m_CaloClusterLoopSize.value(); i++){//m_CaloClusterLoopSize.value()
      for(int j=-1*m_CaloClusterLoopSize.value(); j<=m_CaloClusterLoopSize.value(); j++ ){//m_CaloClusterLoopSize.value()
        //only difference between same region and multiregion is where the boundaries lie.
          
        auto thiscol = col+i;
        auto thisrow = row+j;
        auto min_x = (thiscol - m_cellCenterX.value())*maxXsize;
        auto max_x = (thiscol+1 - m_cellCenterX.value())*maxXsize;
        auto min_y = (thisrow-m_cellCenterY.value())*maxYsize;
        auto max_y = (thisrow+1 - m_cellCenterY.value())*maxYsize;
                    
        //now cluster. 
        if(!multiRegion){
          //find the associated fraction of points in this cell
          //first find all the points in the cell.
            
          auto thisfrac1 = EnergyFraction((min_x-x), 
                                          (max_x-x), 
                                          (min_y-y), 
                                          (max_y-y),1., m_num_int_pts.value());
          auto thisfrac2 = EnergyFraction((min_x-x), 
                                          (max_x-x), 
                                          (min_y-y), 
                                          (max_y-y),2.,m_num_int_pts.value());
          auto thisfrac3 = EnergyFraction((min_x-x), 
                                          (max_x-x), 
                                          (min_y-y), 
                                          (max_y-y),3.5,m_num_int_pts.value());
          
          //cout<<"(*cell fraction: 1 Rm: "<<thisfrac1<<", 2 Rm: "<<thisfrac2<<", 3.5 Rm: "<<thisfrac3<<"*)"<<endl;
          //ecal_tower_pos_and_energy.push_back(std::make_tuple(borderIndex,thisrow,thiscol,
          //                                                  orig_energy*(thisfrac1*0.90 + thisfrac2*0.05 + thisfrac3*0.04),
          //borderIndex,row,col));
          ecal_mc_cluster.AddTower(maxXsize,maxYsize,
                                   region,thisrow,thiscol,orig_energy*(thisfrac1*0.90 + thisfrac2*0.05 + thisfrac3*0.04));
          if(thisrow==ecal_mc_cluster.SeedRow() && thiscol==ecal_mc_cluster.SeedCol()){
            ecal_mc_cluster.SetSeedEnergy(orig_energy*(thisfrac1*0.90 + thisfrac2*0.05 + thisfrac3*0.04));              
          }
        }
        else{//multiregion
          if ( msgLevel(MSG::DEBUG) ) debug()<<"in multiregion area"<<endmsg;            
          if ( msgLevel(MSG::DEBUG) ) debug()<<"checking inside vs outside criteria"<<endmsg;
          //criteria is we have a cell in some region.
          //is the point we are at in the same region?
          //if it is in the same region, no worries.
          //if it isn't, then adjust cell size appropriately            
            
            
          auto curr_cell_x_center = fabs((min_x+max_x)/2.);
          auto curr_cell_y_center = fabs((min_y+max_y)/2.);
          
          bool inspill = is_inside(-1*m_calo_boundary_info[spillRegion]["Xhigh"],//then ask if it is in the box
                                   -1*m_calo_boundary_info[spillRegion]["Yhigh"],
                                   m_calo_boundary_info[spillRegion]["Xhigh"],
                                   m_calo_boundary_info[spillRegion]["Yhigh"],
                                   curr_cell_x_center,curr_cell_y_center);
              
          bool is_in_spill_region = spills_inside_current_region?inspill : !inspill;
          //if spills inside, has to be inside, otherwise, has to be out
          if(spillRegion==LHCb::Detector::Calo::CellCode::CaloArea::UndefinedArea && is_in_spill_region){
            if ( msgLevel(MSG::DEBUG) ) debug()<<"no cells and we are in the spill region!! continue!"<<endmsg;
            continue;
          }
            
            
          if ( msgLevel(MSG::DEBUG) ) debug()<<"current cell center (x,y) = ("<<curr_cell_x_center<<","
                                             <<curr_cell_y_center<<")"<<endmsg;
          
          if ( msgLevel(MSG::DEBUG) ) debug()<<"check of boundary logic: spills_inside_current_region = "
                                             <<spills_inside_current_region
                                             <<"is_in_spill_region = "<< is_in_spill_region<<endmsg;
            
            
            
          //first, find out if this row/col is within the right area:
          if ( msgLevel(MSG::DEBUG) ) debug()<<"maxXsize = "<<maxXsize<<", maxYsize = "<<maxYsize<<endmsg;
          if(is_in_spill_region){
              
            if(spills_inside_current_region){
              if(spillRegion==LHCb::Detector::Calo::CellCode::CaloArea::UndefinedArea)continue;//no beampipe
              int new_reg_col = floor((min_x+max_x)/2.)/m_calo_boundary_info[spillRegion]["CellSizeX"]+
                m_cellCenterX.value();
              int new_reg_row = floor((min_y+max_y)/2.)/m_calo_boundary_info[spillRegion]["CellSizeY"]+
                m_cellCenterY.value();
              if ( msgLevel(MSG::DEBUG) ) debug()<<"got new region row,col<<("<<new_reg_row<<","<<new_reg_col<<")"<<endmsg;
                
              //now iterate around this bin
              for(int ii=0; ii<maxXsize/m_calo_boundary_info[spillRegion]["CellSizeX"]; ++ii){
                float spillRegminX =(new_reg_col+ii 
                                     -m_cellCenterX.value())*m_calo_boundary_info[spillRegion]["CellSizeX"];
                float spillRegmaxX =(new_reg_col+1+ii 
                                     -m_cellCenterX.value())*m_calo_boundary_info[spillRegion]["CellSizeX"];
                for(int jj=0; jj<maxYsize/m_calo_boundary_info[spillRegion]["CellSizeY"];++jj){
                  if ( msgLevel(MSG::DEBUG) ) debug()<<"now in ii,jj "<<ii<<","<<jj<<endmsg;
                    
                  float spillRegminY = (new_reg_row+jj
                                        -m_cellCenterY.value())*m_calo_boundary_info[spillRegion]["CellSizeY"];
                  float spillRegmaxY = (new_reg_row+1+jj 
                                        - m_cellCenterY.value())*m_calo_boundary_info[spillRegion]["CellSizeY"];
                    
                  //get the energy fraction and compute.
                  auto thisfrac1 = EnergyFraction((spillRegminX-x), 
                                                  (spillRegmaxX-x), 
                                                  (spillRegminY-y), 
                                                  (spillRegmaxY-y),1.);
                  auto thisfrac2 = EnergyFraction((spillRegminX-x), 
                                                  (spillRegmaxX-x), 
                                                  (spillRegminY-y), 
                                                  (spillRegmaxY-y),2.);
                  auto thisfrac3 = EnergyFraction((spillRegminX-x), 
                                                  (spillRegmaxX-x), 
                                                  (spillRegminY-y), 
                                                  (spillRegmaxY-y),3.5);
                    
                  // int the_spill_region = (LHCb::Detector::Calo::CellCode::CaloArea::Outer==region)?LHCb::Detector::Calo::CellCode::CaloArea::Middle : 
                  //   (LHCb::Detector::Calo::CellCode::Middle== region ? LHCb::Detector::Calo::CellCode::CaloArea::Inner:-1);
                  
                  if ( msgLevel(MSG::DEBUG) ) debug()<<"spill.SetBinContent(spill.FindBin("<<(spillRegminX+spillRegmaxX)/2.<<","
                                                     <<(spillRegminY+spillRegmaxY)/2.<<"),"
                                                     <<thisfrac1*0.90 + thisfrac2*0.05 + thisfrac3*0.04<<");"<<endmsg;
                  
                  ecal_mc_cluster.AddTower(m_calo_boundary_info[spillRegion]["CellSizeX"],
                                           m_calo_boundary_info[spillRegion]["CellSizeY"],
                                           spillRegion,new_reg_row+jj,new_reg_col+ii,
                                           orig_energy*(thisfrac1*0.90 + thisfrac2*0.05 + thisfrac3*0.04));
                    
                }//jj  
              }//ii
                
            }//inner boundary
              
            else{//outside boundary
              if ( msgLevel(MSG::DEBUG) ) 
                debug()<<"*******************this is an outside boundary case***************************"<<endmsg;
              //cell size is larger, not smaller
              if(spillRegion==LHCb::Detector::Calo::CellCode::CaloArea::UndefinedArea){
                if ( msgLevel(MSG::DEBUG) ) debug()<<"no spill outside the calo!"<<endmsg;
                continue;
              }
                                                
                
              //get division of bins inside
              if ( msgLevel(MSG::DEBUG) ) debug()<<"now merging cells into "
                                                 <<m_calo_boundary_info[spillRegion]["CellSizeX"]/maxXsize
                                                 <<" x "<<m_calo_boundary_info[spillRegion]["CellSizeY"]/maxYsize
                                                 <<" bins"<<endmsg;
              //find bin corresponding to this position in the other region.
              int new_reg_col = floor((min_x+max_x)/2.)/m_calo_boundary_info[spillRegion]["CellSizeX"]
                + m_cellCenterX.value();
              int new_reg_row = floor((min_y+max_y)/2.)/m_calo_boundary_info[spillRegion]["CellSizeY"]
                + m_cellCenterY.value();
              //if ( msgLevel(MSG::DEBUG) ) debug()<<"got new region row,col<<("<<new_reg_row<<","<<new_reg_col<<")"<<endmsg;
              float spillRegminX = (new_reg_col 
                                    -m_cellCenterX.value())*m_calo_boundary_info[spillRegion]["CellSizeX"];
              float spillRegmaxX = (new_reg_col+1 
                                    -m_cellCenterX.value())*m_calo_boundary_info[spillRegion]["CellSizeX"];
              float spillRegminY = (new_reg_row 
                                    -m_cellCenterY.value())*m_calo_boundary_info[spillRegion]["CellSizeY"];
                
              float spillRegmaxY = (new_reg_row+1 
                                    - m_cellCenterY.value())*m_calo_boundary_info[spillRegion]["CellSizeY"];
              // int the_spill_region = (LHCb::Detector::Calo::CellCode::CaloArea::Inner==region)?LHCb::Detector::Calo::CellCode::CaloArea::Middle : 
              //   (LHCb::Detector::Calo::CellCode::Middle== region ? LHCb::Detector::Calo::CellCode::CaloArea::Outer: -1);
              
              if ( msgLevel(MSG::DEBUG) ) debug()<<"spills into outer region"<<endmsg;
              //ask if we already constructed this cell. If so, move on
              bool already_found = false;
              for(auto x : ecal_mc_cluster.Towers())
              {
                if( x.Region() == spillRegion && x.Row()==new_reg_row && x.Col()==new_reg_col)
                {
                  if ( msgLevel(MSG::DEBUG) ) debug()<<"already found this one! Continuing!"<<endmsg;
                  already_found = true;
                }
              }
              if(already_found) continue;
                
              auto thisfrac1 = EnergyFraction((spillRegminX-x), 
                                              (spillRegmaxX-x), 
                                              (spillRegminY-y), 
                                              (spillRegmaxY-y),1.);
              auto thisfrac2 = EnergyFraction((spillRegminX-x), 
                                              (spillRegmaxX-x), 
                                              (spillRegminY-y), 
                                              (spillRegmaxY-y),2.);
              auto thisfrac3 = EnergyFraction((spillRegminX-x), 
                                              (spillRegmaxX-x), 
                                              (spillRegminY-y), 
                                              (spillRegmaxY-y),3.5);
              if ( msgLevel(MSG::DEBUG) ) debug()<<"spill.SetBinContent(spill.FindBin("
                                                 <<(spillRegminX+spillRegmaxX)/2.<<","
                                                 <<(spillRegminY+spillRegmaxY)/2.<<"),"
                                                 <<thisfrac1*0.90 + thisfrac2*0.05 + thisfrac3*0.04<<");"<<endmsg;
              //ecal_tower_pos_and_energy.push_back(std::make_tuple(spillRegion,new_reg_row,new_reg_col,
              //orig_energy*(thisfrac1*0.90 + thisfrac2*0.05 + thisfrac3*0.04),
              //borderIndex,row,col));
                  
              ecal_mc_cluster.AddTower(m_calo_boundary_info[spillRegion]["CellSizeX"],
                                       m_calo_boundary_info[spillRegion]["CellSizeY"],
                                       spillRegion,new_reg_row,new_reg_col,
                                       orig_energy*(thisfrac1*0.90 + thisfrac2*0.05 + thisfrac3*0.04));
                
              if ( msgLevel(MSG::DEBUG) ) debug()<<"merging "
                                                 <<m_calo_boundary_info[spillRegion]["CellSizeX"]/maxXsize
                                                 <<" x "<<m_calo_boundary_info[spillRegion]["CellSizeY"]/maxYsize
                                                 <<" bins"<<endmsg;
            }
          }            
          else{//normal
            auto thisfrac1 = EnergyFraction((min_x-x), 
                                            (max_x-x), 
                                            (min_y-y), 
                                            (max_y-y),1.);
            auto thisfrac2 = EnergyFraction((min_x-x), 
                                            (max_x-x), 
                                            (min_y-y), 
                                            (max_y-y),2.);
            auto thisfrac3 = EnergyFraction((min_x-x), 
                                            (max_x-x), 
                                            (min_y-y), 
                                            (max_y-y),3.5);
              
            //cout<<"(*cell fraction: 1 Rm: "<<thisfrac1<<", 2 Rm: "<<thisfrac2<<", 3.5 Rm: "<<thisfrac3<<"*)"<<endl;
            if ( msgLevel(MSG::DEBUG) ) debug()<<"tmp.SetBinContent(tmp.FindBin("<<(min_x+max_x)/2.<<","<<(min_y+max_y)/2.<<"),"
                                               <<thisfrac1*0.90 + thisfrac2*0.05 + thisfrac3*0.04<<");"<<endmsg;
              
            ecal_mc_cluster.AddTower(maxXsize,maxYsize,region,thisrow,thiscol,
                                     orig_energy*(thisfrac1*0.90 + thisfrac2*0.05 + thisfrac3*0.04));
            if(thisrow==ecal_mc_cluster.SeedRow() && thiscol==ecal_mc_cluster.SeedCol()){
              ecal_mc_cluster.SetSeedEnergy(orig_energy*(thisfrac1*0.90 + thisfrac2*0.05 + thisfrac3*0.04));                
            }
            if ( msgLevel(MSG::DEBUG) ) debug()<<"\t"<< "ListLinePlot[{{"
                                               <<min_x<<","<<min_y<<"},{"
                                               <<min_x<<","<<max_y<<"},{"
                                               <<max_x<<","<<max_y<<"},{"
                                               <<max_x<<","<<min_y<<"},{"
                                               <<min_x<<","<<min_y<<"}}],"<<endmsg;
              
          }
        }//end multiRegion checks
      }//loop on j = ybins
    }//loop on i = xbins
    ecal_tower_pos_and_energy.push_back(ecal_mc_cluster);
    
  }// loop on particles
  
  
  return ecal_tower_pos_and_energy;
}


LamarrSimEcalCluster LamarrCaloProto::Get_Ecal_MC_Cluster(double x, double y, double z){
  int region(LHCb::Detector::Calo::CellCode::CaloArea::UndefinedArea);
  int row(-1), col(-1);
  if(m_smear_calo_intercept){info()<<"no calo intercept smearing yet. todo"<<endmsg;}
  
  if ( msgLevel(MSG::DEBUG) )debug()<<"checking z = "<<z<<endmsg;
  
  //change border_index, maxYsize, maxXsize to something a bit more descriptive
  
  double maxYsize{0.},maxXsize{0.};
  int numCellsX{-1};
  int numCellsY{-1};
  
  //define the calo region, row and column.
  //from Patrick, for each section, (32,32) is always the center, so define from that.
  //means that not all start at zero!!
  if(msgLevel(MSG::DEBUG))debug()<<"on particle with z="<<z<<endmsg;
  
  if(fabs(z -m_zEcal.value())<1e-10){
    
    
    if( fabs(x)< m_calo_x_borders.value().at(0) &&fabs(y)<m_calo_y_borders.value().at(0)){
      if ( msgLevel(MSG::DEBUG) ) debug()<<"particle in beam pipe"<<endmsg;
      return LamarrSimEcalCluster(LHCb::Detector::Calo::CellCode::CaloArea::UndefinedArea ,-1,-1);
    }
    //is this in the box for the inner region?
    if(fabs(x)< m_calo_x_borders.value().at(1) && fabs(y) < m_calo_y_borders.value().at(1)){
      region = LHCb::Detector::Calo::CellCode::CaloArea::Inner;
      maxYsize = m_calo_y_bins.value().at(0);
      maxXsize = m_calo_x_bins.value().at(0);
      //find the correct row and column of the hit cell.
      numCellsX = m_calo_x_borders.value().at(1)/maxXsize;//24 for default lhcb geo
      numCellsY = m_calo_y_borders.value().at(1)/maxYsize;//18 for default lhcb geo
      if ( msgLevel(MSG::DEBUG) ) debug()<<"got numCells (x,y) = ("<<numCellsX<<","<<numCellsY<<") per half"<<endmsg;
    }
    else if(!(fabs(x)< m_calo_x_borders.value().at(1) && fabs(y) < m_calo_y_borders.value().at(1))
            &&fabs(x)< m_calo_x_borders.value().at(2) && fabs(y) < m_calo_y_borders.value().at(2)){
      region  = LHCb::Detector::Calo::CellCode::CaloArea::Middle;
      maxYsize = m_calo_y_bins.value().at(1);
      maxXsize = m_calo_x_bins.value().at(1);
      //find the correct row and column of the hit cell.
      numCellsX = m_calo_x_borders.value().at(1)/maxXsize;//24 for default lhcb geo
      numCellsY = m_calo_y_borders.value().at(1)/maxYsize;//18 for default lhcb geo
      if ( msgLevel(MSG::DEBUG) ) debug()<<"got numCells (x,y) = ("<<numCellsX<<","<<numCellsY<<") per half"<<endmsg;
    }
    else if(!(fabs(x)< m_calo_x_borders.value().at(2) && fabs(y)< m_calo_y_borders.value().at(2))&&
            fabs(x)< m_calo_x_borders.value().at(3) && fabs(y)< m_calo_y_borders.value().at(3)){
      region = LHCb::Detector::Calo::CellCode::CaloArea::Outer;
      maxYsize = m_calo_y_bins.value().at(2);
      maxXsize = m_calo_x_bins.value().at(2);
      
    }
    else{
      if ( msgLevel(MSG::DEBUG) ) debug()<<"outside of calo acceptance!!"<<endmsg;
      return LamarrSimEcalCluster(LHCb::Detector::Calo::CellCode::CaloArea::UndefinedArea,-1,-1);
    }
    if ( msgLevel(MSG::DEBUG) ) debug()<<"check of maxXsize, maxYsize = ("<<maxXsize<<","<<maxYsize<<")"<<endmsg;
    
  }
  else{
    error()<<"something wrong with particle propagation to ecal face! check!"<<endmsg;
    return LamarrSimEcalCluster(LHCb::Detector::Calo::CellCode::CaloArea::UndefinedArea,-1,-1);
  }
  if (msgLevel(MSG::DEBUG) ) {
    debug()<<"got region "<<region<<", with (maxXsize,maxYsize)=("<<maxXsize<<","<<maxYsize<<")"<<endmsg;
  }
  
  row = floor(y/maxYsize)+m_cellCenterY.value();
  col = floor(x/maxXsize)+m_cellCenterX.value();
  if ( msgLevel(MSG::DEBUG) )
    debug()<<"got end vertex ("<<
      x<<" , "<<
      y<<" ) "<<endmsg;    
  
  if ( msgLevel(MSG::DEBUG) ) debug()<<"corresponding to (row,col) (" <<row<<","<<col<<")"<<endmsg;
  
  
  LamarrSimEcalCluster ecal_mc_cluster(region,row,col);
  ecal_mc_cluster.SetNCellsX(numCellsX);
  ecal_mc_cluster.SetNCellsY(numCellsY);
  ecal_mc_cluster.SetMaxXsize(maxXsize);
  ecal_mc_cluster.SetMaxYsize(maxYsize);
  return ecal_mc_cluster;
}



void LamarrCaloProto::CombineOverlappingCluster(LamarrSimEcalClusters& clusters){
  //loop over all clusters and combine the energy of overlaps.
  std::map<std::tuple<int,int,int>, int> firedCells;
  for(auto ecalCluster1 : clusters){//loop over MC parts, effectively      
    for( auto clusterCell : ecalCluster1.Towers()){
      std::tuple<int,int,int>thistup = std::make_tuple(clusterCell.Region(),clusterCell.Row(),clusterCell.Col());
      if(firedCells.find(thistup)!=firedCells.end()){//cell exists.
        firedCells[thistup]+=1;
      }
      else{firedCells[thistup]=1;}
    }//end loop on cells
  }//end loop on clusters
  if ( msgLevel(MSG::DEBUG) ) debug()<<"found "<<firedCells.size()<<" total cells"<<endmsg;
  for(auto contr_cell : firedCells){
    if(contr_cell.second>=1){
      if ( msgLevel(MSG::DEBUG) ) debug()<<"cell in region "<<std::get<0>(contr_cell.first)<<", (row, col) = ("
                                         <<std::get<1>(contr_cell.first) <<","
                                         << std::get<2>(contr_cell.first)<<") has spill over"<<endmsg;
      //sum energy
      float ecell = 0;
      for(auto clus : clusters){
        for(auto cel : clus.Towers()){
          if(cel.Region()==std::get<0>(contr_cell.first) 
             && cel.Row()==std::get<1>(contr_cell.first) 
             && cel.Col()==std::get<2>(contr_cell.first)){
            if ( msgLevel(MSG::DEBUG) ) debug()<<"adding energy "<<cel.E()<<"to total energy of cell ("
                                               <<cel.Region()<<","<<cel.Row()<<", "<<cel.Col()<<")"<<endmsg;              
            ecell+=cel.E();
          }
          
        }//cells
      }//clusters
      //reset energy
      for(auto clus : clusters){
        for(auto cel : clus.Towers()){
          if(cel.Region()==std::get<0>(contr_cell.first) 
             && cel.Row()==std::get<1>(contr_cell.first) 
             && cel.Col()==std::get<2>(contr_cell.first)){
            if ( msgLevel(MSG::DEBUG) ) debug()<<"resetting cell "
                                               <<cel.Region()<<","
                                               <<cel.Row()<<", "
                                               <<cel.Col()
                                               <<"with energy "<<ecell<<endmsg;
            
            cel.SetE(ecell);            
          }//end if
        }//end of second cell loop.
      }//end of second cluster loop
      
    }//end of multiple fired cells            
  }//end of fired cells    
  return;
}
