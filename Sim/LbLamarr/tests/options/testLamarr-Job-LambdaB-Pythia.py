###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from Gaudi.Configuration import *
from Configurables import LbLamarr

eventType = "15874000"

importOptions("$APPCONFIGOPTS/Gauss/DataType-2016.py")
importOptions("$DECFILESROOT/options/{eventType}.py".format(eventType=eventType))
importOptions("$LBPYTHIA8ROOT/options/Pythia8.py")

LbLamarr().EventType = eventType
LbLamarr().EvtMax    = 10
LbLamarr().DataType  = '2016'
LbLamarr().Polarity  = 'MagUp'

importOptions("$APPCONFIGOPTS/Gauss/Beam6500GeV-mu100-2016-nu1.6.py")
importOptions("$GAUSSOPTS/DBTags-2016.py")
importOptions("$APPCONFIGOPTS/Gauss/DataType-2016.py")

