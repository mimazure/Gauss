###############################################################################
# (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
## Options to set options production cuts, tracking cuts
## and muon filter thresholds for loew energy background
## productions
##
## Author: P.Griffith
## Date:   2019-10-18
##

from Configurables import Gauss
from GaudiKernel import SystemOfUnits
from Gaudi.Configuration import *

## In addition one has to modify the Physics List hadronic part to add High Precision (HP)
## processes for low energy neutrons, e.g. FTFP_BERT_HP

Gauss().UserProductionCuts = {
        'CutForElectron'    : 0.5*SystemOfUnits.mm,
        'CutForPositron'    : 0.5*SystemOfUnits.mm,
        'CutForGamma'       : 0.5*SystemOfUnits.mm,
}
Gauss().UserTrackingCuts = {
        'MuonTrCut'     : 10.0*SystemOfUnits.MeV,
        'pKpiCut'       : 0.1*SystemOfUnits.MeV,
        'NeutrinoTrCut' : 0.0*SystemOfUnits.MeV,
        'NeutronTrCut'  : 0.0*SystemOfUnits.MeV,
        'GammaTrCut'    : 0.03*SystemOfUnits.MeV,
        'ElectronTrCut' : 0.03*SystemOfUnits.MeV,
        'OtherTrCut'    : 0.0*SystemOfUnits.MeV,
}

def muonLowEnergySim():
    from Configurables import SimulationSvc
    SimulationSvc().SimulationDbLocation = "$GAUSSROOT/xml/MuonLowEnergy.xml"
appendPostConfigAction(muonLowEnergySim)

