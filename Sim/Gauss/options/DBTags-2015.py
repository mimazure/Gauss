###############################################################################
# (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
# File with LATEST database tags for Sim10
# YEAR = 2015 for pp-collisions
from Configurables import LHCbApp
LHCbApp().DDDBtag   = "dddb-20210215-5"
LHCbApp().CondDBtag = "sim-20201113-5-vc-md100-Sim10"


