###############################################################################
# (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from Gauss.Configuration import *

from Configurables import GaudiSequencer
seqGenFSR = GaudiSequencer("GenFSRSeq")
seqGenFSR.Members += ["GenFSRMerge", "GenFSRLog"]

ApplicationMgr().TopAlg += [seqGenFSR]

from Configurables import LHCbApp
LHCbApp(DataType = "2012", Simulation = True)

EventSelector().Input += [
    "DATAFILE='root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/genFSR-2012/Gauss-13114005-test1.sim' TYP='POOL_ROOTTREE' OPT='READ'",
    "DATAFILE='root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/genFSR-2012/Gauss-13114005-test2.sim' TYP='POOL_ROOTTREE' OPT='READ'",
    "DATAFILE='PFN:genFSR_2012_Gauss_created.xgen' TYP='POOL_ROOTTREE' OPT='READ'"
    ]

LHCbApp().EvtMax = -1
LHCbApp().Simulation = True

#--Set database tags
#LHCbApp().DDDBtag   = "dddb-20170721-2"
#LHCbApp().CondDBtag = "sim-20160321-2-vc-md100"

from GaudiConf import IOHelper
idFile = "genFSR_2012_Gauss_merged.xgen"
IOHelper().outStream(idFile)
