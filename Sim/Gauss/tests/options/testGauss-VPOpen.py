###############################################################################
# (c) Copyright 2000-2021 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from __future__ import print_function
# Override SIMCOND/Conditions/Online/Velo/MotionSystem to set parameters to open
# <!-- VELO motion control open to max +- 29mm, y offset = 0 -->
#<param name="ResolPosRC" type="double">-29</param>
#<param name="ResolPosLA" type="double">+29</param>
#<param name="ResolPosY" type="double">0</param>
from Configurables import UpdateManagerSvc
UpdateManagerSvc().ConditionsOverride += \
   ["Conditions/Online/Velo/MotionSystem := "
    "double ResolPosRC = -29.0 ; double ResolPosLA = 29.0 ;  double ResolPosY = 0.0 ;"]

# remove vvvvvvvvvvvv
# These lines should be deleted when the DBTags-2022.py has a newer 
# version of SIMCOND that includes the VELO motion control
from Configurables import LHCbApp
LHCbApp().CondDBtag = "sim-20210630-vc-md100"
print("Using SIMCOND",LHCbApp().CondDBtag, "to include VP opening in conditions")
# remove ^^^^^^^^^^^^

print("ConditionsOverride = " + ",\n".join(UpdateManagerSvc().ConditionsOverride))
