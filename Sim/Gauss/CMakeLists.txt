###############################################################################
# (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
################################################################################
# Package: Gauss
################################################################################
gaudi_subdir(Gauss v55r4)

find_package(HepMC)

gaudi_depends_on_subdirs(Det/DDDB
                         Event/EventPacker
                         Gaudi
                         GaudiConf
                         GaudiKernel
                         Sim/SimComponents
                         Sim/GaussAlgs
                         Gen/LbEvtGen
                         Gen/LbPGuns
                         Gen/LbMIB
                         Gen/LbPythia
                         Gen/LbPythia8
                         Gen/LbHijing
                         Gen/LbCRMC
                         Sim/GaussAlgs
                         Sim/GaussKine
                         Sim/GaussRICH
                         Sim/GaussRedecay
                         Sim/GaussCherenkov
                         Sim/GaussCalo
                         Sim/GaussTracker
                         Sim/GaussMonitor
                         Velo/VeloGauss
                         Muon/MuonMoniSim)

gaudi_install_python_modules()
gaudi_install_scripts()

gaudi_env(SET AlwaysKillLeadingHadron 1
          SET GAUSSOPTS \${GAUSSROOT}/options)

gaudi_add_test(QMTest QMTEST)
