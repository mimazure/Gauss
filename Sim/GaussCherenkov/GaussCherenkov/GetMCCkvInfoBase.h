/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// $Id: GetMCCkvInfoBase.h,v 1.5 2009-07-17 13:46:12 jonrob Exp $
#ifndef SENSDET_GetMCCkvInfoBase_H
#define SENSDET_GetMCCkvInfoBase_H 1

// base class
#include "RichKernel/RichAlgBase.h"

// from Gaudi

// from GiGa
#include "GiGa/IGiGaSvc.h"
#include "GiGa/GiGaTrajectory.h"
#include "GiGa/GiGaUtil.h"
#include "GiGa/GiGaHitsByID.h"
#include "GiGa/GiGaHitsByName.h"

// from GiGaCnv
#include "GiGaCnv/IGiGaKineCnvSvc.h"
#include "GiGaCnv/IGiGaCnvSvcLocation.h"
#include "GiGaCnv/GiGaKineRefTable.h"

// from Geant4
#include "G4TrajectoryContainer.hh"
#include "G4ParticleDefinition.hh"
#include "G4VHitsCollection.hh"
#include "G4HCofThisEvent.hh"
#include "G4SDManager.hh"

// from LHCb
#include "Event/MCRichTrack.h"
#include "Event/MCParticle.h"

// RichUtils
#include "RichUtils/RichStatDivFunctor.h"
#include "RichUtils/RichPoissonEffFunctor.h"

// Relations
#include "Relations/Relation1D.h"
//#include "Relations/RelationWeighted1D.h"
// RichDet
#include "RichDet/DeRichSystem.h"


// local
#include "GaussCherenkov/CkvG4HitCollName.h"
#include "GaussCherenkov/CkvG4Hit.h"

/** @class GetMCCkvInfoBase GetMCCkvInfoBase.h
 *
 *  Base class for RICH GiGa converter algorithms
 *
 *  @author Sajan EASO
 *  @date   2011-03-06
 */

class GetMCCkvInfoBase : public Rich::AlgBase
{

public:

  /// Standard constructor
  GetMCCkvInfoBase( const std::string& name, ISvcLocator* pSvcLocator );

  ~GetMCCkvInfoBase( ) = default;   ///< Destructor

  StatusCode initialize() override;    ///< Algorithm initialization
  StatusCode finalize  () override;    ///< Algorithm finalization
  StatusCode sysExecute(const EventContext& ctx) override;    ///< Algorithm system execute

protected:

  /** accessor to GiGa Service on demand
   *  @return pointer to GiGa Service
   */
  inline IGiGaSvc* gigaSvc() const
  {
    if ( !m_gigaSvc )
    {
      m_gigaSvc = svc<IGiGaSvc>( m_gigaSvcName );
    }
    return m_gigaSvc;
  }

  /** accessor to kinematics  conversion service
   *  @return pointer to kinematics conversion service
   */
  inline IGiGaKineCnvSvc* kineSvc() const
  {
    if ( !m_gigaKineCnvSvc )
    {
      m_gigaKineCnvSvc = svc<IGiGaKineCnvSvc>( m_kineSvcName );
    }
    return m_gigaKineCnvSvc;
  }

  /// Access collection data
  inline const std::vector<int> & colRange() const { return m_colRange; }

  /// Access collection name data on demand
  inline CkvG4HitCollName * RichG4HitCollectionName() const
  {
    if ( !m_RichG4HitCollectionName )
    {
      m_RichG4HitCollectionName = new  CkvG4HitCollName();
      // if(m_SuperRichFlag) m_RichG4HitCollectionName->setCollConfigWithSuperRich();
      // the following already in the constructor above.
      //if(m_Rich2UseGrandPmt) m_RichG4HitCollectionName->setCollConfigWithMixedPmtSet();

    }
    return m_RichG4HitCollectionName;
  }

  /// data location in TES (const)
  const std::string & dataLocationInTES() const { return m_dataToFill; }

  /// Which RICH is activate
  inline bool richIsActive( const Rich::DetectorType rich ) const
  {
    // return m_RICHes[rich];
    //return (SuperRichFlag() || ( m_RICHes[rich]) );
    return  ( m_RICHes[rich] ) ;
  }

  /// Location of CkvG4Hit to MCRichHit relations
  inline std::string g4HitToMCRichHitRelLoc() const
  {
    return "/Event/MC/Rich/RichG4HitToMCRichHitRelation";
  }

  //  inline bool SuperRichFlag() const
  // {  return m_SuperRichFlag;}

private:

  /// Fill collection data
  void getRichG4CollectionRange();

protected:

  /// Relations between RichG4Hits and MCRichHits
  typedef LHCb::Relation1D<const CkvG4Hit*,LHCb::MCRichHit> G4HitTable;

  /// Get the MCRichHit associated to a given CkvG4Hits
  const LHCb::MCRichHit * getMCRichHit( const CkvG4Hit* g4hit );

private:

  std::string        m_gigaSvcName;                      ///< Name of GiGa Service
  std::string        m_kineSvcName;                      ///< Name of GiGaKine Service
  mutable IGiGaSvc*          m_gigaSvc;                  ///< Pointer to GiGa Service
  mutable IGiGaKineCnvSvc*   m_gigaKineCnvSvc;           ///< Pointer to GiGaKine Service
  mutable CkvG4HitCollName* m_RichG4HitCollectionName;  ///< G4 hit collections for RICH
  std::vector<int> m_colRange;                           ///< Collection data
  std::vector<bool> m_RICHes;                            ///< The RICH detectors to create data objects for
  G4HitTable * m_relationTable;                          ///< G4 hit to MCRichHit relation table
  // bool m_SuperRichFlag;                                 // true for SuperRich, false for two-RICH option
  bool m_Rich2UseGrandPmt;                                 // true when Rich2 uses Grand PMTs.

protected:

  std::string      m_dataToFill;                         ///< Data location in TES to fill

};

#endif // SENSDET_GetMCCkvInfoBase_H
