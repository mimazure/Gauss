/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// $Id: $
#ifndef CHERENKOVTESTBEAMSENSDET_TORCHTBHITCOLLNAME_H 
#define CHERENKOVTESTBEAMSENSDET_TORCHTBHITCOLLNAME_H 1

// Include files
#include "globals.hh"

/** @class TorchTBHitCollName TorchTBHitCollName.h CherenkovTestBeamSensDet/TorchTBHitCollName.h *  
 *
 *  @author Sajan Easo
 *  @date   2012-05-28
 */
class TorchTBHitCollName {
public: 
  /// Standard constructor
  TorchTBHitCollName( ); 

  virtual ~TorchTBHitCollName( ); ///< Destructor
  G4String TorchTBHCName()
  {  return m_TorchTBHCName;}
  
protected:

private:
  G4String  m_TorchTBHCName;
  
};
#endif // CHERENKOVTESTBEAMSENSDET_TORCHTBHITCOLLNAME_H
