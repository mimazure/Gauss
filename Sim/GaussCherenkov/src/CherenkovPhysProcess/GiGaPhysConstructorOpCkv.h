/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// ============================================================================
#ifndef GIGA_GiGaPhysConstructorOpCkv_H
#  define GIGA_GiGaPhysConstructorOpCkv_H 1
// ============================================================================
// include files

// GaudiKernel

// GiGa
#  include "GiGa/GiGaPhysConstructorBase.h"

// forward declarations
template <class TYPE>
class GiGaFactory;

class GiGaPhysConstructorOpCkv : public GiGaPhysConstructorBase {
  /// friend factory for instantiation
  friend class GiGaFactory<GiGaPhysConstructorOpCkv>;

public:
  GiGaPhysConstructorOpCkv( const std::string& type, const std::string& name, const IInterface* parent );

public:
  void ConstructParticle() override;
  void ConstructProcess() override;
  //  void SetCuts ();
  void ConstructPeProcess();
  // Now adding processes for optical photons SE 30-1-2002
  void ConstructOp();
  ///

  bool RichActivateVerboseProcessInfoTag() { return m_RichActivateVerboseProcessInfoTag.value(); }
  void setRichActivateVerboseProcessInfoTag( bool aVTag ) { m_RichActivateVerboseProcessInfoTag.value() = aVTag; }
  int  MaxPhotonsPerRichCherenkovStep() { return m_MaxPhotonsPerRichCherenkovStep.value(); }
  void setMaxPhotonsPerRichCherenkovStep( int aMaxValue ) { m_MaxPhotonsPerRichCherenkovStep.value() = aMaxValue; }
  bool ApplyMaxPhotCkvLimitPerRadiator() { return m_ApplyMaxPhotCkvLimitPerRadiator.value(); }
  void setApplyMaxPhotCkvLimitPerRadiator( bool aLimFlag ) { m_ApplyMaxPhotCkvLimitPerRadiator.value() = aLimFlag; }
  // int MaxPhotonsPerRichCherenkovStepInRich1Agel()
  // {    return m_MaxPhotonsPerRichCherenkovStep.value()InRich1Agel;}
  // void setMaxPhotonsPerRichCherenkovStepInRich1Agel(int aAgelMaxPhot)
  // {m_MaxPhotonsPerRichCherenkovStep.value()InRich1Agel=aAgelMaxPhot;}
  int MaxPhotonsPerRichCherenkovStepInRich1Gas()
  {    return m_MaxPhotonsPerRichCherenkovStepInRich1Gas.value();}
  void setMaxPhotonsPerRichCherenkovStepInRich1Gas(int aRich1GasMaxPhot)
  {m_MaxPhotonsPerRichCherenkovStepInRich1Gas.value()=aRich1GasMaxPhot;}
  int MaxPhotonsPerRichCherenkovStepInRich2Gas()
  {    return  m_MaxPhotonsPerRichCherenkovStepInRich2Gas.value();}
  void setMaxPhotonsPerRichCherenkovStepInRich2Gas(int aRich2MaxPhot)
  {m_MaxPhotonsPerRichCherenkovStepInRich2Gas.value()=aRich2MaxPhot;}
  int MaxPhotonsPerRichCherenkovStepInRichQuartzLikeRadiator()
  {    return m_MaxPhotonsPerRichCherenkovStepInRichQuartzLikeRadiator.value();}
  void setMaxPhotonsPerRichCherenkovStepInRichQuartzLikeRadiator(int aQuartzMaxPhot)
  {m_MaxPhotonsPerRichCherenkovStepInRichQuartzLikeRadiator.value()=aQuartzMaxPhot;}


  std::vector<G4String> RichRadiatorMaterialName()
  {    return m_RichRadiatorMaterialName;}
  void setRichRadiatorMaterialName
  (std::vector<G4String> aRichRadiatorMaterialName)
  {m_RichRadiatorMaterialName=aRichRadiatorMaterialName;}
  std::vector<G4int> RichRadiatorMaterialIndex()
  {    return m_RichRadiatorMaterialIndex;}
  void setRichRadiatorMaterialIndex
  (std::vector<G4int> aRichRadiatorMaterialIndex)
  {m_RichRadiatorMaterialIndex=aRichRadiatorMaterialIndex;}
  std::vector<G4int> FindRichRadiatorMaterialIndices
  ( std::vector<G4String> aRadiatorMaterialNames);
  //  int MaxAllowedPhotStepNumInRayleigh()
  // {    return m_MaxAllowedPhotStepNumInRayleigh;}
  // void setMaxAllowedPhotStepNumInRayleigh(int aMaxRayleighLimit)
  // { m_MaxAllowedPhotStepNumInRayleigh=aMaxRayleighLimit;}
  //  int MaxNumberRayleighScatAllowed()
  // {  return  m_MaxNumberRayleighScatAllowed;}
  // void setMaxNumberRayleighScatAllowed(int amrn)
  // { m_MaxNumberRayleighScatAllowed=amrn;}

  //    bool  UseHpdMagDistortions()
  // {
  //  return m_UseHpdMagDistortions;
  // }
  // void setUseHpdMagDistortions(bool afla)
  // { m_UseHpdMagDistortions= afla;
  // }
  // bool IsPSFPreDc06Flag() {return m_IsPSFPreDc06Flag;}
  // void  setIsPreDc06Flag(bool apsfg) {m_IsPSFPreDc06Flag = apsfg;}
  bool PmtQEUseNominalTable(){    return m_PmtQEUseNominalTable.value();}
  void setPmtQEUseNominalTable(bool aQEOption){m_PmtQEUseNominalTable.value()=aQEOption;}

  bool activateRICHOpticalPhysProcStatus()  {  return m_ActivateRICHOpticalPhysProc.value();}
  void setRICHOpticalPhysProcActivation(bool aAct)
  {m_ActivateRICHOpticalPhysProc.value()=aAct;}

  bool activateRICHCF4Scintillation()
  {  return m_activateRICHCF4Scintillation.value();}

  //  bool activateRICHCF4ScintillationHisto()
  //{return m_activateRICHCF4Scintillation.value()Histo;}

  bool activateTorchTestBeamSimulation()
  {  return m_activateTorchTestBeamSimulation.value();}
  void setaActivateTorchTestBeamSimulation ( bool aActivateTorchTB)
  { m_activateTorchTestBeamSimulation.value()=aActivateTorchTB; }

private:
  ///
  GiGaPhysConstructorOpCkv( const GiGaPhysConstructorOpCkv& );
  GiGaPhysConstructorOpCkv& operator=( const GiGaPhysConstructorOpCkv& );
  ///
  Gaudi::Property<bool> m_RichActivateVerboseProcessInfoTag{this,"RichActivateRichPhysicsProcVerboseTag",false,"RichActivateRichPhysicsProcVerboseTag"};
  Gaudi::Property<int> m_MaxPhotonsPerRichCherenkovStep{this,"RichMaxNumPhotPerCherenkovStep",40,"RichMaxNumPhotPerCherenkovStep"};
  Gaudi::Property<bool> m_ApplyMaxPhotCkvLimitPerRadiator{this,"RichApplyMaxNumCkvPhotPerStepPerRadiator",false,"RichApplyMaxNumCkvPhotPerStepPerRadiator"};
  //  int m_MaxPhotonsPerRichCherenkovStep.value()InRich1Agel;
  Gaudi::Property<int> m_MaxPhotonsPerRichCherenkovStepInRich1Gas{this,"RichMaxPhotonsPerCherenkovStepInRich1Gas",40,"RichMaxPhotonsPerCherenkovStepInRich1Gas"};
  Gaudi::Property<int> m_MaxPhotonsPerRichCherenkovStepInRich2Gas{this,"RichMaxPhotonsPerCherenkovStepInRich2Gas",40,"RichMaxPhotonsPerCherenkovStepInRich2Gas"};
  Gaudi::Property<int> m_MaxPhotonsPerRichCherenkovStepInRichQuartzLikeRadiator{this,"RichMaxPhotonsPerCherenkovStepInRichQuartzLikeRadiators",5000,"RichMaxPhotonsPerCherenkovStepInRichQuartzLikeRadiators"};
  std::vector<G4String> m_RichRadiatorMaterialName{};
  std::vector<G4int> m_RichRadiatorMaterialIndex{};
  //  int m_MaxAllowedPhotStepNumInRayleigh;
  //int m_MaxNumberRayleighScatAllowed;
  // bool m_UseHpdMagDistortions;
  // bool m_IsPSFPreDc06Flag;
  Gaudi::Property<bool> m_PmtQEUseNominalTable{this,"RichPmtUseNominalQETable",true,"RichPmtUseNominalQETable"};
  Gaudi::Property<bool> m_ActivateRICHOpticalPhysProc{this,"RichOpticalPhysicsProcessActivate",true,"RichOpticalPhysicsProcessActivate"};
  Gaudi::Property<bool> m_ActivatePmtPhotoElectricPhysProc{this,"RichPmtPhotoElectricPhysicsProcessActivate",true,"RichPmtPhotoElectricPhysicsProcessActivate"};
  Gaudi::Property<bool> m_activateRICHCF4Scintillation{this,"RichActivateCF4Scintillation",true,"RichActivateCF4Scintillation"};
  // bool m_activateRICHCF4Scintillation.value()Histo;
  Gaudi::Property<bool> m_RichApplyScintillationYieldScaleFactor{this, "RichApplyScintillationYieldScaleFactor", true, "RichApplyScintillationYieldScaleFactor"};
  Gaudi::Property<double> m_RichScintillationYieldScaleFactor{this, "RichScintillationYieldScaleFactor", 1.0, "RichScintillationYieldScaleFactor"};
  Gaudi::Property<bool> m_CherenkovAddBackgrRich2{this, "Rich2BackgrHitsActivate", false, "Rich2BackgrHitsActivate"};
  Gaudi::Property<double> m_CherenkovRich2BackgrProbFactor{this, "Rich2BackgrHitsProbabilityFactor", 0.0,"Rich2BackgrHitsProbabilityFactor"};
  Gaudi::Property<int> m_PmtQESource{this,"RichPmtQESource",0,"RichPmtQESource"};
  Gaudi::Property<double> m_R1PmtQEScaleFactor{this,"Rich1PmtQEOverallScaling",1.0,"Rich1PmtQEOverallScaling"};
  Gaudi::Property<double> m_R2PmtQEScaleFactor{this,"Rich2PmtQEOverallScaling",1.0,"Rich2PmtQEOverallScaling"};
  Gaudi::Property<bool> m_activateTorchTestBeamSimulation{this,"ActivateTorchTBSimulation",false,"ActivateTorchTBSimulation"};
  Gaudi::Property<bool> m_activatePmtModuleSupSet3{this,"ActivatePmtModuleSuppressSet3",false,"ActivatePmtModuleSuppressSet3"};
  Gaudi::Property<bool> m_activatePmtModuleSupSet4{this,"ActivatePmtModuleSuppressSet4",false,"ActivatePmtModuleSuppressSet4"};
  Gaudi::Property<bool> m_activatePmtModuleSupSet5{this,"ActivatePmtModuleSuppressSet5",false,"ActivatePmtModuleSuppressSet5"};
  Gaudi::Property<bool> m_activatePmtModuleSupSet6{this,"ActivatePmtModuleSuppressSet6",false,"ActivatePmtModuleSuppressSet6"};
  //
  Gaudi::Property<bool> m_activatePmtSupSet0{this,"ActivatePmtSuppressSet0",false,"ActivatePmtSuppressSet0"};
  Gaudi::Property<bool> m_activatePmtSupSet1{this,"ActivatePmtSuppressSet1",false,"ActivatePmtSuppressSet1"};
  Gaudi::Property<bool> m_activatePmtSupSet2{this,"ActivatePmtSuppressSet2",false,"ActivatePmtSuppressSet2"};

};
// ============================================================================

// ============================================================================
#endif ///< GIGA_GiGaPhysConstructorOpCkv_H
// ============================================================================
