/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// $Id:GiGaPhysConstructorPhotoDetector.cpp,v 1.12 2009-03-06 10:52:36 seaso Exp $
// Include files 

// from Gaudi

// G4 
#include "G4ParticleTypes.hh"
#include "G4ParticleDefinition.hh"
#include "G4ParticleWithCuts.hh"
#include "G4ProcessManager.hh"
#include "G4ProcessVector.hh"
#include "G4ParticleTable.hh"
#include "G4Material.hh"
#include "G4ios.hh"
#include "G4Transportation.hh"
// #include "G4MultipleScattering.hh"
#include "G4LossTableManager.hh"

// local
#include "GiGaPhysConstructorPhotoDetector.h"
#include "GaussRICH/RichPhotoElectron.h"
#include "GaussCherenkov/CkvGeometrySetupUtil.h"

//#include "GaussRICH/RichG4GaussPathNames.h"
//#include "DetDesc/DetectorElement.h"
//#include "RichDet/DeRichSystem.h"

//-----------------------------------------------------------------------------
// Implementation file for class :GiGaPhysConstructorPhotoDetector
//
// 2002-12-12 : Witek Pokorski
// 2003-04-29 : Sajan Easo
// 2007-01-11 : Gloria Corti, modified for Gaudi v19
//-----------------------------------------------------------------------------

// Declaration of the Tool Factory
DECLARE_COMPONENT( GiGaPhysConstructorPhotoDetector )


//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
GiGaPhysConstructorPhotoDetector::GiGaPhysConstructorPhotoDetector
( const std::string& type   ,
  const std::string& name   ,
  const IInterface*  parent )
  : GiGaPhysConstructorBase( type , name , parent )
{}


//=============================================================================
// ConstructParticle
//=============================================================================
void GiGaPhysConstructorPhotoDetector::ConstructParticle()
{
      RichPhotoElectron::PhotoElectronDefinition(); 

}

//=============================================================================
// ConstructProcess
//=============================================================================
void GiGaPhysConstructorPhotoDetector::ConstructProcess()
{
  MsgStream msg(msgSvc(), name());


  //  IDetectorElement* Rich1DE = getDet<IDetectorElement> (Rich1DeStructurePathName );
  // if( !Rich1DE ){
  //  msg << MSG::INFO  <<" No RICH1 detector element. Possibly RICH system not activated. " 
  //         <<" Hence No RICH HPD Physics Process Activated"<<endmsg;
  //  setRICHHPDPhysProcActivation(false); 
  // }


    ConstructPeGenericProcess();
    msg << MSG::INFO <<" RICHPmtPhysProcess Activation status  " << activateRICHPmtPhysProcStatus() << endmsg;
    

    if( activateRICHPmtPhysProcStatus() || ActivateTorchTBMcpEnergyLossProc()  ){    

      // CkvGeometrySetupUtil * aCkvGeometrySetup=CkvGeometrySetupUtil::getCkvGeometrySetupUtilInstance();
     //  if(! (aCkvGeometrySetup->isSuperRich() ) ) {  
       // temporarily deactivate for superrich for test.
       

      ConstructPmtSiEnLoss();
      
      //     }
     
    }
  
}

//=============================================================================
// ConstructPeGenericProcess
//=============================================================================
void GiGaPhysConstructorPhotoDetector::ConstructPeGenericProcess() {
  // first remove any process assigned to this particle from elsewhere.
  // then add the Transportation, HpdSiEnergyloss process to it.
  // More processes to be added later.
  MsgStream msg(msgSvc(), name());

  //        G4double aPeCut=10.0*km;
  // G4double aPeCut=0.1*mm;
  //  G4ParticleDefinition* photoelectronDef = 
  //  RichPhotoElectron::PhotoElectron();
  G4Transportation* theTransportationProcess= new G4Transportation();
  auto theParticleIterator = GetParticleIterator();
  theParticleIterator->reset();
  while( (*theParticleIterator)() ){
    G4ParticleDefinition* particle = theParticleIterator->value();
    if(  particle->GetParticleName() == "pe-" )
      {
        G4ProcessManager* pmanager =  particle->GetProcessManager();
        G4ProcessVector* pVector = 
          (particle->GetProcessManager())->GetProcessList();
        // msg << MSG::DEBUG << "size of ProcList for pe-  so far  "
        //  <<(G4int)  pVector->size() << endmsg;
        if( (G4int)  pVector->size() > 0 ) { 
          //  msg << MSG::DEBUG 
          //  <<" For pe-  disassociating following processes "<<  endmsg; 
          //  pmanager->DumpInfo();
          for(G4int ip=0; ip < (G4int)  pVector->size() ; ++ip ){
       	    pmanager->RemoveProcess(ip);
          }
          //  msg << MSG::DEBUG <<" For pe- end of process cleanup "<<  endmsg; 
          pmanager ->AddProcess(theTransportationProcess,-1,1,2);
         } else {
           //  msg << MSG::DEBUG 
           //   <<"  pe-  only has no process so far"<<  endmsg;
        
           pmanager ->AddProcess(theTransportationProcess,-1,1,2);          
         }
        
        
        //  pmanager->DumpInfo();
          //particle->SetCuts(aPeCut);
          // particle->SetApplyCutsFlag(true);
        // particle-> DumpTable() ;
      }
    
  }
  
}

//=============================================================================

#include "RichPmtSiEnergyLoss.h"
#include "TorchTBMcpEnergyLoss.h"

//=============================================================================
// ConstructPmtSiEnLoss
//=============================================================================
void GiGaPhysConstructorPhotoDetector::ConstructPmtSiEnLoss()
{
  // Add Decay Process
  //  G4Decay* theDecayProcess = new G4Decay();

  //  RichHpdSiEnergyLoss* theRichHpdSiEnergyLossProcess =
  //  new RichHpdSiEnergyLoss("RichHpdSiEnergyLossProcess", fUserDefined );
  RichPmtSiEnergyLoss* theRichPmtSiEnergyLossProcess =
    new RichPmtSiEnergyLoss("RichPmtSiEnergyLossProcess", fUserDefined );
  theRichPmtSiEnergyLossProcess->setPmtSiDetEff ( m_RichPmtSiDetEfficiency.value());
  theRichPmtSiEnergyLossProcess->setPmtSiPixelChipEff (m_RichPmtPixelChipEfficiency.value());
  theRichPmtSiEnergyLossProcess->setPmtPeBackScaProb(m_RichPmtPeBackScatterProb.value());
  theRichPmtSiEnergyLossProcess->InitializePmtProcParam();


  TorchTBMcpEnergyLoss* theTorchTBMcpEnergyLossProcess =0;

  if( m_ActivateTorchTBMcpEnergyLossProc.value()) {
   theTorchTBMcpEnergyLossProcess =  new TorchTBMcpEnergyLoss("TorchTBMcpEnergyLossProcess", fUserDefined  );
   theTorchTBMcpEnergyLossProcess->setMcpAnodeDetEff(m_TorchTBMcpAnodeEfficiency.value() );
   theTorchTBMcpEnergyLossProcess->setMcpAnodePixelChipEff(m_TorchMcpAnodeReadoutChipEfficiency.value());
   theTorchTBMcpEnergyLossProcess->InitializeMcpProcParam();
  }

  auto theParticleIterator = GetParticleIterator();
  theParticleIterator->reset();
  while( (*theParticleIterator)() ){
    G4ParticleDefinition* particle = theParticleIterator->value();
    G4ProcessManager* pmanager = particle->GetProcessManager();
    //    G4cout<<"ConstructPmtSiEnLoss: Now at particle:  "<< particle->GetParticleName()<<G4endl;
    
    pmanager->SetVerboseLevel(0);

    if(   activateRICHPmtPhysProcStatus() ) {
      
    if( theRichPmtSiEnergyLossProcess->IsApplicable(*particle) ){

            pmanager->AddProcess( theRichPmtSiEnergyLossProcess ,-1,2,2);
            
    }
    }
    
    if( m_ActivateTorchTBMcpEnergyLossProc.value() ) {
      
      if( theTorchTBMcpEnergyLossProcess ->IsApplicable(*particle) ){

            pmanager->AddProcess( theTorchTBMcpEnergyLossProcess ,-1,2,2);
            
       }
     
    }
    
	      if(particle->GetParticleName() == "pe-")
	      {
                (RichPhotoElectron::PhotoElectron())->SetProcessManager(pmanager);
                
	      }
        
  }
  
  
}





//=============================================================================
