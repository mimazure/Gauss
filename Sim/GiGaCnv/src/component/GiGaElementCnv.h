/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef         GIGACNV_GIGAElementCnv_H
#define         GIGACNV_GIGAElementCnv_H  1 

/// STL 
#include <set>
/// GiGaCnv
#include "GiGaCnv/GiGaCnvBase.h"
#include "GiGaCnv/GiGaLeaf.h"
///
template <class TYPE>
class CnvFactory;
class Material;
class Mixture;
class Element;
class Isotope;

/** @class GiGaElementCnv GiGaElementCnv.h
 *
 *  Converter of Element class to Geant4
 *
 *  @author  Vanya Belyaev
 */

class GiGaElementCnv: public GiGaCnvBase
{
 public:
 /// Standard Constructor
  GiGaElementCnv( ISvcLocator* );
  /// Standard (virtual) destructor
  virtual ~GiGaElementCnv();
  ///
 public:

  /// Create representation
  StatusCode createRep
  ( DataObject*      Object  ,
    IOpaqueAddress*& Address ) override;

  /// Update representation
  StatusCode updateRep
  ( IOpaqueAddress*  Address,
    DataObject*      Object ) override;

  /// Class ID for created object == class ID for this specific converter
  static const CLID&          classID();
  
  /// storage Type 
  static unsigned char storageType() ; 
  
private:

  GiGaLeaf m_leaf;

};

// ============================================================================
/// End
// ============================================================================
#endif   ///< GIGACNV_GIGAElementCnv_H
// ============================================================================



