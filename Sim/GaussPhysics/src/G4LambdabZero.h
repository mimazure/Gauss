/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// $Id: G4LambdabZero.h,v 1.1 2006-01-09 20:52:07 robbep Exp $

#ifndef G4LambdabZero_h
#define G4LambdabZero_h 1

#include "globals.hh"
#include "G4ios.hh"
#include "G4ParticleDefinition.hh"

// ######################################################################
// ###                         LambdabZero                        ###
// ######################################################################

class G4LambdabZero : public G4ParticleDefinition
{
 private:
  static G4LambdabZero * theInstance ;
  G4LambdabZero( ) { }
  ~G4LambdabZero( ) { }


 public:
  static G4LambdabZero * Definition() ;
  static G4LambdabZero * LambdabZeroDefinition() ;
  static G4LambdabZero * LambdabZero() ;
};


#endif
