/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// $Id: G4CharginoPlus.cpp,v 1.2 2008-11-27 20:18:13 robbep Exp $

#include <fstream>
#include <iomanip>

#include "G4CharginoPlus.h"
#include "G4ParticleTable.hh"

// ######################################################################
// ###                      CharginoPlus                              ###
// ######################################################################

G4CharginoPlus * G4CharginoPlus::theInstance = 0 ;

G4CharginoPlus * G4CharginoPlus::Definition()
{

  if (theInstance !=0) return theInstance;
  const G4String name = "s_chi_plus_1";
  // search in particle table]
  G4ParticleTable* pTable = G4ParticleTable::GetParticleTable();
  G4ParticleDefinition* anInstance = pTable->FindParticle(name);
  if (anInstance ==0)
  {
  // create particle
  //
  //    Arguments for constructor are as follows
  //               name             mass          width         charge
  //             2*spin           parity  C-conjugation
  //          2*Isospin       2*Isospin3       G-parity
  //               type    lepton number  baryon number   PDG encoding
  //             stable         lifetime    decay table
  //             shortlived      subType    anti_encoding
    anInstance = 
      new G4ParticleDefinition( name , 110.00*CLHEP::GeV, 2.73e-13*CLHEP::GeV, +1.*CLHEP::eplus, 
                                1,              0,             0,
                                0,              0,             0,
                                "supersymmetric", 0,  0,  1000024,
                                false,     6.68e-1*CLHEP::ns,      NULL,
                                false, "CharginoPlus" );
  }
  theInstance = reinterpret_cast<G4CharginoPlus*>(anInstance);
  return theInstance;
}

G4CharginoPlus*  G4CharginoPlus::CharginoPlusDefinition(){return Definition();}
G4CharginoPlus*  G4CharginoPlus::CharginoPlus(){return Definition();}

