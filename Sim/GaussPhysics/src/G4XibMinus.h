/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// $

#ifndef G4XibMinus_h
#define G4XibMinus_h 1

#include "globals.hh"
#include "G4ios.hh"
#include "G4ParticleDefinition.hh"

// ######################################################################
// ###                         Xib minus                        ###
// ######################################################################

class G4XibMinus : public G4ParticleDefinition
{
 private:
  static G4XibMinus * theInstance ;
  G4XibMinus( ) { }
  ~G4XibMinus( ) { }


 public:
  static G4XibMinus
 * Definition() ;
  static G4XibMinus * XibMinusDefinition() ;
  static G4XibMinus * XibMinus() ;
};


#endif
