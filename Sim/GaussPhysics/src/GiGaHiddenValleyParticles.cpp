/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include <algorithm>
#include "GaudiKernel/PhysicalConstants.h"
#include "Kernel/IParticlePropertySvc.h"
#include "Kernel/ParticleProperty.h"
#include "GiGa/GiGaPhysConstructorBase.h"
#include "G4ParticleTable.hh"

#include "G4HiddenValley.h"

// ============================================================================
/** @class GiGaHiddenValleyParticles 
 *  simple class to construct Hidden Valley particles 
 *
 *  The major properties:
 *  
 *    - "HiddenValley" : list of hidden valley particles to be created.
 *                       the default value contains [ "pivDiag" , "pivUp", "pivDn", "rhovDiag", "rhovUp", "rhovDn", "Zv", "gv", "qv", "qvbar" ] 
 *
 *  It also prints (@ MSG::INFO) the properties of created particles.
 *  
 *  GiGaHiggsParticles used as a template to write this file (Vanya Belyaev).
 *
 *  @author Carlos VAZQUEZ SIERRA carlos.vazquez@cern.ch
 *  @date 2018-02-03
 */

class GiGaHiddenValleyParticles : public GiGaPhysConstructorBase
{
public:
  // ==========================================================================
  /// constructor 
  GiGaHiddenValleyParticles    
  ( const std::string& type   ,
    const std::string& name   ,
    const IInterface*  parent ) 
    : GiGaPhysConstructorBase ( type , name , parent )
    , m_hiddenv ()
  {
    m_hiddenv.push_back ( "pivDiag" ) ;
    m_hiddenv.push_back ( "pivUp" ) ;
    m_hiddenv.push_back ( "pivDn" ) ;
    m_hiddenv.push_back ( "rhovDiag" ) ;
    m_hiddenv.push_back ( "rhovUp" ) ;
    m_hiddenv.push_back ( "rhovDn" ) ;
    m_hiddenv.push_back ( "Zv" ) ;
    m_hiddenv.push_back ( "gv" ) ;
    m_hiddenv.push_back ( "qv" ) ;
    m_hiddenv.push_back ( "qvbar" ) ;
    declareProperty 
      ("HiddenValley" , m_hiddenv , 
       "The list of Hidden Valley particles to be instantiated") ;  
  }
  // ==========================================================================
  /// construct the particles 
  void ConstructParticle () override    ; // construct the particles
  /// construct the processed  (empty) 
  void ConstructProcess  () override {} ; // construct the processed
  // ==========================================================================
protected:
  // ==========================================================================  
  /// get the particle property for the given particle name 
  const LHCb::ParticleProperty* pp 
  ( const std::string&    n , 
    LHCb::IParticlePropertySvc* s ) const 
  {
    Assert ( 0 != s , "Invalid ParticleProperty Service") ;
    const LHCb::ParticleProperty* p = s->find ( n ) ;
    Assert ( 0 != p , "No information is available for '" + n + "'") ;
    return p ;
  }
  // ==========================================================================
private:
  // ==========================================================================
  typedef std::vector<std::string> Strings ;
  /// list of Hidden Valley particles to be instantiated
  Strings    m_hiddenv ; // list of Hidden Valley particles to be instantiated
  // ==========================================================================
};

// ============================================================================
// construct the particles 
// ============================================================================
void GiGaHiddenValleyParticles::ConstructParticle () // construct the particles 
{
  
  LHCb::IParticlePropertySvc* ppSvc = 
    svc<LHCb::IParticlePropertySvc> ("LHCb::ParticlePropertySvc") ;
  
  Strings tmp = m_hiddenv ;
  {
    // ========================================================================
    Strings::iterator it = std::find  ( tmp.begin() , tmp.end() , "pivDiag" ) ;
    if ( tmp.end() != it )
    {
      const LHCb::ParticleProperty* piv = pp ( "pivDiag" , ppSvc ) ;
      G4pivDiag* pi_v = G4pivDiag::Create 
        ( piv -> mass     () ,       
          piv -> lifetime () * Gaudi::Units::c_light ) ;
      Assert ( 0 != pi_v , "Unable to create pivDiag" ) ;
      if ( msgLevel ( MSG::INFO )  ) { pi_v->DumpTable () ; }  
      tmp.erase ( it ) ;  
    }
    // ========================================================================
    it = std::find  ( tmp.begin() , tmp.end() , "pivUp" ) ;
    if ( tmp.end() != it )
    {
      const LHCb::ParticleProperty* pivup = pp ( "pivUp" , ppSvc ) ;
      G4pivUp* piv_up = G4pivUp::Create 
        ( pivup -> mass     () ,       
          pivup -> lifetime () * Gaudi::Units::c_light ) ;
      Assert ( 0 != piv_up , "Unable to create pivUp" ) ;
      if ( msgLevel ( MSG::INFO ) ) { piv_up->DumpTable () ; }  
      tmp.erase ( it ) ;  
    }
    // ========================================================================
    
    it = std::find  ( tmp.begin() , tmp.end() , "pivDn" ) ;
    if ( tmp.end() != it )
    {
      const LHCb::ParticleProperty* pivdn = pp ( "pivDn" , ppSvc ) ;
      G4pivDn* piv_dn = G4pivDn::Create 
        ( pivdn -> mass     () ,       
          pivdn -> lifetime () * Gaudi::Units::c_light ) ;
      Assert ( 0 != piv_dn , "Unable to create pivDn" ) ;
      if ( msgLevel ( MSG::INFO ) ) { piv_dn->DumpTable () ; }  
      tmp.erase ( it ) ;  
    }
    
    // ========================================================================
    if ( tmp.end() != it )
    {
      const LHCb::ParticleProperty* rhov = pp ( "rhovDiag" , ppSvc ) ;
      G4rhovDiag* pi_v = G4rhovDiag::Create 
        ( rhov -> mass     () ,       
          rhov -> lifetime () * Gaudi::Units::c_light ) ;
      Assert ( 0 != pi_v , "Unable to create rhovDiag" ) ;
      if ( msgLevel ( MSG::INFO )  ) { pi_v->DumpTable () ; }  
      tmp.erase ( it ) ;  
    }
    // ========================================================================
    it = std::find  ( tmp.begin() , tmp.end() , "rhovUp" ) ;
    if ( tmp.end() != it )
    {
      const LHCb::ParticleProperty* rhovup = pp ( "rhovUp" , ppSvc ) ;
      G4rhovUp* rhov_up = G4rhovUp::Create 
        ( rhovup -> mass     () ,       
          rhovup -> lifetime () * Gaudi::Units::c_light ) ;
      Assert ( 0 != rhov_up , "Unable to create rhovUp" ) ;
      if ( msgLevel ( MSG::INFO ) ) { rhov_up->DumpTable () ; }  
      tmp.erase ( it ) ;  
    }
    // ========================================================================
    
    it = std::find  ( tmp.begin() , tmp.end() , "rhovDn" ) ;
    if ( tmp.end() != it )
    {
      const LHCb::ParticleProperty* rhovdn = pp ( "rhovDn" , ppSvc ) ;
      G4rhovDn* rhov_dn = G4rhovDn::Create 
        ( rhovdn -> mass     () ,       
          rhovdn -> lifetime () * Gaudi::Units::c_light ) ;
      Assert ( 0 != rhov_dn , "Unable to create rhovDn" ) ;
      if ( msgLevel ( MSG::INFO ) ) { rhov_dn->DumpTable () ; }  
      tmp.erase ( it ) ;  
    }
    
    // ========================================================================
    it = std::find  ( tmp.begin() , tmp.end() , "Zv" ) ;
    if ( tmp.end() != it )
    {
      const LHCb::ParticleProperty* zv = pp ( "Zv" , ppSvc ) ;
      G4Zv* z_v = G4Zv::Create 
        ( zv -> mass     () ,       
          zv -> lifetime () * Gaudi::Units::c_light ) ;
      Assert ( 0 != z_v , "Unable to create Zv" ) ;
      if ( msgLevel ( MSG::INFO ) ) { z_v->DumpTable () ; }  
      tmp.erase ( it ) ;  
    }
    // ========================================================================
    it = std::find  ( tmp.begin() , tmp.end() , "qv" ) ;
    if ( tmp.end() != it )
    {
      const LHCb::ParticleProperty* qv = pp ( "qv" , ppSvc ) ;
      G4qv* q_v = G4qv::Create 
        ( qv -> mass     () ,       
          qv -> lifetime () * Gaudi::Units::c_light ) ;
      Assert ( 0 != q_v , "Unable to create qv" ) ;
      if ( msgLevel ( MSG::INFO ) ) { q_v->DumpTable () ; }  
      tmp.erase ( it ) ;  
    }
    // ========================================================================
    it = std::find  ( tmp.begin() , tmp.end() , "gv" ) ;
    if ( tmp.end() != it )
    {
      const LHCb::ParticleProperty* gv = pp ( "gv" , ppSvc ) ;
      G4gv* g_v = G4gv::Create 
        ( gv -> mass     () ,       
          gv -> lifetime () * Gaudi::Units::c_light ) ;
      Assert ( 0 != g_v , "Unable to create gv" ) ;
      if ( msgLevel ( MSG::INFO ) ) { g_v->DumpTable () ; }  
      tmp.erase ( it ) ;  
    }
    // ========================================================================
    
    it = std::find  ( tmp.begin() , tmp.end() , "qvbar" ) ;
    if ( tmp.end() != it )
    {
      const LHCb::ParticleProperty* qvb = pp ( "qvbar" , ppSvc ) ;
      G4qvbar* q_vb = G4qvbar::Create 
        ( qvb -> mass     () ,       
          qvb -> lifetime () * Gaudi::Units::c_light ) ;
      Assert ( 0 != q_vb , "Unable to create qvbar" ) ;
      if ( msgLevel ( MSG::INFO ) ) { q_vb->DumpTable () ; }  
      tmp.erase ( it ) ;  
    }
    
    // ========================================================================
    Assert ( tmp.empty() , "Unknown Hidden Valley particles in the list!" ) ;
    // ========================================================================
  }
}

// ============================================================================
/// Declaration of the Tool Factory
DECLARE_COMPONENT ( GiGaHiddenValleyParticles )

// The END 
// ============================================================================

