/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// ============================================================================
#ifndef    GAUSSPHYS_GIGAPHYSUNKNOWNPARTICLES_H
#define    GAUSSPHYS_GIGAPHYSUNKNOWNPARTICLES_H 1
// ============================================================================
// include files
// ============================================================================
// GiGa
// ============================================================================
#include "GiGa/GiGaPhysConstructorBase.h"
#include "G4Decay.hh"
#include "G4UnknownDecay.hh"
// ============================================================================
// forward declarations
template <class TYPE> class GiGaFactory;

class GiGaPhysUnknownParticles : public GiGaPhysConstructorBase
{
  /// friend factory for instantiation
  friend class GiGaFactory<GiGaPhysUnknownParticles>;

public:

  GiGaPhysUnknownParticles
  ( const std::string& type   ,
    const std::string& name   ,
    const IInterface*  parent ) ;

  ~GiGaPhysUnknownParticles();


public:
  void ConstructParticle () override;
  void ConstructProcess  () override;

private:

  G4Decay m_decayProcess ;
  G4UnknownDecay m_unknownDecay ;

};
// ============================================================================


// ============================================================================
#endif   ///< GAUSSPHYS_GIGAPHYSUNKNOWNPARTICLES_H
// ============================================================================











