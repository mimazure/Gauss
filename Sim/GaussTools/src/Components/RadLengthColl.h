/*****************************************************************************\
* (c) Copyright 2020 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// $Id: $
#ifndef RADLENGTHCOLL_RADLENGTHCOLL_H
#define RADLENGTHCOLL_RADLENGTHCOLL_H 1

// Include files
/// Geant4
#include "G4Timer.hh"
#include "G4VProcess.hh"
#include "G4ProcessManager.hh"
#include "G4Event.hh"
// GiGa
#include "GiGa/GiGaStepActionBase.h"
#include "G4SteppingManager.hh"
#include "GaudiKernel/IHistogramSvc.h"
#include "GaudiKernel/NTuple.h"
#include "GaudiKernel/INTupleSvc.h"
//GaussTools
#include "GaussTools/GaussTrackInformation.h"
/// stream
#include <fstream>
#include <cstdlib>

/** @class RadLengthColl RadLengthColl.h RadLengthColl/RadLengthColl.h
 *

 *
 *  @author Yasmine Sarah Amhis, Luca Pescatore, Kristian Zarebski
 *  @date   2018-01-29
 */

class RadLengthColl : virtual public GiGaStepActionBase {

public:

  RadLengthColl( const std::string& type,
                   const std::string& name,
                   const IInterface*  parent );

  virtual ~RadLengthColl( ); ///< Destructor
  StatusCode initialize() override;    ///< Algorithm initialization
  StatusCode finalize  () override;    ///< Algorithm finalization
  void UserSteppingAction ( const G4Step* ) override;

private:

  G4Track* track;
  G4ParticleDefinition* partdef;
  G4StepPoint *thePreStepPoint;

  const G4VPhysicalVolume* Vol;
  const G4LogicalVolume* VOL;

  enum ePlanesID{
    EndVelo, //0
    EndRich1, //1
    AfterTT, //2
    AfterMagnet, //3
    AfterOTPlane1, //4
    AfterOTPlane2, //5
    AfterOTPlane3, //6 
    AfterRich2, //7
    AfterSpd, //8
    AfterEcal, //9
    AfterHcal, //10
    AfterM2, //11
    AfterM2Filter, //12
    EoD, //13
    EndVP = EndVelo, //0
    AfterUT = AfterTT, //2
    AfterFTPlane1 = AfterOTPlane1, //4
    AfterFTPlane2 = AfterOTPlane2, //5
    AfterFTPlane3 = AfterOTPlane3, //6
    AfterNeutronShield = AfterSpd //8
    };

  std::map<std::string,unsigned int> PlanesIDs{
    std::make_pair("EndVelo",ePlanesID::EndVelo),
      std::make_pair("EndRich1",ePlanesID::EndRich1),
      std::make_pair("AfterTT",ePlanesID::AfterTT),
      std::make_pair("AfterMagnet",ePlanesID::AfterMagnet),
      std::make_pair("AfterOTPlane1",ePlanesID::AfterOTPlane1),
      std::make_pair("AfterOTPlane2",ePlanesID::AfterOTPlane2),
      std::make_pair("AfterOTPlane3",ePlanesID::AfterOTPlane3),
      std::make_pair("AfterRich2",ePlanesID::AfterRich2),
      std::make_pair("AfterSpd",ePlanesID::AfterSpd),
      std::make_pair("AfterEcal",ePlanesID::AfterEcal),
      std::make_pair("AfterHcal",ePlanesID::AfterHcal),
      std::make_pair("AfterM2",ePlanesID::AfterM2),
      std::make_pair("AfterM2Filter",ePlanesID::AfterM2Filter),
      std::make_pair("EoD",ePlanesID::EoD),
      std::make_pair("EndVP",ePlanesID::EndVP),
      std::make_pair("AfterUT",ePlanesID::AfterUT),
      std::make_pair("AfterFTPlane1",ePlanesID::AfterFTPlane1),
      std::make_pair("AfterFTPlane2",ePlanesID::AfterFTPlane2),
      std::make_pair("AfterFTPlane3",ePlanesID::AfterFTPlane3),
      std::make_pair("AfterNeutronShield",ePlanesID::AfterNeutronShield)
      };
  
  
  
    
  std::string VolName, VolumeName, ParticleName, IDPlane;
  std::string ntname;

  // Radiation Length
  G4double MaterialRadiationLength;
  G4double theRadLength, thePlane2PlaneRadLength, theCumulatedRadLength;
  // Interation Length
  G4double MaterialInterLength;
  G4double theInterLength, thePlane2PlaneInterLength, theCumulatedInterLength;

  G4double StepLength;
  G4ThreeVector initial_position;
  int  index, IDP;
  NTuple::Item < long >   m_ntrk ;
  NTuple::Array< float >  m_Xpos, m_Ypos, m_Zpos;
  NTuple::Array< float >  m_cumradlgh, m_p2pradlgh, m_cuminterlgh, m_p2pinterlgh;
  NTuple::Array< float >  m_eta, m_phi;
  NTuple::Array< long >   m_planeID;
  NTuple::Item< float >  m_origx;
  NTuple::Item< float >  m_origy;
  NTuple::Item< float >  m_origz;
  //std::vector< float >   mp_origx;
  //std::vector< float >   mp_origy;
  //std::vector< float >   mp_origz;
  float    mp_origx;
  float    mp_origy;
  float    mp_origz;

  float    previous_Z=-100; // For relative Z position check

  NTuple::Tuple* m_matScan_PlaneDatas ;

  StatusCode status, writestatus;

};
#endif // RADLENGTHCOLL_RADLENGTHCOLL_H
