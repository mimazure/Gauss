/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// $Id: GetMCRichOpticalPhotonsAlg.h,v 1.5 2009-07-17 13:46:12 jonrob Exp $
#ifndef SENSDET_GETMCRICHOPTICALPHOTONSALG_H
#define SENSDET_GETMCRICHOPTICALPHOTONSALG_H 1

// base class
#include "GaussRICH/GetMCRichInfoBase.h"

// rich kernel
#include "RichUtils/RichMap.h"

/** @class GetMCRichOpticalPhotonsAlg GetMCRichOpticalPhotonsAlg.h
 *
 *  Algorithm to create MCRichOpticalPhoton objects from Gauss G4 information
 *
 *  @author Sajan EASO
 *  @date   2005-12-06
 */

class GetMCRichOpticalPhotonsAlg : public GetMCRichInfoBase
{

public:

  /// Standard constructor
  GetMCRichOpticalPhotonsAlg( const std::string& name, ISvcLocator* pSvcLocator );

  ~GetMCRichOpticalPhotonsAlg( ); ///< Destructor

  StatusCode execute   () override;    ///< Algorithm execution
  StatusCode finalize  () override;    ///< Algorithm finalization

private:

  // now the variables used for the local monitoring. This may eventually
  // go into GaussMonitor. SE Nov 2005.
  /// Count number of events processed
  unsigned long int m_nEvts;

  /// map for photon counting
  typedef Rich::Map< const Rich::RadiatorType, unsigned long int > RadMap;

  /// count overall number of photons in each RICH radiator medium
  RadMap m_hitTally;

  /// Location of MC Rich Hits
  std::string m_mcRichHits ;
};

#endif // SENSDET_GETMCRICHOPTICALPHOTONSALG_H
