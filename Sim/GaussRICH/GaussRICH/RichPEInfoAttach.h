/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef      GaussRICH_RICHPEINFOATTACH_h 
#define      GaussRICH_RICHPEINFOATTACH_h 1 
#include "RichPEInfo.h"
#include "G4Track.hh"

extern int RichPhotTkRadiatorNumber ( const G4Track& aPhotonTrk );

extern G4Track* RichPEInfoAttach(const G4Track& aPhotonTk, G4Track* aPETk, const G4ThreeVector & aLocalElectronOrigin);
extern G4Track* RichPEBackScatAttach(const G4Track& aElectronTk, G4Track* aPEBackScatTk);


#endif //GaussRICH_RICHPEINFOATTACH

