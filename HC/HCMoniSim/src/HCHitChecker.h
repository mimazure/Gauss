/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

// Gaudi
#include "GaudiAlg/GaudiTupleAlg.h"
// LHCb
#include "Event/MCHit.h"
/** @class HCHitChecker HCHitChecker.h
 *
 *  Algorithm run in Gauss for monitoring Herschel MCHits.
 *
 */

class MCHit;

class HCHitChecker : public GaudiTupleAlg {
public:
  /// Standard constructor
  using GaudiTupleAlg::GaudiTupleAlg;

  StatusCode initialize() override;    ///< Algorithm initialization
  StatusCode execute() override;       ///< Algorithm execution

private:

  /// TES location of MCHits
  Gaudi::Property<std::string> m_hitsLocation{this,"HitLocation",
  LHCb::MCHitLocation::HC,"TES location of MCHits"};

};
